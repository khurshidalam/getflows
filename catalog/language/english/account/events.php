<?php
// Heading 
$_['heading_title'] 		= 'Events';
$_['heading_title_add'] 	= 'Add New Event';
$_['heading_title_edit'] 	= 'Editing Event: ';
$_['heading_title_view'] 	= 'Viewing Event: ';
$_['heading_title_events'] 	= 'Events';

// Text
$_['text_search']       	= 'Events meeting the search criteria';
$_['text_success']  		= 'Success: You have added %s to your Event.';
$_['text_success_atc']      = 'Success: You have added <a href="%s">%s</a> to your <a href="%s">shopping cart</a>!';
$_['text_success_note']     = 'Thank you, your note has been submitted.';
$_['text_success_save']  	= 'Success: You have saved your Event.';
$_['text_success_add']  	= 'Success: You have added a new Event.';
$_['text_success_delete']  	= 'Success: You have deleted your Event.';
$_['text_account']  		= 'Account';
$_['text_empty']    		= 'Your Events list is Empty, click the add button to start creating one.';
$_['text_no_results']    	= 'There are no results matching your search term.';
$_['text_invitees']    		= 'Invitees';
$_['text_public']    		= 'Public';
$_['text_private']    		= 'Private';
$_['text_enabled']    		= 'Enabled';
$_['text_disabled']    		= 'Disabled';
$_['text_awaiting']    		= 'Awaiting';
$_['text_finished']    		= 'Finsished';
$_['text_event_owner']    	= 'Owned By: ';
$_['text_event_status']    	= 'This Event Is: ';
$_['text_choose_event']    	= 'Choose Event: ';
$_['text_items']            = '%s item(s) - %s';
$_['text_empty']            = 'You have not created any events yet.';
$_['text_no_events']        = 'You have not created any events yet.';
$_['text_not_published']    = 'Your Email Address Will Not Be Publised';
$_['text_in_progress']    	= 'In Progress';
$_['text_no_notes']    		= 'There are no Notes to display.';
$_['text_no_rsvps']    		= 'There are no RSVPs to display.';
$_['text_web_link']    		= 'Website Link (opens in new window):';
$_['text_web_link_anchor']	= 'Click Here to View Website or Photo\'s';
$_['text_send_email']		= 'Email Event Owner';
$_['text_email_sent']		= 'An Email has been sent to the owner of this event.';
$_['text_email_noti_sent']	= 'Notifications have been sent to the selected Invitees.';
$_['text_write']        	= 'Write a note about this event.';
$_['text_note']         	= '<span style="color: #FF0000;">Note:</span> HTML is not translated!';
$_['text_rsvp']        		= 'Let the Event Owner know if you are attending this Event:';
$_['text_rsvp_disabled'] 	= 'RSVPs are Disabled. RSVPs can be enabled from the General Tab.';
$_['text_notes_disabled']	= 'Notes are Disabled. Notes can be enabled from the General Tab.';
$_['text_wait']         	= 'Please Wait!';
$_['text_on']          		= ' on ';
$_['text_message']          = 'Message';
$_['text_placeholder']      = '[Optional] you can tell people about your event using this box.  If you want to add a \'ship to\' address or other personal information, we strongly suggest that you set this event as Private.';
$_['text_noti_message_placeholder'] = '[Optional] You can add your own message to the Notification Email here, click the save button first.';
$_['tab_general']    		= 'General';
$_['tab_notify']    		= 'Notifications';
$_['tab_products']    		= 'Products';
$_['tab_share']    			= 'Sharing';
$_['tab_notes']    			= 'Event Notes';
$_['tab_rsvp']    			= 'RSVP';
$_['entry_short_note']		= 'Note:';
$_['entry_guest_name_1']	= 'Guest Name 1';
$_['entry_guest_name_2']	= 'Guest Name 2';
$_['entry_guest_name_3']	= 'Guest Name 3';
$_['entry_guest_name_4']	= 'Guest Name 4';
$_['entry_guest_name_5']	= 'Guest Name 5';
$_['entry_guest_name_6']	= 'Guest Name 6';
$_['entry_guest_name_7']	= 'Guest Name 7';
$_['entry_guest_name_8']	= 'Guest Name 8';
$_['entry_guest_name_9']	= 'Guest Name 9';
$_['entry_guest_name_10']	= 'Guest Name 10';
$_['entry_to_email']		= 'Recipient Email Address';
$_['entry_email']			= 'Your Email Address';
$_['entry_name']			= 'Your Name';
$_['entry_title']			= 'Event Title';
$_['entry_type']			= 'Event Type';
$_['entry_start_date']		= 'Start Date';
$_['entry_end_date']		= 'End Date';
$_['entry_att_email']		= 'Email Address';
$_['entry_att_name']		= 'Name';
$_['entry_status'] 			= 'Status';
$_['entry_search']      	= 'Search:';
$_['entry_event_title']     = 'Event Title:';
$_['entry_event_name']      = 'Event Owners Name:';
$_['entry_event_owner']     = 'Event Owners Email:';
$_['entry_event_invitee']   = 'Email Address of an Attendee:';
$_['entry_from_email']		= 'Your Email Address';
$_['entry_from_name']		= 'Your Name';
$_['entry_captcha']  		= 'Enter the code in the box below:';
$_['entry_name']			= 'Your Name';
$_['entry_note']			= 'Your Note';
$_['entry_message']			= 'Your Message';
$_['entry_rsvp']			= 'RSVP Tab';
$_['entry_notes']			= 'Notes Tab';
$_['entry_photo']			= 'Link to Photo<br /><span style="font-size:0.8em">Photo will show on Event Page</span>';
$_['entry_web_link']		= 'Link to Website<br /><span style="font-size:0.8em">Link Shows on Event Page</span>';

$_['text_private_info']    		= 'This is a private event and only those people who have been emailed an invitation have access to this event. If you would like to be added to the event then you will need to email the owner of the event so that they can provide you with the necessary link. You can do this by using the form below:';
$_['text_what_is_help']			= 'In simple terms, think \'Wedding List\' or \'Gift Registry\'.<br /><br />The event system allows you to create events to which you attach wishlists and invite friends or relatives to purchase from the list of products that you have specified.<br /><br />
							You can specify a timeframe for the event and choose who to invite - all invitees receive personal email invitations with a link to your event.
							<br /><br /><b>Public vs Private Events:</b><br />All visitors to the website can see a list of all events, but only those people that have been emailed a link to a private event will be able to view it.  Everyone can view Public events.<br /><br />
							<b>Enabled vs Disabled Events:</b><br />Enabled events can be viewed and show up in the events list, Disabled events can\'t be viewed and do not show up in the events list.<br /><br />
							<b>Adding Products:</b><br />Once you have created your event here, you can then edit your event and add products or browse our catalog and add prodcuts to your event directly from the product information page using the \'Add to Event\' link.';
$_['text_eventpage_heading']	= 'You can create your own events or view other peoples events...<br /><br />';
$_['text_view_description']		= 'Events is the simple name for wishlists or gift registries that are available to viewed by others and if you know the person then you can buy items for them directly from their event.<br /><br />
								If you know the person or event that you\'re looking for then you can do a simple search for thier event and buy them something that they actually want - How good is that!<br /><br />
								If you\'re looking for some inspriation then you can equally just have a browse through the events and see what others are buying, or hoping to receive!<br /><br /><br />';
$_['text_create_description']	= 'What is an event? In simple terms, think \'Wedding List\' or \'Gift Registry\'.<br /><br />Events allow you to create multiple wishlists of products for your friends or relatives to buy for you.<br /><br />
									Events are clever: If someone buys you something from your list we will mark that product as having been bought already so you shouldn\'t end up with duplicate items.<br /><br />
									To start creating events simply click the button below: if you\'re not already logged in then you will need to login or create an account first.';
$_['text_adding_products']	= '<p>You can now add products to your event by either editing it and then clicking on the \'Add Products\' button or by browsing our store and adding products by using the \'Add to Event\' link which is now available on the product pages.</p>';
$_['text_what_is_heading']	= 'What is an Event?';
$_['text_sent']				= 'Your Email has been Sent';
$_['text_link']				= 'Here is the link to your event for if you want to send or share it yourself:<br /><br />';
$_['button_send']			= 'Send Notifications to Selected';
$_['button_add']			= 'Add';
$_['button_add_products']	= 'Add Product(s)';
$_['button_edit']			= 'Edit';
$_['button_add_cart']		= 'Add to Cart';
$_['button_select']			= 'Add to Event';
$_['button_view']			= 'View Event';
$_['button_remove']			= 'Remove';
$_['button_show']			= 'Search';
$_['button_cancel']			= 'Cancel / Exit';
$_['button_save']			= 'Save';
$_['button_update']			= 'Update';
$_['button_back']			= 'Back to Results';
$_['button_print']			= 'Printer Friendly Page';
$_['button_print_page']		= 'Print This Page';
$_['button_add_new']		= 'Add New Event';
$_['button_send_email']		= 'Send Email';
$_['button_send_rsvp']		= 'Send RSVP';
$_['button_create_event']	= 'Create Your Own Event Event';
$_['button_view_event']		= 'View Events';
$_['button_show_search']	= 'Show Search Form';
$_['button_hide_search']	= 'Hide Search Form';
$_['text_twitter_title']	= 'Click to share your Event on Twitter';
$_['text_twitter_alt']		= 'Twitter Share Button';
$_['text_facebook_title']	= 'Click to share your Event on Facebook';
$_['text_facebook_alt']		= 'Facebook Share Button';
$_['text_social_share']		= 'Share the Link';
$_['text_social_message']	= 'To share the link to your Event on Facebook or Twitter, just click the buttons below (confirmation opens in new window):';
$_['text_noti_preview']		= 'Notification Preview';
$_['text_my_event']			= 'My Event and Wishlist:';
$_['text_required_fields']	= '* All fields are required';
$_['text_rsvp_sent']		= '<b>Thank You. Your RSPV has been sent to the Event Owner</b>';
$_['text_rsvp_placeholder']	= '[Optional] You can add a short message to the Event Owner here.';
$_['error_name']			= 'Please enter a valid name. Name must be between 3 and 32 characters!';
$_['error_email']			= 'Please enter a valid email address. E-Mail Address does not appear to be valid!';
$_['error_rsvp_name']		= 'Please enter a valid name. Name must be between 3 and 32 characters!';
$_['error_rsvp_email']		= 'Please enter a valid email address. E-Mail Address does not appear to be valid!';
$_['error_rsvp_email_fail']	= '<b>You have already sent an RSVP for this Event.</b>';
$_['error_title']			= 'Title must be between 1 and 50 characters!';
$_['error_start_date']		= 'Please enter a valid start date';
$_['error_end_date']		= 'Please enter a valid end date';
$_['error_captcha']  		= 'Verification code does not match the image!';
$_['error_minimum']         = 'Minimum order amount for %s is %s!';	
$_['error_required']        = '%s required!';
$_['error_mail']        	= 'You must select at least one person to send email to!';

// Column
$_['column_title'] 		= 'Title';
$_['column_qty_rqd'] 	= 'Qty Required';
$_['column_buy_qty'] 	= 'Qty to Buy';
$_['column_qty_bought'] = 'Qty Bought';
$_['column_start_date'] = 'Start Date';
$_['column_end_date'] 	= 'End Date';
$_['column_status'] 	= 'Status';
$_['column_type'] 		= 'Type';
$_['column_action'] 	= 'Action';
$_['column_email'] 		= 'Email';
$_['column_name'] 		= 'Name';
$_['column_guest_name'] = 'Name';
$_['column_note'] 		= 'Note';
$_['column_guests'] 	= 'Guests';

// Email Text
$_['text_email_subject'] 		= 'Event Notification from %s';
$_['text_email_intro']   		= 'Hello,';
$_['text_email_body']    		= '%s has invited you to join their Event. To view this event please click on the link below:';
$_['text_email_noti_message']	= 'Message from Sender:' . "\n" . '%s';
$_['text_email_no_message']		= 'No Message';
$_['text_email_signoff'] 		= 'Kind Regards,';
$_['text_email_end']     		= '%s.';

// Email Message sent to event owners from the private event access page.
$_['mail_subject']   	 = 'I would like access to your Event.';
$_['mail_text_1']   	 = 'You are receiving this email as the person named below has requesed access to your %s event.';
$_['mail_text_2']   	 = 'If you wish to allow %s access to your event then simply reply to this email to send them the link to your event which is shown below:';
$_['mail_text_3']   	 = 'Kind Regards,';
$_['mail_text_4']   	 = '%s.';
?>
<?php
// Heading
$_['heading_title']     = 'Events Product Selector';

// Text
$_['text_search']       = 'Products meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_critea']       = 'Search Criteria';
$_['text_empty']        = 'There is no product that matches the search criteria.';
$_['text_empty']        = 'There are no products to list.';
$_['text_allproducts']  = 'Complete Product List';
$_['text_quantity']     = 'Qty:';
$_['text_price']        = 'Price:'; 
$_['text_tax']          = 'Ex Tax:'; 
$_['text_reviews']      = 'Based on %s reviews.'; 
$_['text_display']      = 'Display:';
$_['text_list']         = 'List';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';
$_['text_events_pp']    = 'Events Product Selector';
$_['text_events']    	= 'Events';
$_['text_events_edit']  = 'Editing Event';
$_['text_account'] 		= 'Account';
$_['button_return']		= 'Return to Event';
$_['button_select_option']     = 'Select Options';
$_['button_close_option']      = 'Close Options';
$_['button_add_product']       = 'Add to Event';
// Entry
$_['entry_search']      = 'Search:';
$_['entry_description'] = 'Search in product descriptions';
?>
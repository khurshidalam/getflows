<?php
// Text
$_['text_option']       = 'Available Options';
$_['text_qty']          = 'Qty:';
$_['text_minimum']      = 'This product has a minimum quantity of %s';
$_['text_upload']       = 'Your file was successfully uploaded!';
$_['text_wait']         = 'Please Wait!';
$_['text_error']        = 'Product not found!';

$_['button_select_option']	= 'Select Options';
$_['button_close_option']	= 'Close Options';
$_['button_add_product']	= 'Add to Event';

// Error
$_['error_upload']      = 'Upload required!';
$_['error_filename']    = 'Filename must be between 3 and 64 characters!';
$_['error_filetype']    = 'Invalid file type!';
?>
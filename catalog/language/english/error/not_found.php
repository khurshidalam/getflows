<?php
// Heading
$_['heading_title'] = 'page not found';

// Text
$_['text_error']    = 'The page you requested cannot be found.';

// Email
$_['email_subject']  = 'Enquiry from %s';

// Errors
$_['error_name']     = 'Name must be between 3 and 32 characters!';
$_['error_email']    = 'E-Mail Address does not appear to be valid!';
$_['error_enquiry']  = 'Enquiry must be between 10 and 3000 characters!';
$_['error_captcha']  = 'Verification code does not match the image!';
?>
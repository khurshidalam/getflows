<?php
// Text
$_['text_title']       = 'Free Shipping';
$_['text_longdescription'] = 'Free Shipping (using USPS)<p style="margin:5px 0 0 26px; font-size:13px; cursor:auto">You can expect your order to arrive within 7-10 business days</p>';
$_['text_description'] = 'Free Shipping (using USPS)';
?>
<?php 
class ControllerEventsEventsView extends Controller {
	private $error = array();

	public function index() {
	
		if (!isset($this->request->get['event_id'])) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
			
		$this->language->load('account/events');
		$this->language->load('checkout/cart');
		$this->load->model('account/events');
		
		$this->getEventPage();
	}

	private function getEventPage() {
	
		if (isset($this->request->get['event_id'])) {
			$eid = $this->request->get['event_id'];
			$this->data['event_id'] = $this->request->get['event_id'];
		} else {
			$eid = '0';
		}
		
		if (isset($this->request->get['akey'])) {
			$akey = $this->request->get['akey'];
			$this->data['akey'] = $this->request->get['akey'];
		} else {
			$akey = '0';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order']) && $this->request->get['order'] == 'sort_desc') {
			$order = SORT_DESC;
			$dir = 'sort_desc';
			$this->data['order'] = 'desc';

		} else {
			$order = SORT_ASC;
			$dir = 'sort_asc';
			$this->data['order'] = 'asc';
		}
		
		$enabled_test = $this->model_account_events->getEnabledEvents($eid);

		if (!$enabled_test) {
  			$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		$this->data['private'] = false;
		
		$private_test = $this->model_account_events->getPrivateEvents($eid);
		
		if (isset($this->request->get['event_id']) && $private_test) {
			if ($akey != $this->model_account_events->getAccessKey($eid) && (!$private_test)) {
	  			$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
			} elseif ($akey != $this->model_account_events->getAccessKey($eid) && ($private_test)) {
				$this->data['private'] = true;
			} elseif ($akey == $this->model_account_events->getAccessKey($eid) && ($private_test)) {
				$this->data['private'] = false;
			}
		}

		if ($akey != $this->model_account_events->getAccessKey($eid) && !$private_test) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		if (!isset($this->request->get['akey']) && !$private_test) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}

		if ($this->model_account_events->checkEvents($eid)) {		
			$query = $this->model_account_events->getEventsTitle($eid);
			$title = $query['title'];
			
			$this->document->setTitle($title);
			$this->data['heading_title'] = $title;
		} else {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		if (version_compare(VERSION, '1.5.4.1', '>') == true) {
			$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		}

		$this->data['entry_from_name'] = $this->language->get('entry_from_name');
		$this->data['entry_from_email'] = $this->language->get('entry_from_email');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');
		
		$this->data['text_private_info'] = $this->language->get('text_private_info');
		$this->data['text_send_email'] = $this->language->get('text_send_email');
		$this->data['text_event_status'] = $this->language->get('text_event_status');
		$this->data['text_event_owner'] = $this->language->get('text_event_owner');
		$this->data['text_no_notes'] = $this->language->get('text_no_notes');
		$this->data['text_note'] = $this->language->get('text_note');
		$this->data['text_wait'] = $this->language->get('text_wait');
		$this->data['text_write'] = $this->language->get('text_write');
		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_message'] = $this->language->get('text_message');
		$this->data['text_rsvp'] = $this->language->get('text_rsvp');
		$this->data['text_rsvp_placeholder'] = $this->language->get('text_rsvp_placeholder');
		$this->data['text_web_link'] = $this->language->get('text_web_link');
		$this->data['text_web_link_anchor'] = $this->language->get('text_web_link_anchor');
					
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_buy_qty'] = $this->language->get('column_buy_qty');
		$this->data['column_qty_bought'] = $this->language->get('column_qty_bought');
		$this->data['column_qty_rqd'] = $this->language->get('column_qty_rqd');
   		$this->data['column_name'] = $this->language->get('column_name');
   		$this->data['column_model'] = $this->language->get('column_model');
   		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_price'] = $this->language->get('column_price');
   		$this->data['column_total'] = $this->language->get('column_total');
				
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_add_cart'] = $this->language->get('button_add_cart');
		$this->data['button_send_email'] = $this->language->get('button_send_email');
		$this->data['button_create_event'] =  $this->language->get('button_create_event');
		$this->data['button_back'] =  $this->language->get('button_back');
		$this->data['button_print'] =  $this->language->get('button_print');
		$this->data['button_send_rsvp'] = $this->language->get('button_send_rsvp');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_note'] = $this->language->get('entry_note');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_short_note'] = $this->language->get('entry_short_note');
		$this->data['entry_guest_name_1'] = $this->language->get('entry_guest_name_1');
		$this->data['entry_guest_name_2'] = $this->language->get('entry_guest_name_2');
		$this->data['entry_guest_name_3'] = $this->language->get('entry_guest_name_3');
		$this->data['entry_guest_name_4'] = $this->language->get('entry_guest_name_4');
		$this->data['entry_guest_name_5'] = $this->language->get('entry_guest_name_5');
		$this->data['entry_guest_name_6'] = $this->language->get('entry_guest_name_6');
		$this->data['entry_guest_name_7'] = $this->language->get('entry_guest_name_7');
		$this->data['entry_guest_name_8'] = $this->language->get('entry_guest_name_8');
		$this->data['entry_guest_name_9'] = $this->language->get('entry_guest_name_9');
		$this->data['entry_guest_name_10'] = $this->language->get('entry_guest_name_10');
		
		$this->data['tab_products'] =  $this->language->get('tab_products');
		$this->data['tab_notes'] =  $this->language->get('tab_notes');
		$this->data['tab_rsvp'] =  $this->language->get('tab_rsvp');
		$this->data['create'] = $this->url->link('account/events/addEvent', '', 'SSL');
		$this->data['print_button'] = $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
		$this->data['eid'] = $eid;
		$this->data['akey'] = $akey;
		
		$event_query = $this->model_account_events->getEventDetail($eid);
		
		$this->data['end_date'] = date("d/m/Y", strtotime($event_query['end_date']));
		$this->data['start_date'] = date("d/m/Y", strtotime($event_query['start_date']));
		$this->data['event_message'] = nl2br($event_query['e_comment']);
		$this->data['event_owner'] = ucfirst($event_query['name']);
		$this->data['photo'] = $event_query['photo'];
		$this->data['web_link'] = $event_query['web_link'];
		$this->data['event_status'] = (strtotime("now") < strtotime($event_query['start_date']) ? $this->language->get('text_awaiting') : (strtotime("now") > strtotime($event_query['end_date']) ? $this->language->get('text_finished') : $this->language->get('text_in_progress')));
		$this->data['rsvp_status'] = $this->model_account_events->getRSVPstatus($eid, $akey);
		$this->data['note_status'] = $this->model_account_events->getNoteStatus($eid, $akey);
		
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 
		
      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title_events'),
			'href'      => $this->url->link('events/events_list', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
		
      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title_view'),
			'href'      => $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

		$this->data['cancel'] = $this->url->link('events_events_list', '', 'SSL');
			
						
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->load->model('tool/image');
		
   		$this->data['products'] = array();
		
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);
		
		if (isset($this->request->get['event_id']) && !$this->data['private']) {
			$products = $this->events->getEventsViewProducts($this->model_account_events->getEventsViewProducts($eid), $eid);
			
			if ($products) {
	      		foreach ($products as $product) {
					$product_total = 0;
						
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}			
					
					if ($product['minimum'] > $product_total) {
						$this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
					}				
						
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
					} else {
						$image = '';
					}
					
			if ($product['image']) {
				$popup = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$popup = '';
			}
					
					$bought_data = array();				
	
					foreach ($product['bought'] as $buyed) {
						
						$bought_data[] = array(
							'bought'  => $buyed['bought_qty']
						);
	   	     		}
						
					$option_data = array();			

	        		foreach ($product['option'] as $option) {
						if ($option['type'] != 'file') {
							$value = $option['option_value'];	
						} else {
							$filename = $this->encryption->decrypt($option['option_value']);
							
							$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
						}
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
   	     			}
					
					// Display prices
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					// Display prices
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
					} else {
						$total = false;
					}
					
					$query = $this->model_account_events->getEventsEndDate($eid);
					
					if (strtotime("now") > strtotime($query['end_date'])) {
						$finished = '';
					} else {
						$finished = 1;
					}
					
					$comment = $this->model_account_events->getEventComment($eid, $product['key']);
					
	   	     		$this->data['products'][] = array(
							'id'	   => $product['product_id'],
							'iid'	   => $product['iid'],
							'key'      => $product['key'],
							'thumb'    => $image,
							'popup'	   => $popup,
							'name'     => $product['name'],
							'comment'  => $comment['p_comment'],
 		         			'model'    => $product['model'],
							'bought'   => $buyed['bought_qty'],
							'tax_class_id' => $product['tax_class_id'],
							'finished' => $finished,
							'fred'	   => $product['quantity'] - $buyed['bought_qty'],
							'option'   => $option_data,
							'quantity' => $product['quantity'],
							'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
							'reward'   => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
							'price'    => preg_replace("/[^-0-9\.]/","",$product['price']),
							'total'    => $total,
							'href'     => $this->url->link('events/events_view/qi', 'product_id=' . $product['product_id'])
					);
				}
			}
		}
		if (isset($this->request->get['akey']) && $this->data['products']) {
			if ($sort == 'name') {
				foreach ($this->data['products'] as $key => $row) {
  	 				$name[$key]  = $row['name'];
				}			

				array_multisort($name, $order, $this->data['products']);
			
			} elseif ($sort == 'model') {
				foreach ($this->data['products'] as $key => $row) {
  	 				$model[$key]  = $row['model'];
				}

				array_multisort($model, $order, $this->data['products']);
			
			} elseif ($sort == 'price') {
				$price= array();
				foreach ($this->data['products'] as $key => $row) {
  	 				$price[$key]  = $row['price'];
				}

				array_multisort($price, $order, $this->data['products']);

			}
		}

		$url = '';

		if ($dir == 'sort_asc') {
			$url .= '&order=sort_desc';
		} else {
			$url .= '&order=sort_asc';
		}

		//$this->data['sort_name'] = ($private_test ? $this->url->link('events/events_view', 'event_id=' . $eid) : $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=name' . $url, 'SSL'));
		//$this->data['sort_model'] = ($private_test ? $this->url->link('events/events_view', 'event_id=' . $eid) : $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=model' . $url, 'SSL'));
		//$this->data['sort_price'] = ($private_test ? $this->url->link('events/events_view', 'event_id=' . $eid) : $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=price' . $url, 'SSL'));
		$this->data['sort_name'] = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=name' . $url, 'SSL');
		$this->data['sort_model'] = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=model' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey . '&sort=price' . $url, 'SSL');
		$this->data['continue'] = $this->url->link('common/home');
		
		$this->data['sort'] = $sort;
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_view.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_view.tpl';
		} else {
			$this->template = 'default/template/events/events_view.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
							
		$this->response->setOutput($this->render());		
	}
	
  	private function validateForm() {
    	if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 32)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}

    	if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
      		$this->error['email'] = $this->language->get('error_email');
    	}
		
    	if ((utf8_strlen($this->request->post['rsvp_name']) < 1) || (utf8_strlen($this->request->post['rsvp_name']) > 32)) {
      		$this->error['name'] = $this->language->get('error_rsvp_name');
    	}

    	if ((utf8_strlen($this->request->post['rsvp_email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['rsvp_email'])) {
      		$this->error['rsvp_email'] = $this->language->get('error_rsvp_email');
    	}
		
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
  	}

	public function add() {
		$this->language->load('account/events');
		
		$this->load->model('account/events');
		
		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (isset($this->request->post['eid'])) {
			$eid = $this->request->post['eid'];
		} else {
			$eid = 0;
		}
		
		if (isset($this->request->post['iid'])) {
			$iid = $this->request->post['iid'];
		} else {
			$iid = 0;
		}
		
		$prod_ops = $this->model_account_events->getProductOption($eid, $iid);
		$productop = explode(':', $product_op = $prod_ops['product']);
	
		// Options
		if (isset($productop[1])) {
			$options = unserialize(base64_decode($productop[1]));
		} else {
			$options = array();
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
						
			if ($options) {
				$option = array_filter($options);
			} else {
				$option = array();	
			}
			
			$option['eventpid'] = $iid;
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
			
			if (!$json) {
				$this->cart->add($this->request->post['product_id'], $quantity, $option);

				$json['success'] = sprintf($this->language->get('text_success_atc'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));
				
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				
				// Totals
				$this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array(); 
					
					$results = $this->model_setting_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
				
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
						
						$sort_order = array(); 
					  
						foreach ($total_data as $key => $value) {
							$sort_order[$key] = $value['sort_order'];
						}
			
						array_multisort($sort_order, SORT_ASC, $total_data);			
					}
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		
		$this->response->setOutput(json_encode($json));		
	}
	
	public function sendPrivateEmail() {
    	$this->load->language('account/events');
		$this->load->model('account/events');
		
		if (isset($this->request->post['eid'])) {
			$eid = $this->request->post['eid'];
		} else {
			$eid = '0';
		}
		
		$query1 = $this->model_account_events->getEmailAddress($eid, $this->model_account_events->getAccessKey($eid));
		$to_email_address = $query1['email'];
		
		$query2 = $this->model_account_events->getEventsTitle($eid);
		$event_title = '"' . $query2['title'] . '"';
		
		$link = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' .$this->model_account_events->getAccessKey($eid), '', 'SSL');
		
			if(isset($this->request->post['from_email']) and preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['from_email'])){
		    
				if($this->session->data['captcha'] == $this->request->post['captcha']) {

					if ((strlen(utf8_decode($this->request->post['from_name'])) > 2) && (strlen(utf8_decode($this->request->post['from_name'])) < 32)) {
					
					    echo('$("#sent_result").html("'.$this->language->get('text_email_sent').'");$("#sendPrivEmail")[0].reset();$("#send_mail_button").hide();');
						$message = sprintf($this->language->get('mail_text_1'), $event_title) . "\n\n";
						$message .= sprintf($this->language->get('mail_text_2'), strtoupper($this->request->post['from_name'])) . "\n\n";
						$message .= sprintf($link) . "\n\n";
						$message .= sprintf($this->language->get('mail_text_3')) . "\n\n";
						$message .= sprintf($this->language->get('mail_text_4'), $this->config->get('config_name'));
						$subject = sprintf($this->language->get('mail_subject'));
							
						$mail = new Mail();
						$mail->protocol = $this->config->get('config_mail_protocol');
						$mail->parameter = $this->config->get('config_mail_parameter');
						$mail->hostname = $this->config->get('config_smtp_host');
						$mail->username = $this->config->get('config_smtp_username');
						$mail->password = $this->config->get('config_smtp_password');
						$mail->port = $this->config->get('config_smtp_port');
						$mail->timeout = $this->config->get('config_smtp_timeout');				
						$mail->setTo($to_email_address);
						$mail->setFrom($this->request->post['from_email']);
						$mail->setSender($this->request->post['from_name']);
						$mail->setSubject($subject);
						$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
						$mail->send();
						
	    				echo('$("#send_result").html("'.$this->language->get('text_email_sent').'");$("#sendPrivEmail")[0].reset();');

	 				}else{
	    				echo('$("#send_result").html("<span class=\"error\">'.$this->language->get('error_name').'</span>")');
	 				}
	 			}else{
	    			echo('$("#send_result").html("<span class=\"error\">'.$this->language->get('error_captcha').'</span>")');
	 			}
		}else{
			echo('$("#send_result").html("<span class=\"error\">'.$this->language->get('error_email').'</span>")');
		}
	}	
	
	public function notes() {
    	$this->language->load('account/events');
		
		$this->load->model('account/events');

		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_no_notes'] = $this->language->get('text_no_notes');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}  
		
		$this->data['notes'] = array();
		
		$notes_total = $this->model_account_events->getTotalNotesByEvent_id($this->request->get['event_id'], $this->request->get['akey']);
			
		$results = $this->model_account_events->getNotesByEventId($this->request->get['event_id'], $this->request->get['akey'], ($page - 1) * 5, 5);
      		
		foreach ($results as $result) {
        	$this->data['notes'][] = array(
        		'author'     => $result['author'],
				'text'       => $result['text'],
        		'notes'    => sprintf($this->language->get('text_notes'), (int)$notes_total),
        		'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        	);
      	}			
			
		$pagination = new Pagination();
		$pagination->total = $notes_total;
		$pagination->page = $page;
		$pagination->limit = 5; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('events/events_view/notes', 'event_id=' . $this->request->get['event_id'] . '&akey=' . $this->request->get['akey'] . '&page={page}', '', 'SSL');
			
		$this->data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_notes.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_notes.tpl';
		} else {
			$this->template = 'default/template/events/events_notes.tpl';
		}
		
		$this->response->setOutput($this->render());
	}
	
	public function write_note() {
		$this->language->load('account/events');
		
		$this->load->model('account/events');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
				$json['error'] = $this->language->get('error_name');
			}
			
			if ((utf8_strlen($this->request->post['text']) < 5) || (utf8_strlen($this->request->post['text']) > 2000)) {
				$json['error'] = $this->language->get('error_text');
			}
	
			if (empty($this->session->data['captcha']) || ($this->session->data['captcha'] != $this->request->post['captcha'])) {
				$json['error'] = $this->language->get('error_captcha');
			}
				
			if (!isset($json['error'])) {
				$this->model_account_events->addNote($this->request->get['event_id'], $this->request->get['akey'], $this->request->post);
				
				$json['success'] = $this->language->get('text_success_note');
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function sendRSVP() {
	
    	$this->load->language('account/events');
		$this->load->model('account/events');
		
		if (isset($this->request->post['eid'])) {
			$eid = $this->request->post['eid'];
		} else {
			$eid = '0';
		}
		
		if (isset($this->request->post['akey'])) {
			$akey = $this->request->post['akey'];
		} else {
			$eid = '0';
		}

		// Check email for previous RSVP and errror out.

			if(isset($this->request->post['rsvp_email']) and preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['rsvp_email'])){
		    
				if($this->session->data['captcha'] == $this->request->post['captcha']) {

					if ((strlen(utf8_decode($this->request->post['rsvp_name'])) > 2) && (strlen(utf8_decode($this->request->post['rsvp_name'])) < 32)) {
					
						if (!$this->model_account_events->checkRSVPemail($this->request->post['rsvp_email'])) {
					

							$this->model_account_events->addRSVP($this->request->post);
	    					echo('$("#sendRSVP")[0].reset();$("#rsvpform").hide();$("#send_rsvp").hide();$("#send_result").html("'.$this->language->get('text_rsvp_sent').'");');

		 				}else{
		    				echo('$("#send_result").html("<span class=\"error\">'.$this->language->get('error_rsvp_email_fail').'</span>")');
		 				}
	 				}else{
	    				echo('$("#error_rsvp_name").html("<span class=\"error\">'.$this->language->get('error_rsvp_name').'</span>")');
	 				}
	 			}else{
	    			echo('$("#error_captcha").html("<span class=\"error\">'.$this->language->get('error_captcha').'</span>")');
	 			}
		}else{
			echo('$("#error_rsvp_email").html("<span class=\"error\">'.$this->language->get('error_rsvp_email').'</span>")');
		}
	}
	
	
	public function qi() { 
		$this->language->load('product/product');
	
		$this->load->model('catalog/category');	
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
		
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
	//	$this->data['product_info'] = $product_info;
		
		if ($product_info) {
			
			$this->data['lang'] = $this->language->get('code');
			$this->data['direction'] = $this->language->get('direction');
			
			$this->data['heading_title'] = $product_info['name'];
			
			$this->data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$this->data['text_model'] = $this->language->get('text_model');
			$this->data['text_reward'] = $this->language->get('text_reward');
			$this->data['text_points'] = $this->language->get('text_points');	
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_stock'] = $this->language->get('text_stock');
			$this->data['text_price'] = $this->language->get('text_price');
			$this->data['text_tax'] = $this->language->get('text_tax');
			$this->data['text_discount'] = $this->language->get('text_discount');
			$this->data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
			
			$this->data['button_cart'] = $this->language->get('button_cart');
			
			$this->load->model('catalog/review');

			$this->data['tab_description'] = $this->language->get('tab_description');
			$this->data['tab_attribute'] = $this->language->get('tab_attribute');
			$this->data['tab_review'] = sprintf($this->language->get('tab_review'), $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']));
			
			$this->data['product_id'] = $this->request->get['product_id'];
			$this->data['manufacturer'] = $product_info['manufacturer'];
			$this->data['manufacturers'] = $this->url->link('product/manufacturer/product', 'manufacturer_id=' . $product_info['manufacturer_id']);
			$this->data['model'] = $product_info['model'];
			$this->data['reward'] = $product_info['reward'];
			$this->data['points'] = $product_info['points'];
			
			if ($product_info['quantity'] <= 0) {
				$this->data['stock'] = $product_info['stock_status'];
			} elseif ($this->config->get('config_stock_display')) {
				$this->data['stock'] = $product_info['quantity'];
			} else {
				$this->data['stock'] = $this->language->get('text_instock');
			}
			
			$this->load->model('tool/image');

			if ($product_info['image']) {
				$this->data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));
			} else {
				$this->data['popup'] = '';
			}
			
			if ($product_info['image']) {
				$this->data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height'));
			} else {
				$this->data['thumb'] = '';
			}
			
			$this->data['images'] = array();
			
			$results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);
			
			foreach ($results as $result) {
				$this->data['images'][] = array(
					'popup' => $this->model_tool_image->resize($result['image'] , $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')),
					'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'))
				);
			}	
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$this->data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['price'] = false;
			}
						
			if ((float)$product_info['special']) {
				$this->data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$this->data['special'] = false;
			}
			
			if ($this->config->get('config_tax')) {
				$this->data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
			} else {
				$this->data['tax'] = false;
			}
			
			$discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);
			
			$this->data['discounts'] = array(); 
			
			foreach ($discounts as $discount) {
				$this->data['discounts'][] = array(
					'quantity' => $discount['quantity'],
					'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')))
				);
			}
			
			if ($product_info['minimum']) {
				$this->data['minimum'] = $product_info['minimum'];
			} else {
				$this->data['minimum'] = 1;
			}
			
			$this->data['review_status'] = $this->config->get('config_review_status');
			$this->data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
			$this->data['rating'] = (int)$product_info['rating'];
			$this->data['description'] = html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');
			$this->data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);
			
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/quick-info.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/events/quick-info.tpl';
			} else {
				$this->template = 'default/template/events/quick-info.tpl';
			}
			
			$this->response->setOutput($this->render());
		} else {		
      		$this->document->setTitle($this->language->get('text_error'));

      		$this->data['heading_title'] = $this->language->get('text_error');

      		$this->data['text_error'] = $this->language->get('text_error');

      		$this->data['button_continue'] = $this->language->get('button_continue');

      		$this->data['continue'] = $this->url->link('common/home');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
			} else {
				$this->template = 'default/template/error/not_found.tpl';
			}

			$this->response->setOutput($this->render());
    	}
  	}
	
	
	public function captcha() {
		$this->load->library('captcha');
		
		$captcha = new Captcha();
		
		$this->session->data['captcha'] = $captcha->getCode();
		
		$captcha->showImage();
	}	
}
?>
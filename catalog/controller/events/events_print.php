<?php 
class ControllerEventsEventsPrint extends Controller {

	public function index() {
	
		if (!isset($this->request->get['event_id'])) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['base'] = $this->config->get('config_ssl');
		} else {
			$this->data['base'] = $this->config->get('config_url');
		}
		
		
		if (version_compare(VERSION, '1.5.5', '<') == true) {
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$server = HTTPS_IMAGE;
			} else {
				$server = HTTP_IMAGE;
			}	
		} else {
			if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
				$server = $this->config->get('config_ssl');
			} else {
				$server = $this->config->get('config_url');
			}
		}
			
		$this->language->load('account/events');
		$this->language->load('checkout/cart');
		$this->load->model('account/events');
		
		if (isset($this->request->get['event_id'])) {
			$eid = $this->request->get['event_id'];
		} else {
			$eid = '0';
		}
		
		if (isset($this->request->get['akey'])) {
			$akey = $this->request->get['akey'];
		} else {
			$akey = '0';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}

		if (isset($this->request->get['order']) && $this->request->get['order'] == 'sort_desc') {
			$order = SORT_DESC;
			$dir = 'sort_desc';
		} else {
			$order = SORT_ASC;
			$dir = 'sort_asc';
		}
		
		$enabled_test = $this->model_account_events->getEnabledEvents($eid);

		if (!$enabled_test) {
  			$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		$this->data['private'] = false;
		
		$private_test = $this->model_account_events->getPrivateEvents($eid);
		
		if (isset($this->request->get['event_id']) && $private_test) {
			if ($akey != $this->model_account_events->getAccessKey($eid) && (!$private_test)) {
	  			$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
			} elseif ($akey != $this->model_account_events->getAccessKey($eid) && ($private_test)) {
				$this->data['private'] = true;
			} elseif ($akey == $this->model_account_events->getAccessKey($eid) && ($private_test)) {
				$this->data['private'] = false;
			}
		}

		if ($akey != $this->model_account_events->getAccessKey($eid) && !$private_test) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		if (!isset($this->request->get['akey']) && !$private_test) {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}

		if ($this->model_account_events->checkEvents($eid)) {		
			$query = $this->model_account_events->getEventsTitle($eid);
			$title = $query['title'];
			
			$this->document->setTitle($title);
			$this->data['heading_title'] = $title;
		} else {
	  		$this->redirect($this->url->link('events/events_list', '', 'SSL')); 
		}
		
		if ($this->config->get('config_logo') && file_exists(DIR_IMAGE . $this->config->get('config_logo'))) {
			$this->data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$this->data['logo'] = '';
		}
		
		$this->data['text_private_info'] = $this->language->get('text_private_info');
		$this->data['text_send_email'] = $this->language->get('text_send_email');
		$this->data['text_event_status'] = $this->language->get('text_event_status');
		$this->data['text_event_owner'] = $this->language->get('text_event_owner');
		$this->data['name'] = $this->config->get('config_name');
		$this->data['www'] = $this->config->get('config_url');
		$this->data['text_empty'] = $this->language->get('text_empty');
		
		$this->data['store'] = $this->config->get('config_owner');
    	$this->data['address'] = nl2br($this->config->get('config_address'));
    	$this->data['telephone'] = $this->config->get('config_telephone');
					
		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_buy_qty'] = $this->language->get('column_buy_qty');
		$this->data['column_qty_bought'] = $this->language->get('column_qty_bought');
		$this->data['column_qty_rqd'] = $this->language->get('column_qty_rqd');
   		$this->data['column_name'] = $this->language->get('column_name');
   		$this->data['column_model'] = $this->language->get('column_model');
   		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_price'] = $this->language->get('column_price');
   		$this->data['column_total'] = $this->language->get('column_total');
				
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_add_cart'] = $this->language->get('button_add_cart');
		$this->data['button_send_email'] = $this->language->get('button_send_email');
		$this->data['button_create_event'] =  $this->language->get('button_create_event');
		$this->data['button_back'] =  $this->language->get('button_back');
		$this->data['button_print_page'] = $this->language->get('button_print_page');

		$this->data['create'] = $this->url->link('account/events/addEvent', '', 'SSL');
		$this->data['eid'] = $eid;
		$this->data['akey'] = $akey;
		
		$event_query = $this->model_account_events->getEventDetail($eid);
		
		$this->data['end_date'] = date("d/m/Y", strtotime($event_query['end_date']));
		$this->data['start_date'] = date("d/m/Y", strtotime($event_query['start_date']));
		$this->data['event_owner'] = ucfirst($event_query['name']);
		$this->data['event_status'] = (strtotime("now") < strtotime($event_query['start_date']) ? $this->language->get('text_awaiting') : (strtotime("now") > strtotime($event_query['end_date']) ? $this->language->get('text_finished') : $this->language->get('text_in_progress')));
	
		$this->load->model('tool/image');
		
   		$this->data['products'] = array();
		
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);

		if (isset($this->request->get['event_id']) && !$this->data['private']) {
			$products = $this->events->getEventsViewProducts($this->model_account_events->getEventsViewProducts($eid), $eid);
			
			if ($products) {
	      		foreach ($products as $product) {
				
					$product_total = 0;
						
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}			
					
					if ($product['minimum'] > $product_total) {
						$this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
					}				
						
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
					} else {
						$image = '';
					}
					
					$bought_data = array();				
	
					foreach ($product['bought'] as $buyed) {
						
						$bought_data[] = array(
							'bought'  => $buyed['bought_qty']
						);
	   	     		}
						
					$option_data = array();			

	        		foreach ($product['option'] as $option) {
						if ($option['type'] != 'file') {
							$value = $option['option_value'];	
						} else {
							$filename = $this->encryption->decrypt($option['option_value']);
							
							$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
						}
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
   	     		}
					
					// Display prices
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
					
					// Display prices
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
					} else {
						$total = false;
					}
					
					$query = $this->model_account_events->getEventsEndDate($eid);
					
					if (strtotime("now") > strtotime($query['end_date'])) {
						$finished = '';
					} else {
						$finished = 1;
					}		
					
					$comment = $this->model_account_events->getEventComment($eid, $product['key']);
					
   	     			$this->data['products'][] = array(
						'id'	   => $product['product_id'],
						'iid'	   => $product['iid'],
						'key'      => $product['key'],
						'thumb'    => $image,
						'name'     => $product['name'],
						'comment'  => $comment['p_comment'],
 	         			'model'    => $product['model'],
						'bought'   => $buyed['bought_qty'],
						'tax_class_id' => $product['tax_class_id'],
						'finished' => $finished,
						'fred'	   => $product['quantity'] - $buyed['bought_qty'],
						'option'   => $option_data,
						'quantity' => $product['quantity'],
						'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
						'reward'   => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
						'price'    => preg_replace("/[^-0-9\.]/","",$product['price']),
						'total'    => $total,
						'href'     => $this->url->link('product/product', 'product_id=' . $product['product_id'])
					);
				}
			}
		}
	
		if (isset($this->request->get['akey']) && $this->request->get['akey'] != '0') {
			if ($sort == 'name') {
				foreach ($this->data['products'] as $key => $row) {
  	 				$name[$key]  = $row['name'];
				}

				array_multisort($name, $order, $this->data['products']);
			
			} elseif ($sort == 'model') {
				foreach ($this->data['products'] as $key => $row) {
	   				$model[$key]  = $row['model'];
				}
	
				array_multisort($model, $order, $this->data['products']);
				
			} elseif ($sort == 'price') {
				$price= array();
				foreach ($this->data['products'] as $key => $row) {
	   				$price[$key]  = $row['price'];
				}
	
				array_multisort($price, $order, $this->data['products']);
	
			}
		}
		
		$url = '';

		if ($dir == 'sort_asc') {
			$url .= '&order=sort_desc';
		} else {
			$url .= '&order=sort_asc';
		}

		//$this->data['sort_name'] = ($private_test ? $this->url->link('events/events_print', 'event_id=' . $eid) : $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=name' . $url, 'SSL'));
		//$this->data['sort_model'] = ($private_test ? $this->url->link('events/events_print', 'event_id=' . $eid) : $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=model' . $url, 'SSL'));
		//$this->data['sort_price'] = ($private_test ? $this->url->link('events/events_print', 'event_id=' . $eid) : $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=price' . $url, 'SSL'));
		$this->data['sort_name'] = $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=name' . $url, 'SSL');
		$this->data['sort_model'] = $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=model' . $url, 'SSL');
		$this->data['sort_price'] = $this->url->link('events/events_print', 'event_id=' . $eid . '&akey=' . $akey . '&sort=price' . $url, 'SSL');
		$this->data['continue'] = $this->url->link('common/home');
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
	
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_print.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_print.tpl';
		} else {
			$this->template = 'default/template/events/events_print.tpl';
		}
		
		$this->response->setOutput($this->render());		
	}
	
}
?>
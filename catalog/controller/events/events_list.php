<?php 
class ControllerEventsEventsList extends Controller {
	private $error = array();

	public function index() {
	
		$this->language->load('account/events');
		$this->load->model('account/events');
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = '';
		}
		
		if (isset($this->request->get['filter_title'])) {
			$filter_title = $this->request->get['filter_title'];
		} else {
			$filter_title = '';
		}
				
		if (isset($this->request->get['filter_owner'])) {
			$filter_owner = $this->request->get['filter_owner'];
		} else {
			$filter_owner = '';
		}
		
		if (isset($this->request->get['filter_invitee'])) {
			$filter_invitee = $this->request->get['filter_invitee'];
		} else {
			$filter_invitee = '';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'end_date';
		}
		
		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

      	
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('events/events_list'),
        	'separator' => $this->language->get('text_separator')
      	);
		
		$url = '';
			
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}
				
		if (isset($this->request->get['filter_owner'])) {
			$url .= '&filter_owner=' . urlencode(html_entity_decode($this->request->get['filter_owner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_invitee'])) {
			$url .= '&filter_invitee=' . urlencode(html_entity_decode($this->request->get['filter_invitee'], ENT_QUOTES, 'UTF-8'));
		}
								
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
								
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_show'] = $this->language->get('button_show');
		$this->data['button_create_event'] =  $this->language->get('button_create_event');
    	$this->data['button_search'] = $this->language->get('button_search');

		$this->data['entry_search'] = $this->language->get('entry_search');
		$this->data['entry_event_name'] = $this->language->get('entry_event_name');
		$this->data['entry_event_title'] = $this->language->get('entry_event_title');
		$this->data['entry_event_owner'] = $this->language->get('entry_event_owner');
		$this->data['entry_event_invitee'] = $this->language->get('entry_event_invitee');

    	$this->data['text_search'] = $this->language->get('text_search');
			
		$this->data['create'] = $this->url->link('account/events/addEvent', '', 'SSL');
		$this->data['text_no_results'] = $this->language->get('text_no_results');
     	
		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_start_date'] = $this->language->get('column_start_date');
		$this->data['column_end_date'] = $this->language->get('column_end_date');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_type'] = $this->language->get('column_type');
		$this->data['column_action'] = $this->language->get('column_action');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		$this->data['button_show_search'] = $this->language->get('button_show_search');
		$this->data['button_hide_search'] = $this->language->get('button_hide_search');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_title']) || isset($this->request->get['filter_owner']) || isset($this->request->get['filter_invitee'])) {
			$data = array(
				'filter_name'       => $filter_name, 
				'filter_title'      => $filter_title, 
				'filter_owner'  	=> $filter_owner,
				'filter_invitee'  	=> $filter_invitee,
				'sort'            	=> $sort,
				'order'           	=> $order,
				'start'             => ($page - 1) * '20',
				'limit'             => '20'
			);
			
			$event_info = $this->model_account_events->getEventsSearch($data);
			$event_total = $this->model_account_events->getTotalEvents($data);
		
		} else {
			$data = array(
				'sort'	=> $sort,
				'order'	=> $order,
				'start'             => ($page - 1) * '20',
				'limit'             => '20'
			);
		
			$event_info = $this->model_account_events->getEventsList($data);
			$event_total = $this->model_account_events->getTotalEvents($data);
		}
							
		$this->data['events'] = array();
		
				
		if ($event_info) {
			foreach ($event_info as $event) {
			$private_test = $this->model_account_events->getPrivateEvents($event['event_id']);

																			
				$this->data['events'][] = array(
					'event_id'	  => $event['event_id'],
					'title'       => $event['title'],
					'start_date'  => $event['start_date'],
					'end_date'    => $event['end_date'],
					'status'      => (strtotime("now") < strtotime($event['start_date']) ? $this->language->get('text_awaiting') : (strtotime("now") > strtotime($event['end_date']) ? $this->language->get('text_finished') : $this->language->get('text_in_progress'))),		
					'type'    	  => ($event['type'] == 1 ? $this->language->get('text_private') : $this->language->get('text_public')),
					'action' 	  => $this->language->get('button_view'),
					'href'        => ($private_test ? $this->url->link('events/events_view', 'event_id=' . $event['event_id']) : $this->url->link('events/events_view', 'event_id=' . $event['event_id'] . '&akey=' . $event['access_key'], 'SSL'))
				);
			}
		}
		
		$url = '';
			
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}
				
		if (isset($this->request->get['filter_owner'])) {
			$url .= '&filter_owner=' . urlencode(html_entity_decode($this->request->get['filter_owner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_invitee'])) {
			$url .= '&filter_invitee=' . urlencode(html_entity_decode($this->request->get['filter_invitee'], ENT_QUOTES, 'UTF-8'));
		}
		
		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		$this->data['sort_title'] = $this->url->link('events/events_list', 'sort=title' . $url, 'SSL');
		$this->data['sort_start_date'] = $this->url->link('events/events_list', 'sort=start_date' . $url, 'SSL');
		$this->data['sort_end_date'] = $this->url->link('events/events_list', 'sort=end_date' . $url, 'SSL');
		$this->data['sort_type'] = $this->url->link('events/events_list', 'sort=type' . $url, 'SSL');
		
		$url = '';
			
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . urlencode(html_entity_decode($this->request->get['filter_title'], ENT_QUOTES, 'UTF-8'));
		}
				
		if (isset($this->request->get['filter_owner'])) {
			$url .= '&filter_owner=' . urlencode(html_entity_decode($this->request->get['filter_owner'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_invitee'])) {
			$url .= '&filter_invitee=' . urlencode(html_entity_decode($this->request->get['filter_invitee'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_title'] = $filter_title;
		$this->data['filter_owner'] = $filter_owner;
		$this->data['filter_invitee'] = $filter_invitee;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$pagination = new Pagination();
		$pagination->total = $event_total;
		$pagination->page = $page;
		$pagination->limit = '20';
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('events/events_list', $url . '&page={page}');
		
		$this->data['pagination'] = $pagination->render();

		$this->data['continue'] = $this->url->link('common/home', '', 'SSL');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_list.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_list.tpl';
		} else {
			$this->template = 'default/template/events/events_list.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
							
		$this->response->setOutput($this->render());		
	}

	
  	private function validateForm() {
    	if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 32)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}

    	if ((utf8_strlen($this->request->post['title']) < 1) || (utf8_strlen($this->request->post['title']) > 32)) {
      		$this->error['title'] = $this->language->get('error_title');
    	}

    	if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
      		$this->error['email'] = $this->language->get('error_email');
    	}
		
    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
  	}
	
}
?>
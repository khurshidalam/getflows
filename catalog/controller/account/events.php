<?php 
class ControllerAccountEvents extends Controller {
	private $error = array();

	public function index() {
	
		$this->language->load('account/events');
		$this->load->model('account/events');

    	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/events', '', 'SSL');

	  		$this->redirect($this->url->link('account/login', '', 'SSL')); 
    	}  
		
		$this->getEventPage();
	}

	public function installdb() {
	
		$this->load->model('account/events');

		$this->model_account_events->installDB();		
		
		$this->document->setTitle('Install DB');
	}

	public function getEventPage() {
		
		$this->document->setTitle($this->language->get('heading_title'));	
      	
		$this->data['breadcrumbs'] = array();

      	$this->data['breadcrumbs'][] = array(
        	'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
        	'separator' => false
      	); 

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/events', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);
								
		$this->data['heading_title'] = $this->language->get('heading_title');
		
		$this->data['button_new'] = $this->url->link('account/events/addEvent', '', 'SSL');
		$this->data['button_add_new'] = $this->language->get('button_add_new');
		
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_adding_products'] = $this->language->get('text_adding_products');
     	
		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_start_date'] = $this->language->get('column_start_date');
		$this->data['column_end_date'] = $this->language->get('column_end_date');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_type'] = $this->language->get('column_type');
		$this->data['column_action'] = $this->language->get('column_action');
		
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['events'] = array();
		
		$event_info = $this->model_account_events->getEvents($this->customer->getId());
		
		if ($event_info) {
			foreach ($event_info as $event) {
																			
				$this->data['events'][] = array(
					'event_id'	  => $event['event_id'],
					'title'       => $event['title'],
					'start_date'  => $event['start_date'],
					'end_date'    => $event['end_date'],
					'status'      => ($event['status'] == 1 ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),		
					'type'    	  => ($event['type'] == 1 ? $this->language->get('text_private') : $this->language->get('text_public')),
					'delete'	  => $this->url->link('account/events/deleteEvent', 'event_id=' . $event['event_id'] . '&akey=' . $event['access_key'], '', 'SSL'),
					'action'	  => $this->language->get('button_edit'),
					'href'		  => $this->url->link('account/events/editEvent', 'event_id=' . $event['event_id'] . '&akey=' . $event['access_key'], '', 'SSL')
				);
			}
		}	

		$this->data['continue'] = $this->url->link('account/account', '', 'SSL');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/events.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/events.tpl';
		} else {
			$this->template = 'default/template/account/events.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
							
		$this->response->setOutput($this->render());		
	}

	public function deleteEvent() {
		$this->language->load('account/events');
		
		if (!isset($this->request->get['event_id']) || !isset($this->request->get['akey'])) {
	  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
		}

		$eid = $this->request->get['event_id'];
		$akey = $this->request->get['akey'];
		
		$this->load->model('account/events');
		
		if ($this->model_account_events->checkDelete($eid, $akey)) {	
			$this->model_account_events->deleteEvents($eid, $akey);
			$this->session->data['success'] = $this->language->get('text_success_delete');
		} else {
			$this->redirect($this->url->link('account/events', '', 'SSL')); 
			$this->data['error_warning'] = $this->language->get('text_fail_delete');
		}
			
		$this->redirect($this->url->link('account/events', '', 'SSL')); 

	}

	public function addEvent() {
		$this->language->load('account/events');
		
		$accesskey = md5(uniqid(rand()));
		
		$this->document->setTitle($this->language->get('heading_title_add'));
		
		$this->data['heading_title'] = $this->language->get('heading_title_add');
		
		$this->load->model('account/events');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_events->addEvent($this->request->post, $accesskey);

			$this->session->data['success'] = $this->language->get('text_success_add');
			
			$this->redirect($this->url->link('account/events', '', 'SSL')); 
		}

		$this->getForm();
	}
	
	public function editEvent() {
		$this->language->load('account/events');
		$this->load->model('account/events');

		if (!isset($this->request->get['event_id'])) {
	  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
		}
		
		$event_id = $this->request->get['event_id'];
		$akey = $this->request->get['akey'];
		$title = $this->model_account_events->getEventsTitle($event_id);
		
		if (!isset($this->session->data['event' . $event_id])) {
			if ($this->model_account_events->checkEvents($event_id)) {	
				$this->model_account_events->createEventsSessions($event_id);
			} else {
		  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
			}
		}
		
		// Update products
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->events->update($key, $value, $event_id);
			}
			
		}

		$this->document->setTitle($this->language->get('heading_title_edit') . $title['title']);
		
		$this->data['heading_title'] = $this->language->get('heading_title_edit');
						
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_events->editEvent($this->request->post, $event_id);

			$this->session->data['success'] = $this->language->get('text_success_save');
			unset($this->session->data['event' . $event_id]);
			$this->redirect($this->url->link('account/events/editEvent&event_id=' . $event_id . '&akey=' . $akey, '', 'SSL')); 			
		}

		$this->getForm();
	}
	
	public function deleteEventProduct() {
		$this->language->load('account/events');
		$this->load->model('account/events');
		
		if (!isset($this->request->get['event_id'])) {
	  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
		}
		
		if (isset($this->request->get['akey'])) {
			$akey = $this->request->get['akey'];
		} else {
			$akey = '0';
		}
		
		if (isset($this->request->get['product'])) {
			$key = $this->request->get['product'];
		} else {
			$key = '0';
		}
		
		$event_id = $this->request->get['event_id'];
		$title = $this->model_account_events->getEventsTitle($event_id);
		
		if (!isset($this->session->data['event' . $event_id])) {
			if ($this->model_account_events->checkEvents($event_id)) {	
				$this->model_account_events->createEventsSessions($event_id);
			} else {
		  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
			}
		}
		
		// Remove
		if (isset($this->request->get['remove'])) {
			$this->events->remove($this->request->get['remove'], $this->request->get['event_id']);
			$this->model_account_events->deleteEventProduct($event_id, $key);
			
			$this->session->data['success'] = $this->language->get('text_remove');
			$this->redirect($this->url->link('account/events/editEvent&event_id=' . $event_id . '&akey=' . $akey, '', 'SSL')); 			
		}	
		// End remove

		$this->document->setTitle($this->language->get('heading_title_edit') . $title['title']);
		
		$this->data['heading_title'] = $this->language->get('heading_title_edit');
						
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_account_events->editEvent($this->request->post, $event_id);

			$this->session->data['success'] = $this->language->get('text_success_save');
			
			$this->redirect($this->url->link('account/events', '', 'SSL')); 
		}

		$this->getForm();
	}
	
	
	private function getForm() {
	
		$this->load->model('account/events');
	
		if (version_compare(VERSION, '1.5.4.1', '>') == true) {
			$this->document->addScript('catalog/view/javascript/jquery/tabs.js');
			$this->document->addScript('catalog/view/javascript/jquery/colorbox/jquery.colorbox-min.js');
			$this->document->addStyle('catalog/view/javascript/jquery/colorbox/colorbox.css');
		}
			
    	if (!$this->customer->isLogged()) {
	  		$this->session->data['redirect'] = $this->url->link('account/events', '', 'SSL');
			$this->redirect($this->url->link('account/login', '', 'SSL')); 
    	} 
			
		if (isset($this->request->get['event_id'])) {
			$eid = $this->request->get['event_id'];
			$this->data['event_id'] = $this->request->get['event_id'];
		} else {
			$eid = '0';
		}
		
		if (isset($this->request->get['akey'])) {
			$akey = $this->request->get['akey'];
			$this->data['akey'] = $this->request->get['akey'];
		} else {
			$akey = '0';
		}
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'name';
		}
		
		if (isset($this->request->get['order']) && $this->request->get['order'] == 'sort_desc') {
			$order = SORT_DESC;
			$dir = 'sort_desc';
		} else {
			$order = SORT_ASC;
			$dir = 'sort_asc';
		}
		
		if (isset($this->request->get['event_id'])) {
				if ($akey != $this->model_account_events->getAccessKey($eid)) {
	  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
			}
		}
		
		$testID = $this->model_account_events->getEventDetails($eid, $akey);
		
		if (($testID) && ($testID['customer_id'] != $this->customer->getId())) {
	  		$this->redirect($this->url->link('account/events', '', 'SSL')); 
    	} 

		$this->data['text_invitees'] = $this->language->get('text_invitees');
		$this->data['text_public'] = $this->language->get('text_public');
		$this->data['text_private'] = $this->language->get('text_private');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_what_is_heading'] = $this->language->get('text_what_is_heading');
		$this->data['text_what_is_help'] = $this->language->get('text_what_is_help');
		$this->data['text_link'] = $this->language->get('text_link');
		$this->data['share_message'] = $this->language->get('share_message');
		$this->data['text_twitter_title'] = $this->language->get('text_twitter_title');
		$this->data['text_twitter_alt'] = $this->language->get('text_twitter_alt');
		$this->data['text_facebook_title'] = $this->language->get('text_facebook_title');
		$this->data['text_facebook_alt'] = $this->language->get('text_facebook_alt');
		$this->data['text_social_share'] = $this->language->get('text_social_share');
		$this->data['text_social_message'] = $this->language->get('text_social_message');
		$this->data['text_noti_preview'] = $this->language->get('text_noti_preview');
		$this->data['text_my_event'] = $this->language->get('text_my_event');
		$this->data['text_not_published'] = $this->language->get('text_not_published');
		$this->data['text_no_notes'] = $this->language->get('text_no_notes');
		$this->data['text_note'] = $this->language->get('text_note');
		$this->data['text_wait'] = $this->language->get('text_wait');
		$this->data['text_write'] = $this->language->get('text_write');
		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_placeholder'] = $this->language->get('text_placeholder');
		$this->data['text_noti_message_placeholder'] = $this->language->get('text_noti_message_placeholder');
		$this->data['text_no_rsvps'] = $this->language->get('text_no_rsvps');
		$this->data['text_rsvp_disabled'] = $this->language->get('text_rsvp_disabled');
		$this->data['text_notes_disabled'] = $this->language->get('text_notes_disabled');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_notify'] = $this->language->get('tab_notify');
		$this->data['tab_products'] = $this->language->get('tab_products');
		$this->data['tab_share'] = $this->language->get('tab_share');
		$this->data['tab_notes'] =  $this->language->get('tab_notes');
		$this->data['tab_rsvp'] =  $this->language->get('tab_rsvp');
		
		$this->data['column_guest_name'] = $this->language->get('column_guest_name');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_note'] = $this->language->get('column_note');
		$this->data['column_guests'] = $this->language->get('column_guests');

		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_title'] = $this->language->get('entry_title');
    	$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_start_date'] = $this->language->get('entry_start_date');
		$this->data['entry_end_date'] = $this->language->get('entry_end_date');
		$this->data['entry_name'] = $this->language->get('entry_att_name');
		$this->data['entry_att_name'] = $this->language->get('entry_att_name');
		$this->data['entry_att_email'] = $this->language->get('entry_att_email');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_note'] = $this->language->get('entry_note');
		$this->data['entry_captcha'] = $this->language->get('entry_captcha');
		$this->data['entry_message'] = $this->language->get('entry_message');
		$this->data['entry_rsvp'] = $this->language->get('entry_rsvp');
		$this->data['entry_notes'] = $this->language->get('entry_notes');
		$this->data['entry_photo'] = $this->language->get('entry_photo');
		$this->data['entry_web_link'] = $this->language->get('entry_web_link');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add'] = $this->language->get('button_add');
		$this->data['button_remove'] = $this->language->get('button_remove');
		$this->data['button_send'] = $this->language->get('button_send');
		$this->data['button_add_products'] = $this->language->get('button_add_products');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		$name = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		$link = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
		
		$this->data['cust_email'] = $this->customer->getEmail();
		$this->data['cust_name'] = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		$this->data['add_products'] = $this->url->link('account/events_pp', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
		$this->data['link'] = $this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
		$this->data['share_link_encoded'] = urlencode($this->url->link('events/events_view', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL'));
		$this->data['rsvp_status'] = $this->model_account_events->getRSVPstatus($eid, $akey);
		$this->data['note_status'] = $this->model_account_events->getNoteStatus($eid, $akey);
			
 		if (isset($this->session->data['error'])) {
			$this->data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		} else {
			$this->data['error_warning'] = '';
		}
	
 		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}
		
 		if (isset($this->error['title'])) {
			$this->data['error_title'] = $this->error['title'];
		} else {
			$this->data['error_title'] = array();
		}
		
 		if (isset($this->error['email'])) {
			$this->data['error_email'] = $this->error['email'];
		} else {
			$this->data['error_email'] = array();
		}
		
 		if (isset($this->error['start_date'])) {
			$this->data['error_start_date'] = $this->error['start_date'];
		} else {
			$this->data['error_start_date'] = array();
		}
		
 		if (isset($this->error['end_date'])) {
			$this->data['error_end_date'] = $this->error['end_date'];
		} else {
			$this->data['error_end_date'] = array();
		}
		
 		if (isset($this->error['email_attendee'])) {
			$this->data['error_email_attendee'] = $this->error['email_attendee'];
		} else {
			$this->data['error_email_attendee'] = array();
		}
		
 		if (isset($this->error['name_attendee'])) {
			$this->data['error_name_attendee'] = $this->error['name_attendee'];
		} else {
			$this->data['error_name_attendee'] = array();
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', '', 'SSL'),
      		'separator' => false
   		);
		
      	$this->data['breadcrumbs'][] = array(       	
        	'text'      => $this->language->get('text_account'),
			'href'      => $this->url->link('account/account', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
      	);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('account/events', '', 'SSL'),
        	'separator' => $this->language->get('text_separator')
   		);
		
		if (isset($this->request->get['event_id'])) {
	   		$this->data['breadcrumbs'][] = array(
   	    		'text'      => $this->language->get('heading_title_edit'),
				'href'      => $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL'),
        		'separator' => $this->language->get('text_separator')
  	 		);
			} else {
  	 		$this->data['breadcrumbs'][] = array(
   	    		'text'      => $this->language->get('heading_title_add'),
				'href'      => $this->url->link('account/events/addEvent', '', 'SSL'),
        		'separator' => $this->language->get('text_separator')
  	 		);
		}
		
		if (!isset($this->request->get['event_id'])) {
			$this->data['action'] = $this->url->link('account/events/addEvent', '', 'SSL');
		} else {
			$this->data['action'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
			$this->data['send'] = $this->url->link('account/events/send', 'event_id=' . $eid . '&akey=' . $akey, '', 'SSL');
		}
		
		$this->data['save_url'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey . '&save=1', '', 'SSL');
		$this->data['cancel'] = $this->url->link('account/events', '', 'SSL');

		if ((isset($this->request->get['event_id'])) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$event_info = $this->model_account_events->getEventDetails($eid, $akey);
			$event_invitees = $this->model_account_events->getEventInvitees($eid);
			$event_rsvps = $this->model_account_events->getEventRSVPs($eid, $akey);
			
		}
		
		if (!empty($event_info)) {
			$noti_message = sprintf($this->language->get('text_email_intro')) . "\n\n";
			$noti_message .= sprintf($this->language->get('text_email_body'), $name) . "\n\n";
			$noti_message .= sprintf($link) . "\n\n";
			$noti_message .= sprintf($this->language->get('text_email_noti_message'), ($event_info['notify_message'] ? $event_info['notify_message'] : $this->language->get('text_email_no_message'))) . "\n\n";
			$noti_message .= sprintf($this->language->get('text_email_signoff')) . "\n\n";
			$noti_message .= sprintf($this->language->get('text_email_end'), $this->config->get('config_name'));
			$this->data['noti_message_preview'] = nl2br($noti_message);
		}

		if (isset($this->request->post['title'])) {
			$this->data['title'] = $this->request->post['title'];
		} elseif (!empty($event_info)) {
			$this->data['title'] = $event_info['title'];
		} else {
			$this->data['title'] = '';
		}

		if (isset($this->request->post['name'])) {
			$this->data['name'] = $this->request->post['name'];
		} elseif (!empty($event_info)) {
			$this->data['name'] = $event_info['name'];
		} else {
			$this->data['name'] = $this->customer->getFirstname() . ' ' . $this->customer->getLastname();
		}
		
		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($event_info)) {
			$this->data['email'] = $event_info['email'];
		} else {
			$this->data['email'] = $this->customer->getEmail();
		}
		
		if (isset($this->request->post['start_date'])) {
			$this->data['start_date'] = $this->request->post['start_date'];
		} elseif (!empty($event_info)) {
			$this->data['start_date'] = $event_info['start_date'];
		} else {
			$this->data['start_date'] = '';
		}
				
		if (isset($this->request->post['end_date'])) {
			$this->data['end_date'] = $this->request->post['end_date'];
		} elseif (!empty($event_info)) {
			$this->data['end_date'] = $event_info['end_date'];
		} else {
			$this->data['end_date'] = '';
		}
		
		if (isset($this->request->post['type'])) {
			$this->data['type'] = $this->request->post['type'];
		} elseif (!empty($event_info)) {
			$this->data['type'] = $event_info['type'];
		} else {
			$this->data['type'] = '';
		}
		
		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($event_info)) {
			$this->data['status'] = $event_info['status'];
		} else {
			$this->data['status'] = '';
		}
		
		if (isset($this->request->post['enable_rsvp'])) {
			$this->data['enable_rsvp'] = $this->request->post['enable_rsvp'];
		} elseif (!empty($event_info)) {
			$this->data['enable_rsvp'] = $event_info['enable_rsvp'];
		} else {
			$this->data['enable_rsvp'] = '';
		}
		
		if (isset($this->request->post['enable_notes'])) {
			$this->data['enable_notes'] = $this->request->post['enable_notes'];
		} elseif (!empty($event_info)) {
			$this->data['enable_notes'] = $event_info['enable_notes'];
		} else {
			$this->data['enable_notes'] = '';
		}
		
		if (isset($this->request->post['photo'])) {
			$this->data['photo'] = $this->request->post['photo'];
		} elseif (!empty($event_info)) {
			$this->data['photo'] = $event_info['photo'];
		} else {
			$this->data['photo'] = '';
		}
		
		if (isset($this->request->post['web_link'])) {
			$this->data['web_link'] = $this->request->post['web_link'];
		} elseif (!empty($event_info)) {
			$this->data['web_link'] = $event_info['web_link'];
		} else {
			$this->data['web_link'] = '';
		}
		
		if (isset($this->request->post['message'])) {
			$this->data['message'] = $this->request->post['message'];
		} elseif (!empty($event_info)) {
			$this->data['message'] = $event_info['e_comment'];
		} else {
			$this->data['message'] = '';
		}
		
		if (isset($this->request->post['notify_message'])) {
			$this->data['notify_message'] = $this->request->post['notify_message'];
		} elseif (!empty($event_info)) {
			$this->data['notify_message'] = $event_info['notify_message'];
		} else {
			$this->data['notify_message'] = '';
		}
		
		$this->data['attendee'] = array();
				
		if (isset($this->request->post['attendee'])) {
			$this->data['attendees'] = $this->request->post['attendee'];
		} elseif (!empty($event_invitees)) {
			$this->data['attendees'] = $event_invitees;
		} else {
			$this->data['attendees'] = '';
		}
		
		$event_rsvp_guests = array();
		$this->data['rsvps'] = array();

		if (!empty($event_rsvps)) {
			foreach ($event_rsvps as $event_rsvp) {
				$event_rsvp_guests = $this->model_account_events->getEventRSVPguests($eid, $akey, $event_rsvp['email']);

				$this->data['rsvps'][] = array(
					'name'      => $event_rsvp['name'],
					'email'    	=> $event_rsvp['email'],
					'note'      => $event_rsvp['short_note'],
					'guests'    => array_filter($event_rsvp_guests)
				);
			}
		}
			
		$this->language->load('checkout/cart');

		$this->data['column_image'] = $this->language->get('column_image');
   		$this->data['column_name'] = $this->language->get('column_name');
   		$this->data['column_model'] = $this->language->get('column_model');
   		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_price'] = $this->language->get('column_price');
  		$this->data['column_total'] = $this->language->get('column_total');
					
		$this->data['button_remove'] = $this->language->get('button_remove');
						
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
			
		$this->load->model('tool/image');
			
   		$this->data['products'] = array();
		
		$data = array(
			'sort'  => $sort,
			'order' => $order
		);

		if (isset($this->session->data['event' . $eid])) {
			
		$products = $this->events->getEventProducts($eid);
   		foreach ($products as $product) {
			$product_total = 0;

			foreach ($products as $product_2) {
				if ($product_2['product_id'] == $product['product_id']) {
					$product_total += $product_2['quantity'];
				}
			}			
			
			if ($product['minimum'] > $product_total) {
				$this->data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
			}				
				
			if ($product['image']) {
				$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
			} else {
				$image = '';
			}
			
			$option_data = array();
			
       		foreach ($product['option'] as $option) {
			  if ($option['type'] != 'file') {
				$value = $option['option_value'];	
			  } else {
				$filename = $this->encryption->decrypt($option['option_value']);
				$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
			  }
				
			$option_data[] = array(
				'name'  => $option['name'],
				'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
				);
       		}
				
			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
			
			// Display prices
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
			} else {
				$total = false;
			}
			
			$comment = $this->model_account_events->getEventComment($eid, $product['key']);

       		$this->data['products'][] = array(
       			'key'      => $product['key'],
       			'thumb'    => $image,
				'name'     => $product['name'],
				'comment'  => $comment['p_comment'],
       			'model'    => $product['model'],
       			'option'   => $option_data,
       			'quantity' => $product['quantity'],
				'tax_class_id' => $product['tax_class_id'],
       			'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
				'reward'   => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
				'price'    => preg_replace("/[^-0-9\.]/","",$product['price']),
				'total'    => preg_replace("/[^-0-9\.]/","",$total),
				'href'     => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'remove'   => $this->url->link('account/events/deleteEventProduct&amp;event_id=' . $eid . '&amp;akey=' . $akey . '&amp;product=' . $product['key'], 'remove=' . $product['key'], '', 'SSL')
				);
     		}
		}
		
		if ($this->data['products']) {
			if ($sort == 'name') {
				foreach ($this->data['products'] as $key => $row) {
	   				$prodname[$key]  = $row['name'];
				}
				
				array_multisort($prodname, $order, $this->data['products']);
			
			} elseif ($sort == 'model') {
				foreach ($this->data['products'] as $key => $row) {
	   				$model[$key]  = $row['model'];
				}

				array_multisort($model, $order, $this->data['products']);
				
			} elseif ($sort == 'price') {
				$price= array();
				foreach ($this->data['products'] as $key => $row) {
	   				$price[$key]  = $row['price'];
				}
	
				array_multisort($price, $order, $this->data['products']);
	
			} elseif ($sort == 'total') {
				$totalsort= array();
				foreach ($this->data['products'] as $key => $row) {
	   				$totalsort[$key]  = $row['total'];
				}
	
				array_multisort($totalsort, $order, $this->data['products']);
			}
		}
		
		$url = '';

		if ($dir == 'sort_asc') {
			$url .= '&order=sort_desc';
		} else {
			$url .= '&order=sort_asc';
		}
		$this->data['sort_name'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey . '&sort=name' . $url, '', 'SSL');
		$this->data['sort_model'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey . '&sort=model' . $url, '', 'SSL');
		$this->data['sort_price'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey . '&sort=price' . $url, '', 'SSL');
		$this->data['sort_total'] = $this->url->link('account/events/editEvent', 'event_id=' . $eid . '&akey=' . $akey . '&sort=total' . $url, '', 'SSL');
		$this->data['continue'] = $this->url->link('common/home');
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->data['continue'] = $this->url->link('common/home');
		$this->data['checkout'] = $this->url->link('checkout/checkout', '', 'SSL');		
		
		if (isset($this->request->get['event_id'])) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/events_edit.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/events_edit.tpl';
			} else {
				$this->template = 'default/template/account/events_edit.tpl';
			}
		} else {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/events_add.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/account/events_add.tpl';
			} else {
				$this->template = 'default/template/account/events_add.tpl';
			}
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'	
		);
							
		$this->response->setOutput($this->render());		
	}
	
  	private function validateForm() {
    	if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 32)) {
      		$this->error['name'] = $this->language->get('error_name');
    	}

    	if ((utf8_strlen($this->request->post['title']) < 1) || (utf8_strlen($this->request->post['title']) > 50)) {
      		$this->error['title'] = $this->language->get('error_title');
    	}

    	if ((utf8_strlen($this->request->post['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $this->request->post['email'])) {
      		$this->error['email'] = $this->language->get('error_email');
    	}

		if (!$this->request->post['start_date']) {
      		$this->error['start_date'] = $this->language->get('error_start_date');
    	}
		
		if (!$this->request->post['end_date']) {
      		$this->error['end_date'] = $this->language->get('error_end_date');
    	}
		
		if (isset($this->request->post['attendee'])) {
			foreach ($this->request->post['attendee'] as $key => $value) {
		    	if ((utf8_strlen($value['email']) > 96) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $value['email'])) {
      				$this->error['email_attendee'][$key] = $this->language->get('error_email');
    			}
			}
		}
		
		if (isset($this->request->post['attendee'])) {
			foreach ($this->request->post['attendee'] as $key => $value) {
    			if ((utf8_strlen($value['name']) < 1) || (utf8_strlen($value['name']) > 32)) {
      				$this->error['name_attendee'][$key] = $this->language->get('error_name');
    			}
			}
		}		

    	if (!$this->error) {
      		return true;
    	} else {
      		return false;
    	}
  	}
	
	public function send() { 
		$this->load->language('account/events');
		$this->load->model('account/events');
		$name = $this->model_account_events->getEventsName($this->request->get['event_id']);
		$link = $this->url->link('events/events_view', 'event_id=' . $this->request->get['event_id'] . '&akey=' . $this->request->get['akey'], '', 'SSL');
		$notify_message = $this->model_account_events->getNotifyMessage($this->request->get['event_id']);

		if (isset($this->request->post['selected'])) {
			foreach ($this->request->post['selected'] as $email) {
			
				$message = sprintf($this->language->get('text_email_intro')) . "\n\n";
				$message .= sprintf($this->language->get('text_email_body'), $name['name']) . "\n\n";
				$message .= sprintf($link) . "\n\n";
				$message .= sprintf($this->language->get('text_email_noti_message'), ($notify_message['notify_message'] ? $notify_message['notify_message'] : $this->language->get('text_email_no_message'))) . "\n\n";
				$message .= sprintf($this->language->get('text_email_signoff')) . "\n\n";
				$message .= sprintf($this->language->get('text_email_end'), $this->config->get('config_name'));
				$subject = sprintf($this->language->get('text_email_subject'), $name['name']);

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($email);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject($subject);
				$mail->setText(html_entity_decode($message, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			
				$this->session->data['success'] = $this->language->get('text_email_noti_sent');
		//		$this->redirect($this->url->link('account/events/editEvent', 'event_id=' . $this->request->get['event_id'] . '&akey=' . $this->request->get['akey'], '', 'SSL'));
				
			}
		} else {
      		$this->session->data['error'] = $this->language->get('error_mail');
		//	$this->redirect($this->url->link('account/events/editEvent', 'event_id=' . $this->request->get['event_id'] . '&akey=' . $this->request->get['akey'], '', 'SSL'));
		}
		
			$this->redirect($this->url->link('account/events/editEvent', 'event_id=' . $this->request->get['event_id'] . '&akey=' . $this->request->get['akey'], '', 'SSL'));
	}
		
	public function addProduct() { 
		$this->language->load('account/events');
		$this->load->model('account/events');

		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
			
			if (!$json) {
				$this->events->addEventProduct($this->request->post['product_id'], $this->request->post['event_id'], $quantity, $option);
				$this->model_account_events->addEventProducts($this->request->post);

				$json['success'] = sprintf($this->language->get('text_success'), $product_info['name']);
				
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				
				// Totals
				$this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array(); 
					
					$results = $this->model_setting_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
				
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
						
						$sort_order = array(); 
					  
						foreach ($total_data as $key => $value) {
							$sort_order[$key] = $value['sort_order'];
						}
			
						array_multisort($sort_order, SORT_ASC, $total_data);			
					}
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		$this->response->setOutput(json_encode($json));		
	}
	
	public function checkOptions() { 
		$this->language->load('account/events');
		$this->load->model('account/events');

		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
			
			if (!$json) {
				$json['success'] = $product_info['product_id'];
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		$this->response->setOutput(json_encode($json));		
	}
	
	public function eventChooser() { 
		$this->language->load('account/events');
		$this->load->model('account/events');
		
		$this->data['text_choose_event'] = $this->language->get('text_choose_event');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_select'] = $this->language->get('button_select');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = '0';
		}
		
		$this->data['product_id'] = $product_id;
		
		$this->data['events'] = array();
		
		$results = $this->model_account_events->getEvents($this->customer->getId());
		
		if ($results) {
			foreach ($results as $result) {
   	    		$this->data['events'][] = array(
   	    			'event_id' => $result['event_id'],
   	    			'title'     => $result['title'],
   	    			'start'    => $result['start_date'],
   	    			'end'      => $result['end_date']
					);
			}
		} else {
			$this->data['text_no_events'] = $this->language->get('text_no_events');
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_chooser.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_chooser.tpl';
		} else {
			$this->template = 'default/template/events/events_chooser.tpl';
		}
		$this->response->setOutput($this->render());										
	}
	
	public function eventChooserCP() { 
		$this->language->load('account/events');
		$this->load->model('account/events');
		
		$this->data['text_choose_event'] = $this->language->get('text_choose_event');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_select'] = $this->language->get('button_select');
		
		if (isset($this->request->get['product_id'])) {
			$product_id = $this->request->get['product_id'];
		} else {
			$product_id = '0';
		}
		
		$this->data['product_id'] = $product_id;
		
		$this->data['events'] = array();
		
		$results = $this->model_account_events->getEvents($this->customer->getId());
		
		if ($results) {
			foreach ($results as $result) {
   	    		$this->data['events'][] = array(
   	    			'event_id' => $result['event_id'],
   	    			'title'     => $result['title'],
   	    			'start'    => $result['start_date'],
   	    			'end'      => $result['end_date']
					);
			}
		} else {
			$this->data['text_no_events'] = $this->language->get('text_no_events');
		}

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/events/events_chooser_cp.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/events/events_chooser_cp.tpl';
		} else {
			$this->template = 'default/template/events/events_chooser_cp.tpl';
		}
		$this->response->setOutput($this->render());										
	}

	public function eventChooserAddProduct() { 
		$this->language->load('account/events');
		$this->load->model('account/events');

		$json = array();
		
		if (isset($this->request->post['product_id'])) {
			$product_id = $this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
		
		if (isset($this->request->get['event_id'])) {
			$event_id = $this->request->get['event_id'];
		} else {
			$event_id = '0';
		}
		
		$this->load->model('catalog/product');
						
		$product_info = $this->model_catalog_product->getProduct($product_id);
		
		if ($product_info) {			
			if (isset($this->request->post['quantity'])) {
				$quantity = $this->request->post['quantity'];
			} else {
				$quantity = 1;
			}
														
			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();	
			}
			
			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);
			
			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}
			
			if (!$json) {
				$this->events->addEventProductFromChooser($this->request->post['product_id'], $event_id, $quantity, $option);
				$this->model_account_events->addEventProductsFromChooser($this->request->post, $event_id);

				$json['success'] = sprintf($this->language->get('text_success'), $product_info['name']);
				
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);
				
				// Totals
				$this->load->model('setting/extension');
				
				$total_data = array();					
				$total = 0;
				$taxes = $this->cart->getTaxes();
				
				// Display prices
				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$sort_order = array(); 
					
					$results = $this->model_setting_extension->getExtensions('total');
					
					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}
					
					array_multisort($sort_order, SORT_ASC, $results);
					
					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('total/' . $result['code']);
				
							$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
						}
						
						$sort_order = array(); 
					  
						foreach ($total_data as $key => $value) {
							$sort_order[$key] = $value['sort_order'];
						}
			
						array_multisort($sort_order, SORT_ASC, $total_data);			
					}
				}
				
				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total));
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
			}
		}
		$this->response->setOutput(json_encode($json));		
	}
	
	public function notes() {
    	$this->language->load('account/events');
		
		$this->load->model('account/events');

		$this->data['text_on'] = $this->language->get('text_on');
		$this->data['text_no_notes'] = $this->language->get('text_no_notes');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}  
		
		$this->data['notes'] = array();
		
		$notes_total = $this->model_account_events->getTotalNotesByEvent_id_Account($this->request->get['event_id'], $this->request->get['akey']);
			
		$results = $this->model_account_events->getNotesByEventId_Account($this->request->get['event_id'], $this->request->get['akey'], ($page - 1) * 5, 5);

		foreach ($results as $result) {
        	$this->data['notes'][] = array(
        		'author'     => $result['author'],
				'text'       => $result['text'],
        		'notes'    	 => sprintf($this->language->get('text_notes'), (int)$notes_total),
				'note_id'	 => $result['note_id'],
				'event_id'	 => $result['event_id'],
				'akey'		 => $result['access_key'],
        		'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
        	);
      	}			

		$pagination = new Pagination();
		$pagination->total = $notes_total;
		$pagination->page = $page;
		$pagination->limit = 5; 
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('account/events/notes', 'event_id=' . $this->request->get['event_id'] . '&page={page}');
			
		$this->data['pagination'] = $pagination->render();
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/events_notes_account.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/account/events_notes_account.tpl';
		} else {
			$this->template = 'default/template/account/events_notes_account.tpl';
		}
		
		$this->response->setOutput($this->render());
	}
	
	public function write_note() {
		$this->language->load('account/events');
		
		$this->load->model('account/events');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			if ((utf8_strlen($this->request->post['text']) < 5) || (utf8_strlen($this->request->post['text']) > 2000)) {
				$json['error'] = $this->language->get('error_text');
			}
	
			if (!isset($json['error'])) {
				$this->model_account_events->addNote($this->request->get['event_id'], $this->request->get['akey'], $this->request->post);
				
				$json['success'] = $this->language->get('text_success_note');
			}
		}
		
		$this->response->setOutput(json_encode($json));
	}
	
	public function remove_note() {
		$this->language->load('account/events');
		
		$this->load->model('account/events');
		
		$json = array();
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
	
				$this->model_account_events->removeNote($this->request->get['event_id'], $this->request->get['akey'], $this->request->post);
				
				$json['success'] = $this->language->get('text_success_note');
			
		}
		
		$this->response->setOutput(json_encode($json));
	}
}
?>
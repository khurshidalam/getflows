<?php echo $header; ?>
<div class="catPagConAra">
<div class="innerwrapper">
<!--breadcrumb open-->
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
<!--breadcrumb closed-->
  
<h1 class="prdct_name"><?php echo $heading_title; ?></h1>

<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content">
  <?php if ($categories) { ?>
 
   	
  
<div style="width:100%; float:left; margin-top:12px;" > 
 <div id="horizontalTab">
            <div class="resp-tabs-container">
            <!--item part open-->
 			 <div class="product-list">
    <?php foreach ($categories as $category) {  ?>
    
    
    
    <div class="prodctPrt">
    
          <!--left part open-->
      <div class="lftPrt">
      	<div class="proInfo">
      <!--<div class="name"><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></div>-->
<div class="name"><a href="<?php echo $category['href']; ?>">
<?php echo $category['name']; ?></a></div>
      </div><!--proInfo div closed-->
      </div>
      <!--left part closed-->
        <!--right part open-->
    </div><!--part closed-->
    <?php } ?>
  </div>
  			<!--item area closed-->
   
                        <!--item part open-->
 			<div class="product-list">
                            <?php echo $content_bottom; ?>
                        </div>
  			<!--item area closed-->
                        
                        
                        
                        
                           <!--item part open-->
 			<div class="product-list"> 
                        <?php echo $content_top; ?>
                        </div>
  			<!--item area closed-->
                        
            </div>
        </div>
  <?php } ?>
  </div>

  </div>
  </div>
  </div>
  
  
  
  
  
  
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			
			
			
			html = '<div class="lftPrt">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			html += '</div>';
			
			html += '<div class="proInfo">';
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			//html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
                        
			html += '</div>';
			
			
						
			
			
			
					
			
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			html += '</div>';
			
						
			$(element).html(html);
		});	
			
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
						
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.totalStorage('display', 'grid');
	}
}


view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>
<div id="footer_outer">
<div id="footer">
<div class="innerwrapper">
  <?php if ($informations) { ?>
  <div class="column">
    <h3><?php echo $text_information; ?></h3>
    <ul>
      <li><a href="">Home</a></li>
      <li><a href="about-us">About Us</a></li>
      <li><a href="sitemap"><?php echo $text_sitemap; ?></a></li>
<!--      <?php foreach ($informations as $information) { ?>
      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      <?php } ?>-->
    </ul>
  </div>
  <?php } ?>
  <div class="column">
    <h3><?php echo $text_about; ?></h3>
    <ul>
      <li><a href="kit-options">Kit Options</a></li>
      <li><a href="why-floss">Why Floss</a></li>
      <li><a href="contact-us"><?php echo $text_contact; ?></a></li>
     
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_account; ?></h3>
    <ul>
      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <div class="column-rgt">
       <h3 class="last">
    <span id="txtHint"> <h3 class="last">Subscribe Newsletter</h3></span></h3>
    
    <script>
function showHint() {
      var email = document.getElementById('newsletteremail');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
    alert('Please provide a valid email address');
    email.focus;
    return false;
 }
 
  var str=document.getElementById('newsletteremail').value;
  $.post('index.php?route=information/subscribetonewsletter',{'email':str},
         function(data)
         {
          alert(data);
          });
}
</script>
    
    
    
    <form action="" method="post" >
    <input name="email" type="text" class="newslter_input" placeholder="Email address..." id="newsletteremail" />
 <input name="submit" type="button" class="foot_signupbtn" value="SignUp" onclick="showHint()" />

    </form>
    <div class="connect-get"><img src="catalog/view/theme/getfloss/images/connect-text.png" /></div>
    <div class="footer_link">
  <ul>
  <li><a href="" target="_blank"><img src="catalog/view/theme/getfloss/images/footer-link/facebook-icon.png" /></a></li>
  <li><a href="" target="_blank"><img src="catalog/view/theme/getfloss/images/footer-link/twitter-icon.png" /></a></li>
  <li><a href="" target="_blank"><img src="catalog/view/theme/getfloss/images/footer-link/youtube-icon.png" /></a></li>
  <li><a href="" target="_blank"><img src="catalog/view/theme/getfloss/images/footer-link/google-icon.png"  /></a></li>
  </ul>
  </div>
    
  </div>

  
  <!--bottom area open-->
  <div class="footer_btm_area">
  
  <div id="powered">
    
   &copy; 2014 Copyright 2014 - Get Floss Dental Products - All rights reserved. <?php
//if ($this->request->get['route'] == 'common/home')
if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') {
echo  " &nbsp;|&nbsp; <a href='privacy-policy' target='_blank'>Privacy Policy</a> &nbsp;|&nbsp; <a href='terms-conditions' target='_blank'>Terms &amp; Conditions</a>";

}?>
     </div>
  <?php if (isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') {?>
     <div class="website-designby" style="display:none">Website design by: B3NET.com</div>
  <?php }?>
  
  </div>
  <!--bottom area closed-->
  
</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!--<div id="powered"><?php echo $powered; ?></div>-->
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
</div>
</div>

</div>
<script>
$(window).load(function() {
    $("#flexiselDemo4").flexisel({
        clone:false
    });
	$("#flexiselDemo5").flexisel({
        clone:false,
		visibleItems: 5
    });
    
});
</script>
<script type="text/JavaScript">
// prepare the form when the DOM is ready 
$(document).ready(function() {
	$("#gallery-pic li img").hover(function(){
		$('#main-img').attr('src',$(this).attr('src').replace('thumb/', ''));
	});
	var imgSwap = [];
	 $("#gallery-pic li img").each(function(){
		imgUrl = this.src.replace('thumb/', '');
		imgSwap.push(imgUrl);
	});
	$(imgSwap).preload();
});
$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
	
	
	if($("#gallery-scroll-area").length){
        
        // Declare variables
        var totalImages = $("#gallery-scroll-area > li").length, 
            imageWidth = $("#gallery-scroll-area > li:first").outerWidth(true),
            totalWidth = imageWidth * totalImages,
            visibleImages = Math.round($("#gallery-wrap-area-scroll").width() / imageWidth),
            visibleWidth = visibleImages * imageWidth,
            stopPosition = (visibleWidth - totalWidth);
            
        $("#gallery-scroll-area").width(totalWidth);
        
        $("#gallery-scroll-area-prev").click(function(){
            if($("#gallery-scroll-area").position().left < 0 && !$("#gallery-scroll-area").is(":animated")){
                $("#gallery-scroll-area").animate({left : "+=" + imageWidth + "px"});
            }
            return false;
        });
        
        $("#gallery-scroll-area-next").click(function(){
			
            if($("#gallery-scroll-area").position().left > stopPosition && !$("#gallery").is(":animated")){
                $("#gallery-scroll-area").animate({left : "-=" + imageWidth + "px"});
            }
            return false;
        });
    }
}
</script>

<!-- Searchomatrix Tracking Code --> 

<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://www.searchomatrix.com/" : "http://www.searchomatrix.com/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 87);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://www.searchomatrix.com/piwik.php?idsite=87" style="border:0" alt="" /></p></noscript>

<!-- End Searchomatrix Tracking Code -->

</body></html>
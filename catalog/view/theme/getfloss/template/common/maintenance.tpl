<?php echo $header; ?>
<link href="catalog/view/theme/getfloss/stylesheet/maintenance-style.css" rel="stylesheet" type="text/css" />
<div id="content">
<div class="mainten_main_box">
<div class="logo_box"><a><img src="catalog/view/theme/getfloss/maintenance-images/ambermarketing-logo.png"></a></div>
<div class="contbox_area">

<div class="mainten_txt_1">This website is under maintenance</div>
<div class="mainten_txt_2">The service you're looking for is unavailable at the moment. We'll be back soon...</div>
<div class="mainten_txt_3">For more information contact us:</div>

<div class="mainten_info_area">
	<div class="mainten_info_wrap">
    	<ul>
            <li><img src="catalog/view/theme/getfloss/maintenance-images/mainten-icon-1.png" width="26" height="27"><p>506 Clubhouse Ave Newport Beach, CA 92663</p></li>
            <li><img src="catalog/view/theme/getfloss/maintenance-images/mainten-icon-2.png" width="26" height="27"><p>info@getfloss.com</p></li>
            <li><img src="catalog/view/theme/getfloss/maintenance-images/mainten-icon-3.png" width="26" height="27"><p><a>949-533-7879</a></p></li>
        </ul>
    </div>
</div>


</div>



</div>
</div>
<?php echo $footer; ?>
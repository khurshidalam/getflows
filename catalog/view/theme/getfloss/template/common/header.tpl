<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta name="google-site-verification" content="MnuptwgFSEPdoDWGugfMEW-S2sprRWs_8GtLgDUPrRk" />
<!--[if lt IE 9]>
<style>body {min-width: 760px;}#columns {zoom: 1;}.p7PMMv19 ul ul {border: 1px solid #444;}</style>
<![endif]-->
<!--[if lt IE 8]>
<style>.three-column-column2 {width: 30%;}.main-content {width: 59%;}</style>
<![endif]-->
<!--[if lte IE 6]>
<style>#masthead, #columns, #footer {width: 980px;}</style>
<![endif]-->
<title><?php echo $title; ?> | getfloss.com</title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script src="catalog/view/theme/getfloss/js/jquery.flexslider-min.js"></script>
<script type="text/javascript" src="catalog/view/theme/getfloss/js/jquery-selectify.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>
<link href="catalog/view/theme/getfloss/css/custom.css" rel="stylesheet" type="text/css" />
<link href="catalog/view/theme/getfloss/css/blue-mobile.css" rel="stylesheet" type="text/css" />
<script src="catalog/view/theme/getfloss/js/blue-mobile.js"></script> 

<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>

<!-- custom css and jss start-->
 <link href="catalog/view/theme/getfloss/css/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/getfloss/css/mobile2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/css/mobile.css" />
<!--<script src="catalog/view/theme/getfloss/js/icheck.js"></script>-->


<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/css/easy-responsive-tabs.css" />
<script src="catalog/view/theme/getfloss/js/easyResponsiveTabs.js"></script>

<style type="text/css">
        .demo {
            width: 980px;
            margin: 0px auto;
        }
        .demo h1 {
                margin:33px 0 25px;
            }
        .demo h3 {
                margin: 10px 0;
            }
        pre {
            background: #fff;
        }
        @media only screen and (max-width: 780px) {
        .demo {
                margin: 5%;
                width: 90%;
         }
        .how-use {
                float: left;
                width: 300px;
                display: none;
            }
        }
        #tabInfo {
            display: none;
        }
    </style>


<link href="catalog/view/theme/getfloss/css/dropdown-list.css" rel="stylesheet">
<script src="catalog/view/theme/getfloss/js/dropdown-list-jquery.js"></script>
<script src="catalog/view/theme/getfloss/js/dropdown-list-jquery-2.js"></script>

<div style="display: none;">
        
<script type="text/javascript" src="catalog/view/theme/getfloss/js/jquery.flexisel.js"></script>
<link href="catalog/view/theme/getfloss/css/flexisel-style.css" rel="stylesheet" type="text/css" />

<?php //if ($this->request->get['route'] != 'product/product'){ ?>
<script src="catalog/view/theme/getfloss/js/jquery-1.9.1.min.js"></script>
<script src="catalog/view/theme/getfloss/js/owl.carousel.js"></script>
<script src="catalog/view/theme/getfloss/js/brand-carousel.js"></script>

<?php //} ?>
<link href="catalog/view/theme/getfloss/css/brand-carousel-style.css" rel="stylesheet">
<?php //if ($this->request->get['route'] != 'product/product'){ ?>       
<script src="catalog/view/theme/getfloss/js/prduct-thumb-carousel.js"></script>
<?php //} ?>
<link href="catalog/view/theme/getfloss/css/product-thumb-carousel-style.css" rel="stylesheet">
<link href="catalog/view/theme/getfloss/css/owl.carousel.css" rel="stylesheet">
<link href="catalog/view/theme/getfloss/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="catalog/view/theme/getfloss/css/jquery.fancybox-1.3.1.css" media="screen" />
<?php //if ($this->request->get['route'] != 'product/product'){ ?>  
  <script type="text/javascript" src="catalog/view/theme/getfloss/js/jquery.fancybox-1.3.1.js"></script>
    <script type="text/javascript">
    // Fancybox specific
    // To make images pretty. Not important
    $(document).ready(function(){
		var $p = jQuery.noConflict();
        $p(".gallery__link").fancybox({
            'titleShow'     : false,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
    });
    </script>
    
<link rel="stylesheet" href="catalog/view/theme/getfloss/css/selectify.css" />
<script type="text/javascript">
$(window).ready(function() {	
$(".drop-se").selectify();
});
</script>



<script src="catalog/view/theme/getfloss/js/prouct-detail-thumb-js.js"></script>

<script type="text/javascript">
    
           jQuery(document).ready(function() {
                jQuery(".dropdown img.flag").addClass("flagvisibility");
    
                jQuery(".dropdown dt a").click(function() {
                    jQuery(".dropdown dd ul").toggle();
                });
                            
                jQuery(".dropdown dd ul li a").click(function() {
                    var text = jQuery(this).html();
                    jQuery(".dropdown dt a span").html(text);
                    jQuery(".dropdown dd ul").hide();
                   // jQuery("#result").html("Selected value is: " + getSelectedValue("sample"));
                });
    
    
                jQuery(".dropdown1 img.flag").addClass("flagvisibility");
    
                jQuery(".dropdown1 dt a").click(function() {
                    jQuery(".dropdown1 dd ul").toggle();
                });
                            
                jQuery(".dropdown1 dd ul li a").click(function() {
                    var text = jQuery(this).html();
                    var added = '<img class="pull" border="0" align="middle" alt="arrow" style="float:right;margin:0;" src="catalog/view/theme/getfloss/images/lan-arrow.png">';
                    jQuery(".dropdown1 dt a span").html(text+added);
                    jQuery(".dropdown1 dd ul").hide();
                   // jQuery("#result").html("Selected value is: " + getSelectedValue("sample"));
                });
				
                jQuery(".dropdown2 img.flag").addClass("flagvisibility");
    
                jQuery(".dropdown2 dt a").click(function() {
                    jQuery(".dropdown2 dd ul").toggle();
                });
                            
                jQuery(".dropdown2 dd ul li a").click(function() {
                    var text = jQuery(this).html();
                    jQuery(".dropdown2 dt a span").html(text);
                    jQuery(".dropdown2 dd ul").hide();
                   // jQuery("#result").html("Selected value is: " + getSelectedValue("sample"));
                });                            
                function getSelectedValue(id) {
                    return jQuery("#" + id).find("dt a span.value").html();
                }
    
                jQuery(document).bind('click', function(e) {
                    var $clicked = jQuery(e.target);
                    if (! $clicked.parents().hasClass("dropdown"))
                        jQuery(".dropdown dd ul").hide();
                    if (! $clicked.parents().hasClass("dropdown1"))
                        jQuery(".dropdown1 dd ul").hide();
					if (! $clicked.parents().hasClass("dropdown2"))
                        jQuery(".dropdown2 dd ul").hide();	
                });
    
    
                jQuery("#flagSwitcher").click(function() {
                    jQuery(".dropdown img.flag").toggleClass("flagvisibility");
                });
            });
    </script>

	<link rel="stylesheet" href="catalog/view/theme/getfloss/css/flexslider.css" type="text/css" media="screen" />
	<script type="text/javascript">
		$(window).load(function() {
			$('.flexslider').flexslider();
		});
	</script>

<?php //} ?>
</div>

<script type="text/javascript">
  function googleTranslateElementInit() {
    new google.translate.TranslateElement({pageLanguage: 'en', autoDisplay: false},     'google_translate_element'); //remove the layout
  }
</script>
 <script type="text/javascript" src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>


<script type="text/javascript">
function triggerHtmlEvent(element, eventName)
{
    var event;
    if(document.createEvent) {
        event = document.createEvent('HTMLEvents');
        event.initEvent(eventName, true, true);
        element.dispatchEvent(event);
    }
    else {
    event = document.createEventObject();
        event.eventType = eventName;
        element.fireEvent('on' + event.eventType, event);
    }
}

</script> 
<script>
function flag(lang)
{
var lang = lang;
    $('#google_translate_element select option').each(function(){
	if($(this).text().indexOf(lang) > -1) {
        $(this).parent().val($(this).val());
        var container = document.getElementById('google_translate_element');
        var select = container.getElementsByTagName('select')[0];
		triggerHtmlEvent(select, 'change');
    }
    });	
} 

</script>
<!----------------------google analytics code start ------------------------------>
 <?php echo $google_analytics; ?>
<!----------------------google analytics code end ------------------------------>

</head>
<body>
 



        
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
               // var $info = $('#tabInfo');
               // var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });


    });
</script>
<div id="container">

<!-- header area open-->
<div class="headerArea">
<div class="innerwrapper">
<div id="header">
<!--mobile menu-->
<div class="resmenuAra">
<div href="#blue-mobile-menu"></div>
<ul id="blue-mobile-menu">
<li><a href="">Home</a></li>
<li><a href="javascript:void(0)">Store</a>
<div>
<ul>
<?php
foreach ($categories as $category) { ?>	
<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
<?php }?>
</ul>
</div>
</li>
<li><a href="about-us">About Us</a></li>
<li><a href="kit-options">Kit Options</a></li>
<li><a href="javascript:void(0)" >Why Floss</a>
<div>
<ul>
<li><a href="why-floss">Why Floss</a></li>
<li><a href="faq">FAQ</a></li>
<li><a href="mission-statement">Mission Statement</a></li>
</ul>
</div>
</li>
<li><a href="contact-us">Contact Us</a></li>
</ul>
</div>
  <!--mobile menu closed-->
  <?php if ($logo) { ?>
  <div id="logo"><a href="">
  <!--<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />-->
  <img src="catalog/view/theme/getfloss/images/logo.png" alt="" title="" border="0"> </a></div>
  <?php } ?>
  <div class="language">
  <dl id="ShopperFlags" class="dropdown1">
<dt><a href="javascript:void(0)"  title="English"><span><img class="flag" src="catalog/view/theme/getfloss/images/us.png" alt="English" /> English<img src="catalog/view/theme/getfloss/images/lan-arrow.png" style="float:right;margin:0;" border="0" align="middle" class="pull" alt="arrow" /></span></a>
</dt>
<dd>
<ul>
<li><a data-lang="English" onclick="flag('English');" title="English"><img class="flag" src="catalog/view/theme/getfloss/images/us.png" alt="English" />English</a></li>
<li><a data-lang="French" onclick="flag('French');" title="French"><img class="flag" src="catalog/view/theme/getfloss/images/fr.png" alt="French" />French</a></li>
<li><a data-lang="German" onclick="flag('German');" title="German"><img class="flag" src="catalog/view/theme/getfloss/images/de.png" alt="German" />German</a></li>
<li><a data-lang="Spanish" onclick="flag('Spanish');" title="Spanish"><img class="flag" src="catalog/view/theme/getfloss/images/es.png" alt="Spanish" />Spanish</a></li>
<li><a data-lang="Russian" onclick="flag('Russian');" title="Russian"><img class="flag" src="catalog/view/theme/getfloss/images/ru.png" alt="Russian" />Russian</a></li>
</ul>
</dd>
</dl>
  </div>
<div id="google_translate_element" style="display:none;"></div>
  
  <!--<div class="currency">
  <dl id="ShopperPressCurrency" class="dropdown">
<dt><a href="javascript:void(0)"><span><img src="catalog/view/theme/getfloss/images/money_dollar.png" border="0" alt="a" class="flag"  />US Dollar<img src="catalog/view/theme/getfloss/images/lan-arrow.png" style="float:right;margin:0;" border="0" align="middle" class="pull" alt="arrow" /></span></a>
</dt>
<dd>
<ul>
<li><a onclick="$('input[name=\'currency_code\']').attr('value', 'USD'); $('#curreny_form').submit();" title="US Dollar"><img src="catalog/view/theme/getfloss/images/money_dollar.png" border="0" class="flag" alt="a" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;  US Dollar</a></li>
<li><a onclick="$('input[name=\'currency_code\']').attr('value', 'GBP'); $('#curreny_form').submit();" title="Pound Sterling"><img src="catalog/view/theme/getfloss/images/money_pound.png" border="0" class="flag" alt="a" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;  UK Pound</a></li>
<li><a onclick="$('input[name=\'currency_code\']').attr('value', 'EUR'); $('#curreny_form').submit();" title="Euro"><img src="catalog/view/theme/getfloss/images/money_euro.png" border="0" class="flag" alt="a" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp; Euro</a></li>
</ul>
</dd>
</dl>
</div>-->
  <?php echo $language; ?>
  <?php echo $currency; ?>
  <div id="welcome-mobile">
  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
    <!--<a>Sign In</a>  or  <a>Create an Account</a> -->
  </div>
  </div>
  <div id="welcome-desktop">
  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
    <!--<a>Sign In</a>  or  <a>Create an Account</a> -->
  </div>
  </div>
    <?php echo $cart; ?>
  <div id="search">
    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
    <div class="button-search"></div>
  </div>
      <div class="logo-tagline">Convenient, High Quality &amp; Low Cost, Toothbrush and Floss Kits Delivered Monthly.</div>
 
</div>
</div>
</div>
<!-- header area closed-->

<!--menu area open-->
<div class="menuArea">
<div class="innerwrapper">
<div id="menu">
<ul>
<li><a href="">Home</a></li>
<li><a href="javascript:void(0)">Store</a>
<div>
<ul>
<?php
foreach ($categories as $category) { ?>	
<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
<?php }?>
</ul>
</div>
</li>
<li><a href="about-us">About Us</a></li>
<li><a href="kit-options">Kit Options</a></li>
<li><a href="javascript:void(0)" >Why Floss</a>
<div>
<ul>
<li><a href="why-floss">Why Floss</a></li>
<li><a href="faq">FAQ</a></li>
<li><a href="mission-statement">Mission Statement</a></li>
</ul>
</div>
</li>
<li><a href="contact-us">Contact Us</a></li>
</ul>
</div>

</div>
</div>
<!-- menu area closed-->

</div>

<?php if ($error) { ?>
    
<div class="warning"><?php echo $error ?><img src="catalog/view/theme/getfloss/image/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div class="innerwrapper"><?php
//if ($this->request->get['route'] != 'common/home')
if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] != 'common/home') {
echo  "<div id='notification'>
</div>";

}?>

</div>

<script>
$("#blue-mobile-menu").blueMobileMenu();
</script>

<script type="text/javascript">
function openSubMenu(id){
	
	$('.submenu').hide();
	document.getElementById("id_menu_"+id).style.display="block";
	
}
function openSubMenu2(){

$('.submenu').hide();
document.getElementById("id_menu_"+id).style.display="none";

}

//$(document).ready(function(e) {
//	$('#menu > ul > li').hover(function(){
//    $('#menu > ul > li').has('.submenu').addClass('subshow');
//		
//	});
	 //$(this).siblings('document.getElementById("id_menu_"+id)').fadeOut(2000);
//});
</script>

<!--css breaking for this reason start-->
<style>
.submenu{
    /*background: url('catalog/view/theme/default/image/menu.png') repeat scroll 0 0 transparent;*/
	background:#fff;
    margin-top:-32px;
    left:140px;
    position:absolute;
    min-width:140px;
    display:none;
	min-height:25px;
	box-shadow:#CCC 0px 0px 3px;
}
.subshow{
	display:block;
}
/*#menu > ul > li >:hover .submenu{
	display:block;
}*/
</style>
<!--[if IE 7]>
<style>
#menu > ul > li > div {
width:140px!important;
}
.submenu{
   left:145px;
}
</style>
<![endif]-->
<!--[if IE 8]>
<style>
#menu > ul > li > div {
width:140px!important;
}
.submenu{
   left:150px;
}
</style>
<!--css breaking for this reason end-->

 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/mobile2.css" rel="stylesheet" type="text/css" />




<div class="checkout-inner-leftpanel">
<div class="login-leftsection-heading"><?php echo $text_new_customer; ?></div>
<div class="checkout-leftpanel-text1"><?php echo $text_checkout; ?></div>
<div class="checkout-leftpanel-rawbox">
<?php if ($account == 'register') { ?>
<input type="radio" name="account" value="register" id="register" checked="checked" />
<?php } else { ?>
<input type="radio" name="account" value="register" id="register" />
<?php } ?>
<?php echo $text_register; ?>
</div>
<div class="checkout-leftpanel-rawbox">
<?php if ($guest_checkout) { ?>
<?php if ($account == 'guest') { ?>
<input type="radio" name="account" value="guest" id="guest" checked="checked" />
<?php } else { ?>
<input type="radio" name="account" value="guest" id="guest" />
<?php } ?>
<?php echo $text_guest; ?>
<?php } ?>
</div>
<div class="checkout-leftpanel-textbg"><?php echo $text_register_account; ?></div>
<input type="button" value="<?php echo $button_continue; ?>" id="button-account" class="login-continue-button" />
</div>
<div class="checkout-inner-rightpanel" id="login">
<div class="login-leftsection-heading"><?php echo $text_returning_customer; ?></div>
<div class="login-rightsection-requiredtext">* Required Fields</div>
<div class="login-rightsection-frombox">
<div class="login-rightsection-fromboxtext"><?php echo $entry_email; ?></div>
<input name="email" type="text" class="login-rightsection-fromboxtextfield" />
</div>
<div class="login-rightsection-frombox">
<div class="login-rightsection-fromboxtext"><?php echo $entry_password; ?></div>
<input name="password" type="password" class="login-rightsection-fromboxtextfield" />
</div>
<div class="login-rightsection-frombox">
<input name="submit" type="button" value="<?php echo $button_login; ?>" id="button-login" class="login-continue-button2" />
<div class="login-rightsection-forgettext"><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></div>
</div>
</div>
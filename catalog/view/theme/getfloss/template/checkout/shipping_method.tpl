 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/mobile2.css" rel="stylesheet" type="text/css" />
 

<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($shipping_methods) { ?>
<div class="checkout-delevery-text1"><?php echo $text_shipping_method; ?></div>
  <?php foreach ($shipping_methods as $shipping_method) { ?>
  <div class="checkout-delevery-heading"><?php echo $shipping_method['title']; ?></div>
  <?php if (!$shipping_method['error']) { ?>
  <?php foreach ($shipping_method['quote'] as $quote) { ?>
  <div class="checkout-delevery-radiorow">
    <?php if ($quote['code'] == $code || !$code) { ?>
      <?php $code = $quote['code']; ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" checked="checked" />
      <?php } else { ?>
      <input type="radio" name="shipping_method" value="<?php echo $quote['code']; ?>" id="<?php echo $quote['code']; ?>" />
    <?php } ?>
    <label for="<?php echo $quote['code']; ?>"><?php echo $quote['longtitle']; ?></label>
    <span><label for="<?php echo $quote['code']; ?>"><?php echo $quote['text']; ?></label></span>
  </div>
  <?php } ?>
  <?php } else { ?>
    <div class="error"><?php echo $shipping_method['error']; ?></div>
  <?php } ?>
  <?php } ?>
<br />
<?php } ?>
<div class="checkout-delevery-abouttext"><?php echo $text_comments; ?></div>
<textarea name="comment" rows="8" class="checkout-edit-massagefield"><?php echo $comment; ?></textarea>
<input name="submit" type="button" value="<?php echo $button_continue; ?>" id="button-shipping-method" class="cheakout-continue-rightbutton" />
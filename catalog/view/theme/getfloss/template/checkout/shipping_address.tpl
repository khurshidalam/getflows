 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/mobile2.css" rel="stylesheet" type="text/css" />
 
<?php if ($addresses) { ?>
<div class="checkout-leftpanel-rawbox">
<input type="radio" name="shipping_address" value="existing" id="shipping-address-existing" checked="checked" />
<?php echo $text_address_existing; ?>
</div>
<div id="shipping-existing">
<select name="address_id" style="width: 100%; margin-bottom: 15px;" size="5" class="checkout-edit-massagefield">
<?php foreach ($addresses as $address) { ?>
<?php if ($address['address_id'] == $address_id) { ?>
<option value="<?php echo $address['address_id']; ?>" selected="selected"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
<?php } else { ?>
<option value="<?php echo $address['address_id']; ?>"><?php echo $address['firstname']; ?> <?php echo $address['lastname']; ?>, <?php echo $address['address_1']; ?>, <?php echo $address['city']; ?>, <?php echo $address['zone']; ?>, <?php echo $address['country']; ?></option>
<?php } ?>
<?php } ?>
</select>
</div>
<div class="checkout-leftpanel-rawbox" style="margin:0 0 20px;">
<input type="radio" name="shipping_address" value="new" id="shipping-address-new" />
<?php echo $text_address_new; ?>
</div>
<?php } ?>
<div id="shipping-new" style="display: <?php echo ($addresses ? 'none' : 'block'); ?>;">
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_firstname; ?></div>
<input name="firstname" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_lastname; ?></div>
<input name="lastname" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><?php echo $entry_company; ?></div>
<input name="company" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
&nbsp;
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_address_1; ?></div>
<input name="address_1" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><?php echo $entry_address_2; ?></div>
<input name="address_2" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_city; ?></div>
<input name="city" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_postcode; ?></div>
<input name="postcode" type="text" class="register-inner-fromboxtextfield" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_country; ?></div>
<div class="con-drop-field">
<select name="country_id" class="drop-p">
<option value=""><?php echo $text_select; ?></option>
<?php foreach ($countries as $country) { ?>
<?php if ($country['country_id'] == $country_id) { ?>
<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
<?php } else { ?>
<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
<?php } ?>
<?php } ?>
</select>
</div>
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_zone; ?></div>
<div class="con-drop-field">
<select name="zone_id" class="drop-p"></select>
</div>
</div>
</div>
<input name="submit" type="button" class="cheakout-continue-rightbutton" id="button-shipping-address" value="<?php echo $button_continue; ?>" />
<script type="text/javascript"><!--
$('#shipping-address input[name=\'shipping_address\']').live('change', function() {
	if (this.value == 'new') {
		$('#shipping-existing').hide();
		$('#shipping-new').show();
	} else {
		$('#shipping-existing').show();
		$('#shipping-new').hide();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('#shipping-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#shipping-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#shipping-postcode-required').show();
			} else {
				$('#shipping-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#shipping-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#shipping-address select[name=\'country_id\']').trigger('change');
//--></script>
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/mobile2.css" rel="stylesheet" type="text/css" />
 
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($payment_methods) { ?>
<div class="checkout-delevery-text1"><?php echo $text_payment_method; ?></div>
  <?php foreach ($payment_methods as $payment_method) { ?>
  <div class="checkout-delevery-radiorow">
    <?php if ($payment_method['code'] == $code || !$code) { ?>
      <?php $code = $payment_method['code']; ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" checked="checked" />
    <?php } else { ?>
      <input type="radio" name="payment_method" value="<?php echo $payment_method['code']; ?>" id="<?php echo $payment_method['code']; ?>" />
    <?php } ?>
    <label for="<?php echo $payment_method['code']; ?>"><?php echo $payment_method['title']; ?></label>
  </div>
  <?php } ?>
<?php } ?>
<div class="checkout-delevery-abouttext"><?php echo $text_comments; ?></div>
<textarea name="comment" rows="8" class="checkout-edit-massagefield"><?php echo $comment; ?></textarea>
<?php if ($text_agree) { ?>
<div class="cheakout-inner-bottomsection">
  <div class="cheakout-inner-bottom-cheakboxtext"><?php echo $text_agree; ?></div>
  <div class="cheakout-inner-bottom-cheakbox">
    <?php if ($agree) { ?>
      <input type="checkbox" name="agree" value="1" checked="checked" />
    <?php } else { ?>
      <input type="checkbox" name="agree" value="1" />
    <?php } ?>
  </div>
  <input name="submit" type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="cheakout-continue-rightbutton" />
</div>
<?php } else { ?>
<div class="cheakout-inner-bottomsection">
  <input name="submit" type="button" value="<?php echo $button_continue; ?>" id="button-payment-method" class="cheakout-continue-rightbutton" />
</div>
<?php } ?>
<script type="text/javascript"><!--
$('.colorbox').colorbox({
	width: 640,
	height: 480
});
//--></script> 
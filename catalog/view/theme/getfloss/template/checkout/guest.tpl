 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/all-natural-quest/stylesheet/cartcheckout/mobile2.css" rel="stylesheet" type="text/css" />
 
<div class="register-innerbox">
<div class="register-innerbox-heading"><?php echo $text_your_details; ?></div>
<div class="register-required-text">* Required Fields</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_firstname; ?></div>
<input name="firstname" type="text" class="register-inner-fromboxtextfield" value="<?php echo $firstname; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_lastname; ?></div>
<input name="lastname" type="text" class="register-inner-fromboxtextfield" value="<?php echo $lastname; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_email; ?></div>
<input name="email" type="text" class="register-inner-fromboxtextfield" value="<?php echo $email; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_telephone; ?></div>
<input name="telephone" type="text" class="register-inner-fromboxtextfield" value="<?php echo $telephone; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><?php echo $entry_fax; ?></div>
<input name="fax" type="text" class="register-inner-fromboxtextfield" value="<?php echo $fax; ?>" />
</div>
</div>
<div class="register-innerbox">
<div class="register-innerbox-heading"><?php echo $text_your_address; ?></div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><?php echo $entry_company; ?></div>
<input name="company" type="text" class="register-inner-fromboxtextfield" value="<?php echo $company; ?>" />
</div>
<div style="display: <?php echo (count($customer_groups) > 1 ? 'table-row' : 'none'); ?>;"> <?php echo $entry_customer_group; ?><br />
<?php foreach ($customer_groups as $customer_group) { ?>
<?php if ($customer_group['customer_group_id'] == $customer_group_id) { ?>
<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" checked="checked" />
<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
<br />
<?php } else { ?>
<input type="radio" name="customer_group_id" value="<?php echo $customer_group['customer_group_id']; ?>" id="customer_group_id<?php echo $customer_group['customer_group_id']; ?>" />
<label for="customer_group_id<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></label>
<br />
<?php } ?>
<?php } ?>
<br />
</div>
<div class="register-inner-frombox" id="company-id-display">&nbsp;

<div class="register-inner-fromboxtext"><span class="required"></span>  <?php echo $entry_company_id; ?></div>
<input type="text" name="company_id" value="<?php echo $company_id; ?>" class="register-inner-fromboxtextfield" />

</div>


<div class="register-inner-frombox" id="tax-id-display">
<div class="register-inner-fromboxtext"><span id="tax-id-required" class="required">*</span> <?php echo $entry_tax_id; ?></div>
<input name="tax_id" type="text" class="register-inner-fromboxtextfield" value="<?php echo $tax_id; ?>" />
</div>

<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_address_1; ?></div>
<input name="address_1" type="text" class="register-inner-fromboxtextfield" value="<?php echo $address_1; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><?php echo $entry_address_2; ?></div>
<input name="address_2" type="text" class="register-inner-fromboxtextfield" value="<?php echo $address_2; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_city; ?></div>
<input name="city" type="text" class="register-inner-fromboxtextfield" value="<?php echo $city; ?>" />
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php //echo $entry_postcode; ?>Zip Code :</div>
<input name="postcode" type="text" value="<?php echo $postcode; ?>" class="register-inner-fromboxtextfield" />
</div>





<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_country; ?></div>
<div class="con-drop-field">
<select name="country_id" class="drop-p">
<option value=""><?php echo $text_select; ?></option>
<?php foreach ($countries as $country) { ?>
<?php if ($country['country_id'] == $country_id) { ?>
<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
<?php } else { ?>
<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
<?php } ?>
<?php } ?>
</select>
</div>
</div>
<div class="register-inner-frombox">
<div class="register-inner-fromboxtext"><span class="required">*</span> <?php echo $entry_zone; ?></div>
<div class="con-drop-field">
<select name="zone_id" class="drop-p"></select>
</div>
</div>
</div>
<div class="register-innerbox" style="border-bottom:none; margin-bottom:0;">
<?php if ($shipping_required) { ?>
<?php if ($shipping_address) { ?>
<input type="checkbox" name="shipping_address" value="1" id="shipping" checked="checked" />
<?php } else { ?>
<input type="checkbox" name="shipping_address" value="1" id="shipping" />
<?php } ?>
<?php echo $entry_shipping; ?>
<?php } ?>
</div>
<div class="cheakout-inner-bottomsection">
<input name="submit" type="button" class="cheakout-continue-rightbutton" id="button-guest" value="<?php echo $button_continue; ?>" />
</div>








  
  
 
</div>

<script type="text/javascript"><!--
$('#payment-address input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('#payment-address input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript"><!--
$('#payment-address select[name=\'country_id\']').bind('change', function() {
	if (this.value == '') return;
	$.ajax({
		url: 'index.php?route=checkout/checkout/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('#payment-address select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#payment-postcode-required').show();
			} else {
				$('#payment-postcode-required').hide();
			}
			
			html = '<option value=""><?php echo $text_select; ?></option>';
			
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<option value="' + json['zone'][i]['zone_id'] + '"';
	    			
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#payment-address select[name=\'zone_id\']').html(html);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('#payment-address select[name=\'country_id\']').trigger('change');
//--></script>
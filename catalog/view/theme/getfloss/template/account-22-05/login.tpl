<?php echo $header; ?>


<div class="loginAra">
<div class="innerwrapper">
<?php echo $column_left; ?><?php //echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><?php echo $heading_title; ?></h1>
  <?php if ($success) { ?>
<div class="vrchr_hddesgn"><p><br><?php echo $success; ?></p></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="vrchr_hddesgn"><p><br><?php echo $error_warning; ?></p></div>
<?php } ?>
  <div class="login-content">
    <div class="left">
      <h2><?php echo $text_new_customer; ?></h2>
      <div class="content">
        <p><b><?php echo $text_register; ?></b></p>
        <p><?php echo $text_register_account; ?></p>
        <a href="<?php echo $register; ?>" class="yellow_btn"><?php echo $button_continue; ?></a></div>
    </div>
    <div class="right">
      <h2><?php echo $text_returning_customer; ?></h2>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="content">
          <p><?php echo $text_i_am_returning_customer; ?></p>
          <div class="frmFldArea">
          <label><?php echo $entry_email; ?></label>
          <input type="text" name="email" value="<?php echo $email; ?>" class="inputTextFld" />
		</div>
          <div class="frmFldArea">
          <label><?php echo $entry_password; ?></label>
          <input type="password" name="password" value="<?php echo $password; ?>"  class="inputTextFld" />
          <br />
          <a href="<?php echo $forgotten; ?>" class="forgotpasas"><?php echo $text_forgotten; ?></a><br />
          <br />
          <input type="submit" value="<?php echo $button_login; ?>" class="yellow_btn" />
          <?php if ($redirect) { ?>
          <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
          <?php } ?>
        </div>
      </form>
    </div>
  </div>
  <?php echo $content_bottom; ?></div>
  
  </div>
  </div>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>
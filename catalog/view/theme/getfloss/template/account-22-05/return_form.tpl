<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="prdctRetrnAra">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  
<h1 class="prdct_name"><?php echo $heading_title; ?></h1>


<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  

  
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2 class="catPag_box-heading2"><span><?php echo $text_order; ?></span><?php echo $text_description; ?></h2>
    <div class="content">
      <div class="left">
      <div class="frmFldArea">
      <label><div class="star">*</div> <?php echo $entry_firstname; ?></label>
        <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="inputTextFld" />
        <?php if ($error_firstname) { ?>
        <span class="error"><?php echo $error_firstname; ?></span>
        <?php } ?>
        </div>
        <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_lastname; ?></label>
        <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="inputTextFld" />
        <?php if ($error_lastname) { ?>
        <span class="error"><?php echo $error_lastname; ?></span>
        <?php } ?>
        </div>
        <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_email; ?></label>
        <input type="text" name="email" value="<?php echo $email; ?>" class="inputTextFld" />
        <?php if ($error_email) { ?>
        <span class="error"><?php echo $error_email; ?></span>
        <?php } ?>
        </div>
		<div class="frmFldArea">
        <label><div class="star">*</div><?php echo $entry_telephone; ?></label>
        <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="inputTextFld" />
        <?php if ($error_telephone) { ?>
        <span class="error"><?php echo $error_telephone; ?></span>
        <?php } ?>
        </div>
        </div>
      <div class="right">
      <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_order_id; ?></label>
        <input type="text" name="order_id" value="<?php echo $order_id; ?>" class="inputTextFld" />
        <?php if ($error_order_id) { ?>
        <span class="error"><?php echo $error_order_id; ?></span>
        <?php } ?>
        </div>
        <div class="frmFldArea">
        <label>
        <?php echo $entry_date_ordered; ?></label>
        <input type="text" name="date_ordered" value="<?php echo $date_ordered; ?>" class="inputTextFld" />
      </div>
    </div>
    <h2 class="catPag_box-heading" style="margin-top:20px;"><?php echo $text_product; ?></h2>
    <div id="return-product">
      <div class="content">
        <div class="return-product">
          <div class="left">
          
          
          <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_product; ?></label>
            <input type="text" name="product" value="<?php echo $product; ?>"  class="inputTextFld" />
            <?php if ($error_product) { ?>
            <span class="error"><?php echo $error_product; ?></span>
            <?php } ?>
            </div>
            <div class="frmFldArea">
            <label><div class="star">*</div> <?php echo $entry_model; ?></label>
            <input type="text" name="model" value="<?php echo $model; ?>"   class="inputTextFld" />
            <?php if ($error_model) { ?>
            <span class="error"><?php echo $error_model; ?></span>
            <?php } ?>
            </div>
            
             <div class="frmFldArea">
            <label><?php echo $entry_quantity; ?></label>
            <input type="text" name="quantity" value="<?php echo $quantity; ?>"  class="inputTextFld" />
          </div>
          
          <div class="frmFldArea">
            <label><div class="star">*</div> <?php echo $entry_reason; ?></label>
            <div class="radioareea">
              <?php foreach ($return_reasons as $return_reason) { ?>
              <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
              <ul>
              <li><input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" checked="checked" /><label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label></li>
              </ul>
              <?php } else { ?>
              <ul>
              <li><input type="radio" name="return_reason_id" value="<?php echo $return_reason['return_reason_id']; ?>" id="return-reason-id<?php echo $return_reason['return_reason_id']; ?>" /><label for="return-reason-id<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></label></li>
              </ul>
              <?php  } ?>
              <?php  } ?>
            </div>
            <?php if ($error_reason) { ?>
            <span class="error"><?php echo $error_reason; ?></span>
            <?php } ?>
          </div>
          
            
          </div>
          <!--<div class="return-model">
          </div>-->
          <div class="right">
          	
            
            <div class="frmFldArea">
            <label><?php echo $entry_opened; ?></label>
            <div class="radioareea">
            <ul>
            <li>
            <?php if ($opened) { ?>
            <input type="radio" name="opened" value="1" id="opened" checked="checked" />
            <?php } else { ?>
            <input type="radio" name="opened" value="1" id="opened" />
            <?php } ?>
            <label for="opened"><?php echo $text_yes; ?></label></li>
            <li>
            <?php if (!$opened) { ?>
            <input type="radio" name="opened" value="0" id="unopened" checked="checked" />
            <?php } else { ?>
            <input type="radio" name="opened" value="0" id="unopened" />
            <?php } ?>
            <label for="unopened"><?php echo $text_no; ?></label>
            </li>
            </ul>
            </div>
            </div>

			<div class="frmFldArea">
            <label><?php echo $entry_fault_detail; ?></label>
            <textarea name="comment" cols="150" rows="6"><?php echo $comment; ?></textarea>
            </div>
            
            <div class="frmFldArea">
            <label><?php echo $entry_captcha; ?></label>
            <img src="index.php?route=account/return/captcha" alt="" />
            <input type="text" name="captcha" value="<?php echo $captcha; ?>" class="capinput" />
            <?php if ($error_captcha) { ?>
            <span class="error"><?php echo $error_captcha; ?></span>
            <?php } ?>
            </div>
            
          </div>
         
        </div>
        <div class="return-detail">
          <div class="return-reason">
          </div>
          <div class="return-opened">
            
          </div>
          <div class="return-captcha">
          </div>
        </div>
      </div>
    </div>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="back_btnn"><?php echo $button_back; ?></a></div>
      <div class="right"><?php echo $text_agree; ?>
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="back_btnn"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
    <?php } ?>
  </form>
  <?php echo $content_bottom; ?></div>
  
</div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script> 
<?php echo $footer; ?>
<?php echo $header; ?>

<div class="forgotAra">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><?php echo $heading_title; ?></h1>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <p><?php echo $text_email; ?></p><br />
    <h2><?php echo $text_your_email; ?></h2>
    <div class="content">
      <div class="frmFldArea">
          <label><?php echo $entry_email; ?></label>
          <input type="text" name="email" value=""  class="inputTextFld" />
          </div>
    </div>
    <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="back_btnn"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
  </form>
  <?php echo $content_bottom; ?></div>
  
  </div>
  </div>
<?php echo $footer; ?>
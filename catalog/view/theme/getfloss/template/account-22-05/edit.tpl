<?php echo $header; ?>


<div class="cretaccuntAra">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
 <h1 class="prdct_name"><?php echo $heading_title; ?></h1>
  
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content" >

<?php if ($error_warning) { ?>
<div class="vrchr_hddesgn"><p><?php echo $error_warning; ?></p></div>
<?php } ?>
 
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h1 class="prdct_name"><?php echo $text_your_details; ?></h1>
    <div class="content">
          <div class="frmFldArea">
        <label><div class="star">*</div><?php echo $entry_firstname; ?></label>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" class="inputTextFld" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?>
	  </div>
        <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_lastname; ?></label>
          <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="inputTextFld"/>
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?>
        </div>
	
        <div class="frmFldArea">
        <label><div class="star">*</div> <?php echo $entry_email; ?></label>
          <input type="text" name="email" value="<?php echo $email; ?>" class="inputTextFld" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?>
           </div>
	
	
        <div class="frmFldArea">
        <label><div class="star">*</div>  <?php echo $entry_telephone; ?></label>
          <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="inputTextFld" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?>
        </div>
	
            <div class="frmFldArea"> <label><?php echo $entry_fax; ?></label>
          <input type="text" name="fax" value="<?php echo $fax; ?>" class="inputTextFld" /></td>
        </div>
    
    
    
       <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="back_btnn"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
    </div>
  
  </form>
  <?php echo $content_bottom; ?>
      
</div>
<!--End body section-->

</div>
  </div>
<?php echo $footer; ?>
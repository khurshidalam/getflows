<?php echo $header; ?>
<div class="infoConAra">
<div class="innerwrapper">
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/events.css" />
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?><?php echo $title; ?></h1>
  <div id="tabs" class="htabs"><a href="#tab-general"><?php echo $tab_general; ?></a>
<?php if ($title) { ?>
    <a href="#tab-products"><?php echo $tab_products; ?></a>
    <a href="#tab-notify"><?php echo $tab_notify; ?></a>
    <a href="#tab-share"><?php echo $tab_share; ?></a>
    <a href="#tab-notes"><?php echo $tab_notes; ?></a>
    <a href="#tab-rsvp"><?php echo $tab_rsvp; ?></a>
<?php }?>
  </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
  <div id="tab-general" class="tab-content">
		<table>
		  <tr>
		    <td><span class="required">*</span>&nbsp;<?php echo $entry_title; ?></td>
		    <td><input type="text" value="<?php echo $title; ?>" name="title" id="title" /><br />
		    <?php if ($error_title) { ?>
			  <span class="error"><?php echo $error_title; ?></span>
			<?php } ?></td>
		  </tr>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_name; ?></td>
			<td><input type="text" value="<?php echo $name; ?>" name="name" id="name" /><br />
			<?php if ($error_name) { ?>
			  <span class="error"><?php echo $error_name; ?></span>
			<?php } ?></td>
		  </tr>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_email; ?></td>
			<td><input type="text" value="<?php echo $email; ?>" name="email" id="email" /><br />
			<?php if ($error_email) { ?>
			  <span class="error"><?php echo $error_email; ?></span>
			<?php } ?></td>
		  </tr>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_start_date; ?></td>
			<td><input type="text" value="<?php echo $start_date; ?>" name="start_date" id="start_date" /><br />
			<?php if ($error_start_date) { ?>
			  <span class="error"><?php echo $error_start_date; ?></span>
			<?php } ?></td>
		  </tr>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_end_date; ?></td>
			<td><input type="text" value="<?php echo $end_date; ?>" name="end_date" id="end_date" /><br />
			<?php if ($error_end_date) { ?>
			  <span class="error"><?php echo $error_end_date; ?></span>
			<?php } ?></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_type; ?></td>
			<td><select name="type">
              <?php if ($type) { ?>
				<option value="1" selected="selected"><?php echo $text_private; ?></option>
                <option value="0"><?php echo $text_public; ?></option>
                <?php } else { ?>
                  <option value="1"><?php echo $text_private; ?></option>
                  <option value="0" selected="selected"><?php echo $text_public; ?></option>
                <?php } ?>
            </select></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_status; ?></td>
			<td><select name="status">
            <?php if ($status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_notes; ?></td>
			<td><select name="enable_notes">
            <?php if ($enable_notes) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_rsvp; ?></td>
			<td><select name="enable_rsvp">
            <?php if ($enable_rsvp) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_photo; ?></td>
		    <td><input type="text" value="<?php echo $photo; ?>" name="photo" id="photo" placeholder="http://www.website.com" /><br />
		  </tr>
		  <tr>
			<td><?php echo $entry_web_link; ?></td>
		    <td><input type="text" value="<?php echo $web_link; ?>" name="web_link" id="web_link" placeholder="http://www.website.com" /><br />
		  </tr>
		  <tr>
			<td><?php echo $entry_message; ?></td>
			<td><textarea rows="4" cols="50" name="message" placeholder="<?php echo $text_placeholder; ?>"><?php echo $message; ?></textarea></td>
		  </tr>
		  <tr>
			<td><br /><h2><?php echo $text_invitees; ?></h2></td>
			<td></td>
		  </tr>
		</table>
        <table id="attendee" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_att_name; ?></td>
              <td class="right"><?php echo $entry_att_email; ?></td>
              <td></td>
            </tr>
          </thead>
		  <tfoot>
            <tr>
              <td colspan="2"></td>
              <td class="left"><a onclick="addAttendee();" class="button"><?php echo $button_add; ?></a></td>
            </tr>
		  </tfoot>
          <?php $attendee_row = 0; ?>
		  <?php if ($attendees) { ?>
			<?php foreach ($attendees as $attendee) { ?>
			<tbody id="attendee-row<?php echo $attendee_row; ?>">
              <tr>
                <td class="left"><input type="text" name="attendee[<?php echo $attendee_row; ?>][name]" value="<?php echo $attendee['name']; ?>" size="18" />
                <?php if (isset($error_name_attendee[$attendee_row])) { ?>
				  <span class="error"><?php echo $error_name_attendee[$attendee_row]; ?></span>
				<?php } ?>
				</td>
                <td class="right"><input type="text" name="attendee[<?php echo $attendee_row; ?>][email]" value="<?php echo $attendee['email']; ?>" size="18" />
                <?php if (isset($error_email_attendee[$attendee_row])) { ?>
				  <span class="error"><?php echo $error_email_attendee[$attendee_row]; ?></span>
				<?php } ?>
				</td>
				<td class="left"><a onclick="$('#attendee-row<?php echo $attendee_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a>
				</td>
		      </tr>
			</tbody>
          <?php $attendee_row++; ?>
		    <?php } ?>
		    <?php } ?>
		</table>
      <div class="buttons">
		<div class="left">
			<a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
		</div>
		<div class="right">
			<a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
		</div>
	  </div>
	</div>
  <div id="tab-products" class="tab-content">
      <div class="buttons"><div class="left">
	  <a onclick="location = '<?php echo htmlspecialchars_decode($add_products); ?>';" class="button"><?php echo $button_add_products; ?></a>
	  </div></div>
    <div class="cart-info">
      <table>
        <thead>
          <tr>
            <td class="image"><?php echo $column_image; ?></td>
			
			<td class="left">
			<?php if ($sort == 'title') { ?>
				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
			<?php } ?></td>		  
			
			<td class="left">
			<?php if ($sort == 'model') { ?>
				<a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
			<?php } ?></td>		  

            <td class="quantity"><?php echo $column_quantity; ?></td>
			
			<td class="total">
			<?php if ($sort == 'price') { ?>
				<a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
			<?php } ?></td>		  
			
			<td class="total">
			<?php if ($sort == 'total') { ?>
				<a href="<?php echo $sort_total; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_total; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_total; ?>"><?php echo $column_total; ?></a>
			<?php } ?></td>		  
			
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="image" style="border-bottom: none;">
			<?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?>
			</td>
            <td class="name" style="border-bottom: none;">
			  <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <?php if (!$product['stock']) { ?>
                <span class="stock">***</span>
              <?php } ?>
              <div>
                <?php foreach ($product['option'] as $option) { ?>
                  - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
                <?php } ?>
              </div></td>
            <td class="model" style="border-bottom: none;"><?php echo $product['model']; ?></td>
            <td class="quantity" style="border-bottom: none;">
			  <input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" />&nbsp;<a href="<?php echo $product['remove']; ?>"><img src="catalog/view/theme/default/image/remove.png" alt="<?php echo $button_remove; ?>" title="<?php echo $button_remove; ?>" /></a>
			</td>
            <td class="price" style="border-bottom: none;"><?php echo $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))); ?></td>
            <td class="price" style="border-bottom: none;"><?php echo $this->currency->format($this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'))); ?></td>
			<tr><td colspan="6"><textarea name="comment-<?php echo $product['key']; ?>" placeholder="You can add a public comment about this product here." value="<?php echo $product['comment']; ?>" cols="60" rows="1"><?php echo $product['comment']; ?></textarea></td></tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
      <div class="buttons">
	    <div class="left">
		  <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
		</div>
		<div class="right">
		<a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a>
		</div>
	  </div>
  </div>	
  <div id="tab-notify" class="tab-content">
		  <div>
		  <?php echo $entry_message; ?><br />
			<textarea rows="4" cols="50" name="notify_message" placeholder="<?php echo $text_noti_message_placeholder; ?>"><?php echo $notify_message; ?></textarea>
		  </div>
      <div class="buttons">
	    <div class="left">
		  <a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a>
		</div>
	  </div>
    <table class="list">
      <thead>
        <tr>
          <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);$('.buttons').show();" /></td>
          <td style="padding: 10px;"><?php echo $entry_att_name; ?></td>
          <td style="padding: 10px;"><?php echo $column_email; ?></td>
        </tr>
      </thead>
      <?php if ($attendees) { ?>
      <?php foreach ($attendees as $attendee) { ?>
      <tbody>
        <tr>
            <td style="padding: 10px; vertical-align: middle;">
              <input type="checkbox" name="selected[]" value="<?php echo $attendee['email']; ?>"/>
             </td>
          <td style="padding: 10px; vertical-align: middle;"><?php echo $attendee['name']; ?></td>
          <td style="padding: 10px; vertical-align: middle;"><?php echo $attendee['email']; ?></td>
        </tr>
      <?php } ?>
	  </tbody>
      <?php } ?>
    </table>
      <?php if ($attendees) { ?>
		<div class="buttons">
		<div class="left">
		<a onclick="$('#form').attr('action', '<?php echo $send; ?>'); $('#form').submit();" class="button"><?php echo $button_send; ?></a>
		</div>
		</div>
      <?php } ?>
		<h2><?php echo $text_noti_preview; ?></h2>
		<div><?php echo $noti_message_preview; ?></div>
	</div>
	
	</form>  
	
  <div id="tab-share" class="tab-content">
  	  <div><?php echo $text_link; ?><?php echo $link; ?><br /><br /></div>
		<h2><?php echo $text_social_share; ?></h2>
		<div><?php echo $text_social_message; ?></div>
			<div style="margin-top: 10px">
				<a target="_blank" href="http://twitter.com/share?text=<?php echo $text_my_event; ?> <?php echo htmlspecialchars_decode($link); ?>" title="<?php echo $text_twitter_title; ?>"><img src="catalog/view/theme/default/image/twitter.png" alt="<?php echo $text_twitter_alt; ?>"  /></a>
				<a title="<?php echo $text_facebook_title; ?>" href="http://www.facebook.com/sharer.php?s=100&p[title]=<?php echo $text_my_event; ?>&p[url]=<?php echo $link; ?>" target="_blank"><img src="catalog/view/theme/default/image/facebook.png" alt="<?php echo $text_facebook_alt; ?>"  /></a>
			</div>
	</div>  
	  

  <div id="tab-notes" class="tab-content">
  <?php if ($note_status) { ?>
    <div id="note"></div>
    <h2 id="note-title"><?php echo $text_write; ?></h2>
    <br />
    <b><?php echo $entry_note; ?></b>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <div class="buttons">
      <div class="right"><a id="button-note" class="button"><?php echo $button_continue; ?></a></div>
	</div>
  <?php } else { ?>
    <h2 id="note-title"><?php echo $text_notes_disabled; ?></h2>
  <?php } ?>
  </div>
  
  
  <div id="tab-rsvp" class="tab-content">
  <?php if ($rsvp_status) { ?>
    <?php if ($rsvps) { ?>
		<div class="wishlist-info">
		  <table>
			<thead>
			  <tr>
				<td class="name"><?php echo $column_guest_name; ?></td>
				<td class="note"><?php echo $column_note; ?></td>
				<td class="guests"><?php echo $column_guests; ?></td>
			  </tr>
			</thead>
		  <?php foreach ($rsvps as $rsvp) { ?>
			<tbody>
			  <tr>
				<td class="name"><b><?php echo $rsvp['name']; ?></b><br /><?php echo $rsvp['email']; ?></td>
				<td class="name" style="width:350px"><?php echo $rsvp['note']; ?></td>
				<td class="name">
				<?php if ($rsvp['guests']) { ?>
				  <?php foreach ($rsvp['guests'] as $guest) { ?>
					<?php echo $guest; ?><br />
				  <?php } ?>
				<?php } ?>
			  </tr>
			</tbody>
		  <?php } ?>
		  </table>
		</div>
    <?php } else {?>
	  <?php echo $text_no_rsvps; ?>
    <?php } ?>
  <?php } else {?>
    <h2 id="rsvp-title"><?php echo $text_rsvp_disabled; ?></h2>
  <?php } ?>
  </div>
  
  
  <?php //echo $content_bottom; ?>
  </div></div></div>
<script type="text/javascript"><!--
var attendee_row = <?php echo $attendee_row; ?>;
function addAttendee() {	
	html  = '<tbody id="attendee-row' + attendee_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="attendee[' + attendee_row + '][name]" value="" size="18" />';
	html += '    <td class="right"><input type="text" name="attendee[' + attendee_row + '][email]" value="" size="18" />';
	html += '    <td class="left"><a onclick="$(\'#attendee-row' + attendee_row + '\').remove();" class="button"><?php echo $button_remove; ?></a>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#attendee tfoot').before(html);
	
	attendee_row++;
}
$("#start_date").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    minDate: new Date(),
    maxDate: '+2y',
    onSelect: function(date){

        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

        $("#end_date").datepicker( "option", "minDate", endDate );
        $("#end_date").datepicker( "option", "maxDate", '+2y' );

    }
});

$("#end_date").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true
});
$('#tabs a').tabs(); 

$('#note .pagination a').live('click', function() {
	$('#note').fadeOut('slow');
		
	$('#note').load(this.href);
	
	$('#note').fadeIn('slow');
	
	return false;
});			

$('#note').load('index.php?route=account/events/notes&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>');

$('#button-note').bind('click', function() {
	$.ajax({
		url: 'index.php?route=account/events/write_note&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-note').attr('disabled', true);
			$('#note-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-note').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#note-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#note-title').after('<div class="success">' + data['success'] + '</div>');
				
				$('#note').load('index.php?route=account/events/notes&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>');
								
				$('textarea[name=\'text\']').val('');
			}
		}
	});
});
//--></script>
<?php echo $footer; ?>
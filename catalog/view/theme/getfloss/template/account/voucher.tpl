<?php echo $header; ?>

<div class="vrformmAra">
<div class="innerwrapper">
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><?php echo $heading_title; ?></h1>
  
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  
  
  <div class="vrchr_hddesgn"><p><?php echo $text_description; ?></p></div>
  <div class="vrformmAra" id="vrchr_form">
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    
   <div class="frmFldArea">
   <label><div class="star">*</div> <?php echo $entry_to_name; ?></label>
   <input type="text" name="to_name" value="<?php echo $to_name; ?>" class="inputTextFld" />
          <?php if ($error_to_name) { ?>
          <span class="error"><?php echo $error_to_name; ?></span>
          <?php } ?>
   </div> 
    
    <div class="frmFldArea">
   <label><div class="star">*</div> <?php echo $entry_to_email; ?></label>
    <input type="text" name="to_email" value="<?php echo $to_email; ?>" class="inputTextFld" />
          <?php if ($error_to_email) { ?>
          <span class="error"><?php echo $error_to_email; ?></span>
          <?php } ?>
          </div>
    <div class="frmFldArea">
   <label><div class="star">*</div> <?php echo $entry_from_name; ?></label>
    <input type="text" name="from_name" value="<?php echo $from_name; ?>" class="inputTextFld"  />
          <?php if ($error_from_name) { ?>
          <span class="error"><?php echo $error_from_name; ?></span>
          <?php } ?>
          </div>
          
   <div class="frmFldArea">
   <label><div class="star">*</div> <?php echo $entry_from_email; ?></label>
   <input type="text" name="from_email" value="<?php echo $from_email; ?>"  class="inputTextFld" />
          <?php if ($error_from_email) { ?>
          <span class="error"><?php echo $error_from_email; ?></span>
          <?php } ?>
          </div>
          
    <div class="frmFldArea">
   <label><div class="star">*</div> <?php echo $entry_theme; ?></label>
   <div class="radioareea">
   <ul>
   <?php foreach ($voucher_themes as $voucher_theme) { ?>
          <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
          <li><input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>" checked="checked" />
          <label for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label></li>
          <?php } else { ?>
          <li><input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" id="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>" />
          <label for="voucher-<?php echo $voucher_theme['voucher_theme_id']; ?>"><?php echo $voucher_theme['name']; ?></label></li>
          <?php } ?>
          <?php } ?>
          </ul>
          <?php if ($error_theme) { ?>
          <span class="error"><?php echo $error_theme; ?></span>
          <?php } ?>
   </div>
   </div>
   
   <div class="frmFldArea">
   <label><?php echo $entry_message; ?></label>
   <textarea name="message" cols="40" rows="5"><?php echo $message; ?></textarea>
   </div>
   
   <div class="frmFldArea">
   <label><div class="star">*</div><?php echo $entry_amount; ?></label> 
   <input type="text" name="amount" value="<?php echo $amount; ?>" size="5"  class="inputTextFld" />
          <?php if ($error_amount) { ?>
          <span class="error"><?php echo $error_amount; ?></span>
          <?php } ?>
          </div>
    

    <div class="buttons vrchr">
      <div class="right vrchrrutn">
      <div style="width:auto; float:left; padding:10px 10px 0px 0px;">
        <?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?>
        </div>
        <span class="terms_txt"><?php echo $text_agree; ?></span>
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
  </form>
  </div>
  <?php echo $content_bottom; ?></div>
  </div>
  </div>
<?php echo $footer; ?>

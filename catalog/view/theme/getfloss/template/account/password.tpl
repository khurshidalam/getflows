<?php echo $header; ?>


<div class="cretaccuntAra editaccnt">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
 <h1 class="prdct_name"><?php echo $heading_title; ?></h1>

<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
<?php echo $content_top; ?>

  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
     <div class="content">
    <h1 class="prdct_name">Change Your Password</h1>
      
      <div class="frmFldArea">
      <label><div class="star">*</div><?php echo $entry_password; ?> </label>
      <input type="password" name="password" value="<?php echo $password; ?>"  class="inputTextFld"  />
      <?php if ($error_password) { ?>
       <span class="error"><?php echo $error_password; ?></span>
       <?php } ?>
        </div>
      
      <div class="frmFldArea">
      <label><div class="star">*</div> <?php echo $entry_confirm; ?> </label>
        <input type="password" name="confirm" value="<?php echo $confirm; ?>"  class="inputTextFld" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?>
        </div>
    
     <div class="buttons">
      <div class="left"><a href="<?php echo $back; ?>" class="back_btnn"><?php echo $button_back; ?></a></div>
      <div class="right">
        <input type="submit" value="<?php echo $button_continue; ?>" class="yellow_btn" />
      </div>
    </div>
  </form>
  <?php echo $content_bottom; ?>
      
</div>
<!--End body section-->

</div>
  </div>  </div>
<?php echo $footer; ?>
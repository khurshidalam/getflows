<?php echo $header; ?>

<div class="cretaccuntAra">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><span><?php echo $heading_title; ?></span></h1>

<!--<?php echo $column_left; ?>-->
<!--<?php echo $column_right; ?>-->
<div id="content">
  
  <div class="vrchr_hddesgn"><p><?php echo $text_account_already; ?><?php echo $content_top; ?></p></div>
  <?php if ($error_warning) { ?>
<div class="vrchr_hddesgn"><p><?php echo $error_warning; ?><?php echo $content_top; ?></p></div>
<?php } ?>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <h2 class="prdct_name6"><?php echo $text_your_details; ?></h2>
    <div class="content">
          <div class="frmFldArea">
      <label><div class="star">*</div><?php echo $entry_firstname; ?></label>
         <input type="text" name="firstname" value="<?php echo $firstname; ?>" class="inputTextFld" />
            <?php if ($error_firstname) { ?>
            <span class="error"><?php echo $error_firstname; ?></span>
            <?php } ?>
            </div>
            <div class="frmFldArea">
      <label> <?php echo $entry_lastname; ?></label>
          <input type="text" name="lastname" value="<?php echo $lastname; ?>" class="inputTextFld" />
            <?php if ($error_lastname) { ?>
            <span class="error"><?php echo $error_lastname; ?></span>
            <?php } ?>
            </div>
          <div class="frmFldArea">
      <label><div class="star">*</div><?php echo $entry_email; ?></label>
          <input type="text" name="email" value="<?php echo $email; ?>" class="inputTextFld" />
            <?php if ($error_email) { ?>
            <span class="error"><?php echo $error_email; ?></span>
            <?php } ?>
            </div>
            
          <div class="frmFldArea">
      <label><?php echo $entry_telephone; ?></label>
          <input type="text" name="telephone" value="<?php echo $telephone; ?>" class="inputTextFld" />
            <?php if ($error_telephone) { ?>
            <span class="error"><?php echo $error_telephone; ?></span>
            <?php } ?>
            </div>
          <script type="text/javascript" >
             function myFunction(y)
             {
                if (y ==1)
                {
                     document.getElementById('resellershow1').style.display='block';
                    document.getElementById('resellershow').style.display='block';
                   
                }
                if(y ==2)
                {
                    document.getElementById('resellershow1').style.display='none';
                    document.getElementById('resellershow').style.display='none';
                    
                }
            }


            function load()
            {
            document.getElementById('resellershow').style.display='none';
            }
   </script>
          
             
              
              
          
          
          <div class="frmFldArea">
      <label><span class="star" style="color: #fff;">*</span><?php echo $entry_fax; ?></label>
          <input type="text" name="fax" value="<?php echo $fax; ?>" class="inputTextFld" />
          </div>

    </div>
    <h2 class="prdct_name6"><?php echo $text_your_address; ?></h2>
    <div class="content">
          <div class="frmFldArea"> 
          <label><span class="star">*</span> <?php echo $entry_address_1; ?></label>
          <input type="text" name="address_1" value="<?php echo $address_1; ?>" class="inputTextFld" />
            <?php if ($error_address_1) { ?>
            <span class="error"><?php echo $error_address_1; ?></span>
            <?php } ?>
            </div>
            <div class="frmFldArea"> 
          <label> 
          <?php echo $entry_address_2; ?></label>
          <input type="text" name="address_2" value="<?php echo $address_2; ?>" class="inputTextFld" />
</div>
          <div class="frmFldArea"> 
          <label><span class="star">*</span> <?php echo $entry_city; ?></label>
          <input type="text" name="city" value="<?php echo $city; ?>" class="inputTextFld"  />
            <?php if ($error_city) { ?>
            <span class="error"><?php echo $error_city; ?></span>
            <?php } ?>
            </div>
<div class="frmFldArea"> 
          <label><span class="star" style="color: #fff;">*</span>
<div id="postcode-required" class="star">*</div> <?php echo $entry_postcode; ?></label>
          <input type="text" name="postcode" value="<?php echo $postcode; ?>" class="inputTextFld" />
            <?php if ($error_postcode) { ?>
            <span class="error"><?php echo $error_postcode; ?></span>
            <?php } ?>
            </div>
            
         <div class="frmFldArea"> 
          <label>
<div class="star">*</div> <?php echo $entry_country; ?></label>
<div class="dropfield">
         <select name="country_id" class="drop-se">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            </div>
            <?php if ($error_country) { ?>
            <span class="error"><?php echo $error_country; ?></span>
            <?php } ?>
            </div>
       
          <div class="frmFldArea"> 
          <label>
<div class="star">*</div><?php echo $entry_zone; ?></label>
<div class="dropfield" id="zone_id">
          <select name="zone_id" class="drop-se">
          <option value=""> --- Please Select --- </option>
            </select>
            </div>
            <?php if ($error_zone) { ?>
            <span class="error"><?php echo $error_zone; ?></span>
            <?php } ?>
            </div>
      
    </div>
    <h2 class="prdct_name6"><?php echo $text_your_password; ?></h2>
    <div class="content">
      <div class="frmFldArea"> 
          <label>
<div class="star">*</div> <?php echo $entry_password; ?></label>
          <input type="password" name="password" value="<?php echo $password; ?>" class="inputTextFld" />
            <?php if ($error_password) { ?>
            <span class="error"><?php echo $error_password; ?></span>
            <?php } ?></div>
       <div class="frmFldArea"> 
          <label>
<div class="star">*</div> <?php echo $entry_confirm; ?></label>
          <input type="password" name="confirm" value="<?php echo $confirm; ?>" class="inputTextFld" />
            <?php if ($error_confirm) { ?>
            <span class="error"><?php echo $error_confirm; ?></span>
            <?php } ?></div>
    </div>
    <h2 class="prdct_name6"><?php echo $text_newsletter; ?></h2>
    <div class="content">
      <div class="frmFldArea"> <label>
<div class="star">*</div>  <?php echo $entry_newsletter; ?></label>
          <div class="radioareea"><ul><?php if ($newsletter) { ?>
           <li> <input type="radio" name="newsletter" value="1" checked="checked" />
            <?php echo $text_yes; ?></li>
            <li><input type="radio" name="newsletter" value="0" />
            <?php echo $text_no; ?></li>
            <?php } else { ?>
           <li> <input type="radio" name="newsletter" value="1" />
            <?php echo $text_yes; ?></li>
            <li><input type="radio" name="newsletter" value="0" checked="checked" />
            <?php echo $text_no; ?></li>
            <?php } ?></ul></div>
    </div>
    <?php if ($text_agree) { ?>
    <div class="buttons">
      <div class="right">
      
      <span>&nbsp;<?php if ($agree) { ?>
        <input type="checkbox" name="agree" value="1" checked="checked" />
        <?php } else { ?>
        <input type="checkbox" name="agree" value="1" />
        <?php } ?><?php echo $text_agree; ?></span>
        
        
      </div>
      <input type="submit" value="" class="cretaccuntAra-register" style="margin-left:10px;" />
      <div class="register-requiredtext"><strong>*</strong> = required field</div>
    </div>
    <?php } else { ?>
    <div class="buttons">
      <div class="right">
        <input type="submit" value="" class="cretaccuntAra-register" style="margin-left:10px;" />
      </div>
    </div>
    <?php } ?>
  </form>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('input[name=\'customer_group_id\']:checked').live('change', function() {
	var customer_group = [];
	
<?php foreach ($customer_groups as $customer_group) { ?>
	customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_display'] = '<?php echo $customer_group['company_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['company_id_required'] = '<?php echo $customer_group['company_id_required']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_display'] = '<?php echo $customer_group['tax_id_display']; ?>';
	customer_group[<?php echo $customer_group['customer_group_id']; ?>]['tax_id_required'] = '<?php echo $customer_group['tax_id_required']; ?>';
<?php } ?>	

	if (customer_group[this.value]) {
		if (customer_group[this.value]['company_id_display'] == '1') {
			$('#company-id-display').show();
		} else {
			$('#company-id-display').hide();
		}
		
		if (customer_group[this.value]['company_id_required'] == '1') {
			$('#company-id-required').show();
		} else {
			$('#company-id-required').hide();
		}
		
		if (customer_group[this.value]['tax_id_display'] == '1') {
			$('#tax-id-display').show();
		} else {
			$('#tax-id-display').hide();
		}
		
		if (customer_group[this.value]['tax_id_required'] == '1') {
			$('#tax-id-required').show();
		} else {
			$('#tax-id-required').hide();
		}	
	}
});

$('input[name=\'customer_group_id\']:checked').trigger('change');
//--></script> 
<script type="text/javascript">
$('select[name=\'country_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=account/register/country&country_id=' + this.value,
		dataType: 'json',
		beforeSend: function() {
			$('select[name=\'country_id\']').after('<span class="wait">&nbsp;<img src="catalog/view/theme/default/image/loading.gif" alt="" /></span>');
		},
		complete: function() {
			$('.wait').remove();
		},			
		success: function(json) {
			if (json['postcode_required'] == '1') {
				$('#postcode-required').show();
			} else {
				$('#postcode-required').hide();
			}
                        html = '<div class="option" data-id="" data-text="<?php echo $text_select; ?>"><?php echo $text_select; ?></div>';
			html1 = '<option value=""><?php echo $text_select; ?></option>';
			if (json['zone'] != '') {
				for (i = 0; i < json['zone'].length; i++) {
        			html += '<div class="option" data-id="' + json['zone'][i]['zone_id'] + '" data-text="'+json['zone'][i]['name']+'"';
	    			html1 += '<option value="' + json['zone'][i]['zone_id'] + '"';
					if (json['zone'][i]['zone_id'] == '<?php echo $zone_id; ?>') {
	      				html1 += ' selected="selected"';
	    			}
	
	    			html += '>' + json['zone'][i]['name'] + '</div>';
                                html1 += '>' + json['zone'][i]['name'] + '</option>';
				}
			} else {
				html += '<div class="option" data-id="" data-text="<?php echo $text_none; ?>"><?php echo $text_none; ?></div>';
                                html1 += '<option value="0" selected="selected"><?php echo $text_none; ?></option>';
			}
			
			$('#zone_id .selectify .options').html(html);
                        $('select[name=\'zone_id\']').html(html1);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$('select[name=\'country_id\']').trigger('change');
</script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		width: 640,
		height: 480
	});
});
//--></script>
</div>
</div> 
<?php echo $footer; ?>
<?php if (count($currencies) > 1) { ?>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="curreny_form">
  <div class="currency">
  <dl id="ShopperPressCurrency" class="dropdown">
    <?php foreach ($currencies as $currency) { ?>
    <?php if ($currency['code'] == $currency_code) { ?>
<dt><a href="javascript:void(0)"><span><img src="catalog/view/theme/getfloss/images/money_<?php echo $currency['code']?>.png" border="0" alt="a" class="flag"  /><?php echo $currency['title']; ?><img src="catalog/view/theme/getfloss/images/lan-arrow.png" style="float:right;margin:0;" border="0" align="middle" class="pull" alt="arrow" /></span></a>
</dt>
<?php } ?>
<?php } ?>
<dd>
<ul>
<?php foreach ($currencies as $currency) { ?>  
<li><a onclick="$('input[name=\'currency_code\']').attr('value', '<?php echo $currency['code']?>'); $('#curreny_form').submit();" title="<?php echo $currency['title']; ?>"><img src="catalog/view/theme/getfloss/images/money_<?php echo $currency['code']?>.png" border="0" class="flag" alt="a" align="middle" />&nbsp;&nbsp;&nbsp;&nbsp;  <?php echo $currency['title']?></a></li>
<?php } ?>
</ul>
</dd>
</dl>
</div>
  <div id="currency"><?php echo $currency_code; ?><br />
    <input type="hidden" name="currency_code" value="" />
    <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
  </div>
</form>
<?php } ?>

<div class="box">
  <div class="catPag_box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
	 <div class="catgory_lft_optn_area">
		<div class="catOptnUl">
        	<ul>
   
      <?php foreach ($categories as $category) { ?>
      <li>
        <?php if ($category['category_id'] == $category_id) { ?>
        <a class="active" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php } else { ?>
        <a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
        <?php if ($category['children']) { ?>
        <ul>
          <?php foreach ($category['children'] as $child) { ?>
           <li>
            <?php if ($child['category_id'] == $child_id) { ?>
            <a href="<?php echo $child['href']; ?>" class="active"> - <?php echo $child['name']; ?></a>
            <?php } else { ?>
            <a href="<?php echo $child['href']; ?>"> - <?php echo $child['name']; ?></a>
            <?php } ?>
          </li>
          <?php } ?>
        </ul>
        <?php } ?>
      </li>
      <?php } ?>
    </ul>
  </div>
</div>
</div>
</div>
<script>
$(document).ready(function(e) {
	$(".catgoryTabb > span").addClass("catgoryTabb-arrow");
    $(".catgoryTabb").click(function(){
	$(".catgoryTabb > span").toggleClass("actv_arrow");
	$(".box-content").slideToggle();
		});
});
</script>



<!--<div class="box">
<div class="catgoryTabb">Narrow Your Results <span></span></div>
  <div class="catPag_box-heading"><?php //echo $heading_title; ?></div>
  <div class="box-content">
    
    <!--option part open-->
    <!--<div class="catgory_lft_optn_area">
    	<div class="optn_heading">Current Offers </div>
        <div class="catOptnUl">
        	<ul>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>On Sale</p><span>(380)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Free Shipping Eligible</p><span>(1587)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Special Offers</p><span>(438)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Package Deals</p><span>(13)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Clearance &amp; More</p><span>(677)</span></li>-->
            <!--<li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p></p><span></span></li>
            </ul>
        </div>
    </div>
    <!--option part closed-->
    
    <!--option part open-->
<!--    <div class="catgory_lft_optn_area">
    	<div class="optn_heading">Screen Size </div>
        <div class="catOptnUl">
        	<ul>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>14" and Under</p><span>(1071)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>15" - 16"</p><span>(592)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>17" and Up</p><span>(165)</span></li>-->
            <!--<li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p></p><span></span></li>-->
       <!--     </ul>
        </div>
    </div>-->
    <!--option part closed-->
    
    <!--option part open-->
   <!-- <div class="catgory_lft_optn_area">
    	<div class="optn_heading">Brand </div>
        <div class="catOptnUl">
        	<ul>
			<li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>HP</p><span>(487)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Asus</p><span>(126)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Lenovo</p><span>(248)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Dell</p><span>(471)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Samsung</p><span>(41)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Apple®</p><span>(79)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Acer</p><span>(169)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Toshiba</p><span>(82)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>See All</p><span>(32)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p></p><span></span></li>
            </ul>
        </div>
    </div>-->
    <!--option part closed-->
    
    <!--option part open-->
    <!--<div class="catgory_lft_optn_area">
    	<div class="optn_heading">Price </div>
        <div class="catOptnUl">
        	<ul>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Built-in Webcam</p><span>(1298)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Ultraportable</p><span>(1022)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>ENERGY STAR Certified</p><span>(781)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>HDMI Output</p><span>(681)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Touch Screen</p><span>(456)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Backlit Keyboard</p><span>(421)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Wireless Display</p><span>(155)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>2-in-1</p><span>(72)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Blu-ray Drive</p><span>(36)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>4G LTE Enabled</p><span>(3)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p></p><span></span></li>
            </ul>
        </div>
    </div>-->
    <!--option part closed-->
    
    <!--option part open-->
   <!-- <div class="catgory_lft_optn_area">
    	<div class="optn_heading">Laptop Features </div>
        <div class="catOptnUl">
        	<ul>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Built-in Webcam</p><span>(1298)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Ultraportable</p><span>(1022)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>ENERGY STAR Certified</p><span>(781)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>HDMI Output</p><span>(681)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Touch Screen</p><span>(456)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Backlit Keyboard</p><span>(421)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Wireless Display</p><span>(155)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>2-in-1</p><span>(72)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>Blu-ray Drive</p><span>(36)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p>4G LTE Enabled</p><span>(3)</span></li>
            <li><input tabindex="1" type="checkbox" id="input-1" class="demo-list"><p></p><span></span></li>
            </ul>
        </div>
    </div>-->
    <!--option part closed-->
<!--    
  </div>
</div>
-->

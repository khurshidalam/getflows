<div style="width:100%; float:left; background:url(catalog/view/theme/getfloss/images/slider-bg.png) repeat-x; "><!--position:absolute; top:0px; z-index:9999; background:#0F0;-->

<div class="innerwrapper">
  <div id="notification">
</div>
</div>

  </div>
 
<div class="banner-section"> 
<div class="innerwrapper">
<div class="flexslider">
<ul class="slides">
<?php foreach ($banners as $banner) { ?>
<li>
<div class="da-img">
<?php if ($banner['link']) { ?>
<a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
<?php } else { ?>
<img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
<?php } ?>
</div>
<div class="flexslider-con">
<h2><?php echo $banner['title']; ?></h2>
<?php echo html_entity_decode($banner['content'], ENT_QUOTES, 'UTF-8'); ?>
<?php if ($banner['link']) { ?>
<div class="da-link"><a href="<?php echo $banner['link']; ?>"><img src="catalog/view/theme/getfloss/images/sli-ordernow.png "/></a></div>
<?php } ?>
</div>
</li>
<?php } ?>  
</ul>
</div>
</div>
</div>

<!--<div class="slideshow">
<div class="sliderwrapper">
  
 <div id="slideshow" class="nivoSlider">
  
 
      <?php foreach ($banners as $banner) { ?>
    <?php if ($banner['link']) { ?>
    <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" /></a>
    <?php } else { ?>
    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" />
    <?php } ?>
    <?php } ?>
  </div>

  
  

 </div> 
</div>-->
<!--<script type="text/javascript">
$(document).ready(function() {
	$('#slideshow').nivoSlider();
});
</script>-->

<?php echo $header; ?>
<script>
    function quicksubmit()
    {
	var searchurl=$('#quickerror').val();
	$.post('index.php?route=information/contact/quicksubmit',{'str': searchurl},
	       function(data)
	       {
		alert(data);
		});
    }
</script>
<div class="cretaccuntAra">
<div class="innerwrapper">

<div class="breadcrumb">
<?php foreach ($breadcrumbs as $breadcrumb) { ?>
<a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
<?php } ?>
</div>

<?php if($heading_title != 'page not found'){?>
<div id="content" style="width:100%"><?php echo $content_top; ?>
<h1 class="prdct_name"><span><?php echo $heading_title; ?></span></h1>
<div class="content"><?php echo $text_error; ?></div>
<?php echo $content_bottom; ?>
</div>
<?php }
else{ ?>
<div class="error-warp">
<div class="heading">Page not found</div>
<div class="subheading">Sorry, but the page you were looking for can't be found. See below for what you can do about that</div>
<div class="leftpanel">
<div class="quick-errorreport">Quick error report</div>
<div class="quick-errorreportsubtext">You can quickly report this missing page by clicking the button below</div>
<div class="quick-reportbutton"><a onclick="quicksubmit();"><img src="catalog/view/theme/getfloss/images/submit-quickreport.png" alt=""/></a></div>
<input id="quickerror" type="hidden" value="<?php echo $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>"/>
</div>
<div class="leftpanel" style="float:right;width:46%;">
<div class="quick-errorreport">Feedback request</div>
<div class="quick-errorreportsubtext" style="margin-bottom: 30px;">If you'd like some feedback about the content you are looking for, please fill in the form below and I'll get back to you.</div>
<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
<div class="quick-fromrow">
<label>* Name</label>
<input type="text" class="quick-textfield" value="" name="name">
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
</div>
<div class="quick-fromrow">
<label>* Email</label>
<input type="email" class="quick-textfield" value="" name="email">
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
</div>
<div class="quick-fromrow">
<label>Message</label>
<textarea name="enquiry" class="quick-messagefield"></textarea>
<?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
</div>
<input name="submit" type="submit" class="quick-sendmessage" value="">
</form>
</div>
</div>
<?php }?>
</div>
</div>
<?php echo $footer; ?>

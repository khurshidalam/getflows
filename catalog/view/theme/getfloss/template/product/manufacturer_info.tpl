<?php echo $header; ?>
<div class="infoConAra">
<div class="innerwrapper">
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php if ($products) { ?>
  <div class="product-filter">
   <!-- <div class="display"><b><?php /*echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; */?></a></div>-->
    <div class="limit"><span style="color: #fff;"><?php echo $text_limit; ?></span>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort"><span style="color: #fff;"><?php echo $text_sort; ?></span>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected">
	<?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
 <!-- <div class="product-compare"><a href="<?php /*echo $compare; ?>" id="compare-total"><?php echo $text_compare; */?></a></div>-->
  <div class="resp-tabs-container">
		 <div class="product-list">
    <?php foreach ($products as $product) {  ?>
    
    
    
    <div class="prodctPrt">
    
          <!--left part open-->
      <div class="lftPrt">
      	<div class="proInfo">
      <!--<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>-->
<div class="name"><a href="<?php echo $product['href']; ?>"><span>
<?php   //echo $product['date_modified'];
$added_date= $product['date_modified'];
 $modified_date= $product['date_added'];
 $current_date=date("Y-m-d");

 $added_date = explode("-", $added_date);
$modified_date = explode("-", $modified_date);
$current_date = explode("-", $current_date);
$t=($modified_date[1]-$current_date[1]);
$t1=($added_date[1]-$current_date[1]);
if($t==0 OR $t1==0){
echo "New!";
}
?>

</span> <?php echo $product['name']; ?></a></div>
      <div class="description">
     
      <div class="model_info">
        
      <div class="modelnamehd">Model:</div>
      <div class="modlname"><?php echo $product['model']; ?></div>
      <div class="modelnamehd">SKU:</div>
      <div class="modlname"><?php echo $product['sku']; ?></div>
      </div>
       <div class="prodct_detail">
          <ul>
            <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
             <?php if($attribute_group['attribute_group_id']==7 or $attribute_group['attribute_group_id']==8 or $attribute_group['attribute_group_id']==9){ ?>
            <li> <?php echo $attribute['text']; ?></li>
             <?php } ?>
            <?php } ?>
            <?php } ?>
          </ul>
          </div>
    <div class="rateAra">Customer Reviews:<span class="star"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></span> <?php echo $product['rating']; ?> of 5 <a>(<?php echo $product['reviews']; ?> reviews)</a></div> 
       <a href="<?php echo $product['href']; ?>" class="checklink">Check Shipping &amp; Availability ›</a>  
      </div>
      </div><!--proInfo div closed-->
      </div>
      <!--left part closed-->
        <!--right part open-->
      <?php if ($product['price']) { ?>
      <div class="price">
       <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
        
     
      
        <div class="sub_price">Reg. Price: <span><?php echo $product['special']; if($product['special']=='' ) echo $product['price']; ?></span> You Save: <span>
	<?php $a=str_replace(',','',$product['price']); $b=str_replace(',','',$product['special']); ?>
         
         <?php $ab= (substr($a,1,10))-(substr($b,1,10)); if(substr($a,1,10)== $ab) echo '$0.00'; else echo $ab; ?></span></div>
        <div class="ulara"> 
             <ul>
           <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
              <?php if($attribute_group['attribute_group_id']==3){ ?>
            <li><?php echo $attribute['text']; ?></li>
             <?php } ?>
            <?php } ?>
            <?php } ?>
             </ul>
       </div>
     </div>
      <?php } ?>
      
      <div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="add2cart" />
      </div>
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><i><img src="catalog/view/theme/getfloss/custom_images/category-page-image/wish-icon.png" width="9" height="8" /></i><?php echo $button_wishlist; ?></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" style="display:none;"><?php echo $button_compare; ?></a></div>
      <!--right part closed-->
      <?php if ($product['thumb']) { ?>
      <div class="image" style="float:left;">
      <a href="<?php echo $product['href']; ?>">
      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
            </a>
      </div>
      <?php } ?>
    </div><!--part closed-->
    <?php } ?>
  </div>
       </div>  
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php }?>
  <?php echo $content_bottom; ?></div></div>
  </div>
<script type="text/javascript">
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			
			
			
			html = '<div class="lftPrt">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			html += '</div>';
			
			html += '<div class="proInfo">';
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
                        
			html += '</div>';
			
			
			
			html  += '<div class="right">';
			var price = $(element).find('.price').html();
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			html += '</div>';
						
			
			
			
					
			
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			html += '</div>';
			
						
			$(element).html(html);
		});	
			
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
						
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.totalStorage('display', 'grid');
	}
}


view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script>

<?php echo $footer; ?>
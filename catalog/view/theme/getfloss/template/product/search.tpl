<?php echo $header; ?>
<style>
  .back_btnne a img {
    padding-top: 7px;
}
</style>
<div class="searchrnAra">
<div class="innerwrapper">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><span><?php echo $heading_title; ?></span></h1>
<?php //echo $column_left; ?><?php //echo $column_right; ?>
<div id="content" style="width:100%">

<?php echo $content_top; ?>

  <!--<b><?php echo $text_critea; ?></b>-->
  <div class="catPag_box-heading_2">
    <p><span><?php echo $entry_search; ?></span>
      <?php if ($search) { ?>
      <input type="text" name="search" value="<?php echo $search; ?>" />
      <?php } else { ?>
      <input type="text" name="search" value="<?php echo $search; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
      <?php } ?>
      <select name="category_id">
        <option value="0"><?php echo $text_category; ?></option>
        <?php foreach ($categories as $category_1) { ?>
        <?php if ($category_1['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_1['children'] as $category_2) { ?>
        <?php if ($category_2['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_2['category_id']; ?>" selected="selected"><?php echo $category_2['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_2['category_id']; ?>"><?php echo $category_2['name']; ?></option>
        <?php } ?>
        <?php foreach ($category_2['children'] as $category_3) { ?>
        <?php if ($category_3['category_id'] == $category_id) { ?>
        <option value="<?php echo $category_3['category_id']; ?>" selected="selected"><?php echo $category_3['name']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $category_3['category_id']; ?>"><?php echo $category_3['name']; ?></option>
        <?php } ?>
        <?php } ?>
        <?php } ?>
        <?php } ?>
      </select>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="seerchBtn" style="float:none"/>
    </p>
    <p style="margin-top:10px;">
    <div class="spaccc">&nbsp;</div>
    <?php if ($sub_category) { ?>
      <input type="checkbox" name="sub_category" value="1" id="sub_category" checked="checked" />
      <?php } else { ?>
      <input type="checkbox" name="sub_category" value="1" id="sub_category" />
      <?php } ?>
      <label for="sub_category"><?php echo $text_sub_category; ?></label>
    <?php if ($description) { ?>
    <input type="checkbox" name="description" value="1" id="description" checked="checked" />
    <?php } else { ?>
    <input type="checkbox" name="description" value="1" id="description" />
    <?php } ?>
    <label for="description"><?php echo $entry_description; ?></label>
    </p>
  </div>
  <!--<div class="buttons">
    <div class="right"><input type="button" value="<?php echo $button_search; ?>" id="button-search" class="button" /></div>
  </div>-->
  <h2><?php echo $text_search; ?></h2>
  <?php if ($products) { ?>

<div class="resp-tabs-container">
		 <div class="product-list">
    <?php foreach ($products as $product) {  ?>
    
    
    <div class="prodctPrt">
    
          <!--left part open-->
      <div class="lftPrt">
      	<div class="proInfo">
      <!--<div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>-->
<div class="name"><a href="<?php echo $product['href']; ?>"><span>
<?php   //echo $product['date_modified'];
$added_date= $product['date_modified'];
 $modified_date= $product['date_added'];
 $current_date=date("Y-m-d");

 $added_date = explode("-", $added_date);
$modified_date = explode("-", $modified_date);
$current_date = explode("-", $current_date);
$t=($modified_date[1]-$current_date[1]);
$t1=($added_date[1]-$current_date[1]);
if($t==0 OR $t1==0){
echo "New!";
}
?>

</span> <?php echo $product['name']; ?></a></div>
      <div class="description">
     <?php echo $product['description']; ?>
      <div class="model_info">
        
      <div class="modelnamehd">Model:</div>
      <div class="modlname"><?php echo $product['model']; ?></div>
      <?php if($product['sku']!='') { ?>
      <div class="modelnamehd">SKU:</div>
      <div class="modlname"><?php echo $product['sku']; ?></div>
      <?php } ?>
      </div>
       <div class="prodct_detail">
          <ul>
            <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
             <?php if($attribute_group['attribute_group_id']==7 or $attribute_group['attribute_group_id']==8 or $attribute_group['attribute_group_id']==9){ ?>
            <li> <?php echo $attribute['text']; ?></li>
             <?php } ?>
            <?php } ?>
            <?php } ?>
          </ul>
          </div>
    <div class="rateAra">Customer Reviews:<span class="star"><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['reviews']; ?>" /></span> <?php echo $product['rating']; ?> of 5 <a>(<?php echo $product['reviews']; ?> )</a></div> 
      </div>
      </div><!--proInfo div closed-->
      </div>
      <!--left part closed-->
        <!--right part open-->
      <?php if ($product['price']) { ?>
      <div class="price">
       <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new"><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <br />
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
        
     
      
        <div class="sub_price">Reg. Price: <span><?php echo $product['special']; if($product['special']=='' ) echo $product['price']; ?></span> You Save: <span>
	<?php $a=str_replace(',','',$product['price']); $b=str_replace(',','',$product['special']); ?>
         
         <?php $ab= (substr($a,1,10))-(substr($b,1,10)); if(substr($a,1,10)== $ab) echo '$0.00'; else echo $ab; ?></span></div>
        <div class="ulara"> 
             <ul>
           <?php foreach($product['attribute_groups'] as $attribute_group) { ?>
            <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
              <?php if($attribute_group['attribute_group_id']==3){ ?>
            <li><?php echo $attribute['text']; ?></li>
             <?php } ?>
            <?php } ?>
            <?php } ?>
             </ul>
       </div>
     </div>
      <?php } ?>
      <?php if (!$product['asin']) { ?>
      <div class="cart">
        <input type="button" value="<?php echo $button_cart; ?>" onclick="addToCart('<?php echo $product['product_id']; ?>');" class="add2cart" />
      </div>
      <?php }?>
      <?php if ($product['asin']) { ?>
      <div class="back_btnne"> <a href="<?php echo $product['href']; ?>" style="margin-top: 25px;" ><img src="catalog/view/theme/getfloss/image/viewdetails-btn.png" alt="view details" /></a> </div>
      <?php }?>
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');"><i><img src="catalog/view/theme/getfloss/custom_images/category-page-image/wish-icon.png" width="9" height="8" /></i><?php echo $button_wishlist; ?></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" style="display:none;"><?php echo $button_compare; ?></a></div>
      <!--right part closed-->
      <?php if ($product['thumb']) { ?>
      <div class="image" style="float:left;">
      <a href="<?php echo $product['href']; ?>">
      <img src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
            </a>
      </div>
      <?php } ?>
    </div><!--part closed-->
    <?php } ?>
  </div>
       </div>                    

  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php }?>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#content input[name=\'search\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').bind('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').attr('disabled', 'disabled');
		$('input[name=\'sub_category\']').removeAttr('checked');
	} else {
		$('input[name=\'sub_category\']').removeAttr('disabled');
	}
});

$('select[name=\'category_id\']').trigger('change');

$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';
	
	var search = $('#content input[name=\'search\']').attr('value');
	
	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').attr('value');
	
	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}
	
	var sub_category = $('#content input[name=\'sub_category\']:checked').attr('value');
	
	if (sub_category) {
		url += '&sub_category=true';
	}
		
	var filter_description = $('#content input[name=\'description\']:checked').attr('value');
	
	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			
			
			
			html = '<div class="lftPrt">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
			html += '</div>';
			
			html += '<div class="proInfo">';
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
                        
			html += '</div>';
			
			
			
			html  += '<div class="right">';
			var price = $(element).find('.price').html();
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			if ($(element).find('.back_btnne').html() != null) {
			  html += '  <div class="back_btnne">' + $(element).find('.back_btnne').html() + '</div>';
			}
			if ($(element).find('.cart').html() != null) {
			html += '  <div class="cart">' + $(element).find('.cart').html() + '</div>';
			}
			html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			html += '</div>';
						
			
			
			
					
			
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			html += '</div>';
			
						
			$(element).html(html);
		});	
			
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';
			
			var price = $(element).find('.price').html();
			
			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
						
			html += '<div class="cart">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.compare').html() + '</div>';
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.totalStorage('display', 'grid');
	}
}


view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
</div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<div class="infoConAra">
<div class="innerwrapper">
<div id="content">
  
  	
  	<div class="content"><p><?php echo $text_conditions ?></p></div>
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="testimonial">
	<div class="content">
          <div class="frmFldArea2">
            <label><?php echo $entry_title ?></label>
              <input type="text" name="title" value="<?php echo $title; ?>" class="inputTextFld" size = 90 />
              <?php if ($error_title) { ?>
              <span class="error"><?php echo $error_title; ?></span>
              <?php } ?>
          </div>
          <div class="frmFldArea2">
            <label><?php echo $entry_enquiry ?><span class="required">*</span></label>
              <textarea name="description" class="textarea2" rows="10"><?php echo $description; ?></textarea>
              <?php if ($error_enquiry) { ?>
              <span class="error"><?php echo $error_enquiry; ?></span>
              <?php } ?>
          </div>
          <div class="frmFldArea2">
            <label><?php echo $entry_name ?></label>
              <input type="text" name="name" class="inputTextFld" value="<?php echo $name; ?>" />
              <?php if ($error_name) { ?>
              <span class="error"><?php echo $error_name; ?></span>
              <?php } ?>
          </div>
         <div class="frmFldArea2">
            <label><?php echo $entry_city ?></label>
			<input type="text" name="city" class="inputTextFld" value="<?php echo $city; ?>" />
		</div>
          <div class="frmFldArea2">
            <label>
		  <?php echo $entry_email ?></label>
              <input type="text" name="email" class="inputTextFld" value="<?php echo $email; ?>" />
              <?php if ($error_email) { ?>
              <span class="error"><?php echo $error_email; ?></span>
              <?php } ?>
          </div>
           <div class="frmFldArea2">
            <label><?php echo $entry_rating; ?> </label><i><?php echo $entry_good; ?></i>
        		<input type="radio" name="rating" value="1" style="margin: 0;" <?php if ( $rating == 1 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="2" style="margin: 0;" <?php if ( $rating == 2 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="3" style="margin: 0;" <?php if ( $rating == 3 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="4" style="margin: 0;" <?php if ( $rating == 4 ) echo 'checked="checked"';?> />
        		&nbsp;
        		<input type="radio" name="rating" value="5" style="margin: 0;" <?php if ( $rating == 5 ) echo 'checked="checked"';?> />
        		&nbsp; <i><?php echo $entry_bad; ?></i>

          </div>
          <div class="frmFldArea2">
          <label>&nbsp;</label>
              
              <div class="capptch">
              <div><?php echo $entry_captcha; ?><span class="required">*</span></div>
              <img src="index.php?route=information/contact/captcha" style="float:left;" />
              <input type="text" name="captcha" class="inputTextFld" value="<?php echo $captcha; ?>" />
              <?php if ($error_captcha) { ?>
              <span class="error"><?php echo $error_captcha; ?></span>
              <?php } ?>
              </div>
          </div>    
		<div class="frmFldArea2">
          <label>&nbsp;</label>
          <a  onclick="$('#testimonial').submit();" class="yellow_button space_right" style="float:left;"><span><?php echo $button_send; ?></span></a>
          <a class="gray_button" href="<?php echo $showall_url;?>"><span><?php echo $show_all; ?></span></a>
          </div>
        
        
        
        <!--<div class="frmFldArea2">
<label>&nbsp;</label>
              <input type="text" name="captcha" class="inputTextFld" value="<?php echo $captcha; ?>" /><br>
		</div>-->
	  </div>
      <!--<div class="buttons">
        <table width=100%>
          <tr>
            <td width=50%><a  onclick="$('#testimonial').submit();" class="button"><span><?php echo $button_send; ?></span></a></td>
		<td align="right"><a class="button" href="<?php echo $showall_url;?>"><span><?php echo $show_all; ?></span></a>
		</td>
          </tr>
        </table>

      </div>-->
    </form>
  
</div>

</div>
</div>
<?php echo $footer; ?> 
<?php echo $header; ?>
<div class="productDetlAra">
<div class="innerwrapper">
<!--breadcrumb open-->
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
<!--breadcrumb closed-->

<?php echo $column_left; ?>
<?php echo $column_right; ?>
<div id="content">

<!--top area open-->
<div class="prdctDetlTop">

<div class="lftaara">

<!-- iamge part open-->
<div class="prodct_dtl_image_area">
<?php if ($thumb) { ?>
      <div class="image">
      <img src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>"  />
      </div><!--colorbox-->
      <?php } ?>
         
      <div class="image2">
                    <?php if ($images) { ?>
             
              
	<ul id="flexiselDemo5">
<?php foreach ($images as $image) { ?>
          <li><a href="<?php echo $image['popup']; ?>" title="<?php echo $heading_title; ?>" class="colorbox cboxElement"><img src="<?php echo $image['thumb']; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></li>
     <?php } ?>     
    </ul>


              <?php } ?>
              </div>
              </div>
<!-- image part closed -->

<!--mid detail area open-->
      <div class="detalAra">
      <h1 class="prdct_name3"><?php echo $heading_title; ?></h1>
      <div class="prdct_description">
      <?php
      if(strlen($description > 450))
      {
      echo substr($description,0,450).'...';
      }
      else echo $description;
      ?>
      </div>
      
<!--          <div class="prdctFullName">
          <?php if ($manufacturer) { ?>
        <span><?php echo $text_manufacturer; ?></span>  <?php echo $manufacturer; ?>
        <?php } ?>
          </div>-->
      
        
          <!--<div class="model_info_2">
          <div class="modelnamehd">Model:</div><div class="modlname"><?php echo $model; ?></div>
          
          </div>-->

      
          <!--<div class="rateAra-2">Customer Reviews:<span class="star">  
	 
	  <?php if ($review_status) { ?>
        <img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp; &nbsp; <a onclick='$("#tab-review").trigger("click")'> ( <?php echo $reviews; ?> )</a>
	<?php } ?>
      </span></div>-->
	  <div style="clear:both;"></div>
      
      
	  <!--<p>&nbsp;</p>-->
	   <!--<div class="share">
          <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
          <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script> 
          </div>-->
          <div class="proprice"> <?php if ($price) {if ($special) {  echo $special;  }else {echo $price;} } ?> <span class="linethrow"><?php if ($old_price) {if ($special) {  echo $old_price;  } } ?></span>
	  <?php if($target_url){?><span style="font-size:11px; font-weight: normal; vertical-align: middle">(as of <?php echo $date_added;?> - <a rel="nofollow" target="_blank" href="http://www.amazon.com/o/ASIN/<?php echo $asin;?>/?tag=getflo05-20">info</a>)</span><?php }?>
	  </div>
          
          
          <div class="propice_area">
          <!-- left open -->
          <div class="propice_left_area">
          <?php if ($price) { 
      
        
	 $a= substr($price,1,10); 
	 $aa= substr($special,1,10);
	 if($aa!=''){
	$c= ($a-$aa);
	 }
	 else
	 {
	$c=0;
	 }
	
       
      } ?>
      		<!--<div class="sub_price-2">Old Price: <span><?php if ($price) {if ($special) {  echo $price;  } if (!$special) {  echo $price;  } } ?></span> You Save: <span><?php echo '$'.$c; ?></span></div>-->
         
	<!--  <a class="checklink-2" href=""><?php echo $text_stock; ?> <?php echo $stock; ?> </a>-->
        <div class="price-addbtn">  
        <?php  //echo $temp = $_REQUEST['product_id'];?>
        <?php if(!$target_url){?><input type="button" value="Add to Cart" onClick="addToCart('<?php echo $product_id; ?>');" class="add2cart-2"><?php }?>
        <?php if($target_url){?><div class="amazone-buynow"><a href="<?php echo $target_url;?>" target="_blank"><img src="catalog/view/theme/getfloss/images/amazone-btn.png" /></a></div><?php }?>
        </div>
          </div> 
          <!-- left closed--> 
          
          <!-- right open -->
          <!--<div class="propice_right_area">
         		 <a onClick="addToWishList('<?php echo $product_id; ?>');" class="add2Wishlist-2" >Add to Wish List</a>
	  			<a onClick="addToCompare('<?php echo $product_id; ?>');" class="add2Wishlist-2" ><?php echo $button_compare; ?></a>
          </div>-->
            <!-- right closed-->
           </div> 
            
          <div class="product-bottombtn">
          <div class="link1"><a onClick="addToWishList('<?php echo $product_id; ?>');">Add To Wishlist</a></div>
          <div class="link2"><a onClick="addToCompare('<?php echo $product_id; ?>');">Add To Compare</a></div>
          <!--<div class="link3"><a href="#">Email A Friend</a></div>-->
          </div>  
         
      </div>
      <!--mid detail area closed-->   
  </div>      
</div>

<?php if(!$target_url){?>
<div style="clear:both;"></div>
<div class="frequently-box">
  <div class="frequently-heading"><h2>Additional items</h2></div>
  <div class="frequently-innerbox">

  <div class="leftbox">
    <div class="leftbox-row">
      <img src="<?php echo $additionalthumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
      </div>
    <div class="leftbox-plusrow"><img src="catalog/view/theme/getfloss/images/cart-plusicon.jpg"/></div>
    <?php $apcount = 1;?>
    <?php foreach($additionalproducts as $ap){?>
      <div class="leftbox-row" id="image<?php echo $ap['product_id']?>"><img src="<?php echo $ap['thumb']; ?>" alt="<?php echo $ap['name']; ?>"  title="<?php echo $ap['name']; ?>"/></div>
      <?php if($apcount < 2){?>
      <div class="leftbox-plusrow" id="plus<?php echo $ap['product_id']?>"><img src="catalog/view/theme/getfloss/images/cart-plusicon.jpg"/></div>
      <?php }?>
      <?php $apcount++;?>
      <?php }?>
</div>
  <div class="rightbox">
    <div class="rightbox-text">Price for all items: <span id="allprice"><?php echo $adtotal;?></span> </div>
    <input type="button" onclick="addalltocart('<?php echo $product_id; ?>')" value="Add all items to Cart" class="right-addtocart" />
</div>
<?php foreach($additionalproducts as $ap){?>  
<div class="frequently-bottomtext2"><input class="chkNumber" type="checkbox" name="<?php echo $ap['name']?>" value="<?php echo $ap['product_id']?>" checked/>&nbsp;Add an <?php echo $ap['name'];?> to any kit for <font color="#990000" id="price<?php echo $ap['product_id']?>"><?php echo $ap['price'];?></font></div>
<?php }?>
  </div>
  </div>

  <script>
    $('.frequently-bottomtext2 :checkbox').click(function() {
    var $this = $(this);
    var pid = $this.val();
    var price = $('#price'+pid).html();
    price = price.replace('$', '');
    price = parseFloat(price);
    var allprice = $('#allprice').html();
    allprice = allprice.replace('$', '');
    allprice = parseFloat(allprice);
    
    // $this will contain a reference to the checkbox   
    if ($this.is(':checked')) {
	$('#image'+pid).show();
	$('#image'+pid).prev().show();
	var totprice = parseFloat(price+allprice);
    } else {
        $('#image'+pid).hide();
	$('#image'+pid).prev().hide();
	var totprice = parseFloat(allprice-price);
    }
	totprice = totprice.toFixed(2);
	$('#allprice').html('$'+totprice);
    });
    
    function addalltocart(pid)
    {
	addToCart(pid);
	$('.chkNumber:checked').each(function () {
            chkId = $(this).val();
	    addOthersToCart(pid,chkId);
        });
    }
  </script>
<?php }?> 

	
<!-- bottom tab area open-->
<div class="bottm-tab-ara" > 
 <div id="horizontalTab">
            <ul class="resp-tabs-list">
                <li id="description">Description</li>
                <!--<li id="comments">comments</li>-->
                <li id="tab-review">Ratings &amp; Reviews</li>
                <div class="share">
          <div class="addthis_default_style"><a class="addthis_button_compact"><?php echo $text_share; ?></a> <a class="addthis_button_email"></a><a class="addthis_button_print"></a> <a class="addthis_button_facebook"></a> <a class="addthis_button_twitter"></a></div>
          <script type="text/javascript" src="//s7.addthis.com/js/250/addthis_widget.js"></script> 
          </div>
            </ul>
            
            <div class="resp-tabs-container">
            <!--overview part open-->
 			 <div id="description">
          <div class="tabfull-description">
          <?php echo $description;?>
          </div>
          <div class="tabfull-bottomtext">
          <div class="naturaltext"><?php if(isset($category)){?><a href="<?php echo $category['href']?>"><?php echo $category['text']?></a><?php } else echo 'Featured';?></div>
          <div class="beautitext">
	    <?php foreach($tags as $tag){?>
	    <a href="<?php echo $tag['href'];?>"><?php echo $tag['tag'];?></a>
	    <?php }?>
	  </div>
          </div>
             </div>

             <!--<div id="comments">
              test
             
             </div>-->

             <div class="revwAra">
			
             	<div class="headttxt">Customer Reviews</div>
                <div class="revwStar"> <?php if ($products ) {  ?>
	  <?php if ($review_status) { ?>
        <img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="<?php echo $reviews; ?>" />&nbsp;&nbsp;&nbsp;<strong><?php echo $rating; ?>.0</strong> </a>
	<?php } ?>
      <?php } ?></div>
                <div class="revwTxtAra"><span><?php echo $rating; ?>0%</span> of reviewers would recommend this product </div>
                
   
		
		
		
		
		
                <a class="wrtRvwButn" id="showmenu" >Write a review</a>
		<div class="menu" style="display: none;" id="text" >
		    
	    
		<?php if ($review_status) { ?>
  <div id="tab-review" class="tab-content">
   
    <h2 id="review-title"><?php echo $text_write; ?></h2>
    <div class="frmFldArea2"><label><?php echo $entry_name; ?></label>
    <input type="text" name="name" value="" class="inputTextFld" />
    </div>
   
    <div class="frmFldArea2"><label><?php echo $entry_review; ?></label>
    <textarea name="text" cols="40" rows="8" class="inputTextFld"></textarea>
    <span class="txtspan"><?php echo $text_note; ?></span></div>
    
    <div class="frmFldArea2"><label><?php echo $entry_rating; ?></label>
     <span class="span"><?php //echo $entry_bad; ?></span>
    <input type="radio" name="rating" value="1" />
    &nbsp;
    <input type="radio" name="rating" value="2" />
    &nbsp;
    <input type="radio" name="rating" value="3" />
    &nbsp;
    <input type="radio" name="rating" value="4" />
    &nbsp;
    <input type="radio" name="rating" value="5" />
    &nbsp;<span class="span"><?php //echo $entry_good; ?></span></div>
    
    <div class="buttons">
      <div class="right"><a id="button-review" class="yellow_btn"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
</div>
		<!--<div style="clear:both;"></div>
               <p>&nbsp;</p>
                -->
               
                <!-- profile area open-->
             
                <!-- profile area closed-->
		<div style="clear:both;"></div>
                 <div id="review"></div>
                <!--member detail area open-->
                
                
                <!--<div class="memdetalara">
                 <div class="srtByAra">
                    <a class="postcomment">&nbsp;&nbsp;&nbsp;&nbsp;</a>
                </div>
                </div>-->
                <!--member detail area closed-->
                
             </div>

            </div>
        </div> 
</div>


<div style="clear:both;"></div>
<div class="infobighding"><span>Related Products</span></div>
<div class="product-relatedwarp">
<div class="relete_product_area">
<ul id="flexiselDemo4">
  <?php if ($products) { ?>
    <?php foreach ($products as $product) { ?>
      <li class="relet_prdct_box">
        <?php if ($product['thumb']) { ?>
        <div class="thumbimg"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" /></a></div>
        <?php } ?>
	<?php echo "<br>"; ?>
        <div class="name"><a href="<?php echo $product['href']; ?>" style="text-align: center;" ><span>New! </span><?php echo $product['name']; ?></a></div>
        <?php echo "<br>"; ?>
        <div class="relet_prdct_price">
	        <?php if ($product['price']) { ?>
     
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
         <?php echo $product['price']; ?><?php echo $product['special']; ?>
          <?php } ?>
       
        <?php } ?>
	
	</div>
        <?php if (!$product['asin']) { ?>
      <a class="add3cart" onclick="addToCart('<?php echo $product['product_id']; ?>');"  ><?php echo $button_cart; ?></a>
      <?php }?>
      <?php if ($product['asin']) { ?>
      <div class="back_btnne"> <a href="<?php echo $product['href']; ?>" style="margin-top: 25px;" ><img src="catalog/view/theme/getfloss/image/viewdetails-btn.png" alt="view details" /></a> </div>
      <?php }?>
	    
	</li>
     <?php } ?>
  <?php } ?>
  </ul>
</div> 

</div>


<!--<div style="clear:both;"></div>
<div class="infobighding"><span>Related Products</span></div>
<div class="relete_product_area">
  <ul id="flexiselDemo4">
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
    <li><div style="width:auto; float:left; height:100px; background:#999;">fsdfs df</div></li>
</ul> 
</div>-->

<script>

$(document).ready(function() {
    $('#showmenu').toggle(
        function() {
            $('.menu').slideDown("fast");
        },
        function() {
            $('.menu').slideUp("fast");
        }
    );
});
</script>

 





  
   
  <?php echo $content_bottom; ?></div>
  
 

</div>
</div>  
  
  
  
  
  
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});
//--></script> 
<script type="text/javascript"><!--

$('select[name="profile_id"], input[name="quantity"]').change(function(){
    $.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name="product_id"], input[name="quantity"], select[name="profile_id"]'),
		dataType: 'json',
        beforeSend: function() {
            $('#profile-description').html('');
        },
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
            
			if (json['success']) {
                $('#profile-description').html(json['success']);
			}	
		}
	});
});
    
$('#button-cart').bind('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, information, .error').remove();
			
			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
					}
				}
                
                if (json['error']['profile']) {
                    $('select[name="profile_id"]').after('<span class="error">' + json['error']['profile'] + '</span>');
                }
			} 
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					
				$('.success').fadeIn('slow');
					
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
});
//--></script>
<?php if ($options) { ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/ajaxupload.js"></script>
<?php foreach ($options as $option) { ?>
<?php if ($option['type'] == 'file') { ?>
<script type="text/javascript"><!--
new AjaxUpload('#button-option-<?php echo $option['product_option_id']; ?>', {
	action: 'index.php?route=product/product/upload',
	name: 'file',
	autoSubmit: true,
	responseType: 'json',
	onSubmit: function(file, extension) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').after('<img src="catalog/view/theme/default/image/loading.gif" class="loading" style="padding-left: 5px;" />');
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', true);
	},
	onComplete: function(file, json) {
		$('#button-option-<?php echo $option['product_option_id']; ?>').attr('disabled', false);
		
		$('.error').remove();
		
		if (json['success']) {
			alert(json['success']);
			
			$('input[name=\'option[<?php echo $option['product_option_id']; ?>]\']').attr('value', json['file']);
		}
		
		if (json['error']) {
			$('#option-<?php echo $option['product_option_id']; ?>').after('<span class="error">' + json['error'] + '</span>');
		}
		
		$('.loading').remove();	
	}
});
//--></script>
<?php } ?>
<?php } ?>
<?php } ?>

<script type="text/javascript"><!--
$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');
		
	$('#review').load(this.href);
	
	$('#review').fadeIn('slow');
	
	return false;
});			

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
$('#tabs a').tabs();
//--></script> 
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-timepicker-addon.js"></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	if ($.browser.msie && $.browser.version == 6) {
		$('.date, .datetime, .time').bgIframe();
	}

	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
	$('.datetime').datetimepicker({
		dateFormat: 'yy-mm-dd',
		timeFormat: 'h:m'
	});
	$('.time').timepicker({timeFormat: 'h:m'});
});
//--></script> 
<?php echo $footer; ?>
<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>


<div class="affilatdAra">
<div class="innerwrapper">

<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><?php echo $heading_title; ?></h1>


<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  
  <?php echo $text_description; ?>
  <div class="login-content">
    <div class="left">
      <h2><?php echo $text_new_affiliate; ?></h2>
      <div class="content"><?php echo $text_register_account; ?> <a href="<?php echo $register; ?>" class="yellow_btn" style="text-decoration:none; float:left; margin-top:10px;"><?php echo $button_continue; ?></a></div>
    </div>
    <div class="right">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <h2><?php echo $text_returning_affiliate; ?></h2>
        <div class="content">
          <p><?php echo $text_i_am_returning_affiliate; ?></p> 
          <div class="frmFldArea">
          <label><?php echo $entry_email; ?></label>
          <input type="text" name="email" value="<?php echo $email; ?>" class="inputTextFld" />
          </div>
          <div class="frmFldArea">
          <label><?php echo $entry_password; ?></label>
          <input type="password" name="password" value="<?php echo $password; ?>"  class="inputTextFld" />
          </div>
          <a href="<?php echo $forgotten; ?>" class="forgotpasas"><?php echo $text_forgotten; ?></a>
          <input type="submit" value="<?php echo $button_login; ?>" class="yellow_btn" style="float:left;" />
          <?php if ($redirect) { ?>
          <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
          <?php } ?>
        </div>
      </form>
    </div>
  </div>
  
 </div>
 </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?>
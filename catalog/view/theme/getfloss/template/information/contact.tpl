<?php echo $header; ?>
<div class="cntctConAra">
<div class="innerwrapper">
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><span><?php echo $heading_title; ?></span></h1>
  <div class="contactus-subtext">Have any question?</div>
  <div class="contactus-smalltext">Feel free to contact us anytime, our experts will respond as soon as possible!</div>
  <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <div class="formmAra">
<!--    <h2 class="prdct_name"><?php echo $text_contact; ?></h2>-->
    <div class="frmFldArea">
    <div class="frmhlfArea">
    <label><?php echo $entry_name; ?> *</label>
    <input type="text" name="name" class="inputTextFld" value="<?php echo $name; ?>" />
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    </div>
    <div class="frmhlfArea">
    <label>Last Name</label>
    <input type="text" name="lastname" class="inputTextFld" value="<?php echo $name; ?>" />
    </div>
    </div>
    
    <div class="frmFldArea">
    <div class="frmhlfArea">
    <label>Contact Number</label>
    <input type="text" name="contactnumber" class="inputTextFld" value="<?php echo $name; ?>" />
    </div>
    <div class="frmhlfArea">
    <label>E-Mail *</label>
    <input type="text" name="email" class="inputTextFld" value="<?php echo $email; ?>" />
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    </div>
    </div>
    <div class="frmFldArea">
    <label>Message *</label>
    <textarea name="enquiry" cols="40" rows="10"><?php echo $enquiry; ?></textarea>
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    </div>
    <div class="frmFldArea">
    <label>Security Code * &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="-1">Help us fight spam by completing the following</font></label>
    <img src="index.php?route=information/contact/captcha" alt="" />
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" class="capinput" />
        <div class="contact-requiredtext"><strong>*</strong> = required field</div>
    <input type="submit" value="" class="contact-submit" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>
    </div>
    
    </div>
  </form>
  
<!--  <div class="address_Ara">
      <div class="left">
      <h2 class="prdct_name"><?php echo $text_location; ?></h2>
      <div class="addressheading"><?php echo $text_address; ?></div>
      <p>
        <?php echo $store; ?><br />
        <?php echo $address; ?>
        </p>
        <?php if ($telephone) { ?>
        <div class="addressheading"><?php echo $text_telephone; ?></div>
        <p><?php echo $telephone; ?><br />
        <br />
        <?php } ?>
        <?php if ($fax) { ?>
        <b><?php echo $text_fax; ?></b><br />
        <?php echo $fax; ?>
        <?php } ?></p>
        </div>
    </div>-->
  
  <!--<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    
    
    
    <div class="formmAra">
    <h2 class="prdct_name"><?php echo $text_contact; ?></h2>
    <div class="frmFldArea">
    <label><?php echo $entry_name; ?></label>
    <input type="text" name="name" class="inputTextFld" value="<?php echo $name; ?>" />
    <?php if ($error_name) { ?>
    <span class="error"><?php echo $error_name; ?></span>
    <?php } ?>
    </div>
    
    <div class="frmFldArea">
    <label><?php echo $entry_email; ?></label>
    <input type="text" name="email" class="inputTextFld" value="<?php echo $email; ?>" />
    <?php if ($error_email) { ?>
    <span class="error"><?php echo $error_email; ?></span>
    <?php } ?>
    </div>
    <div class="frmFldArea">
    <label><?php echo $entry_enquiry; ?></label>
    <textarea name="enquiry" cols="40" rows="10"><?php echo $enquiry; ?></textarea>
    <?php if ($error_enquiry) { ?>
    <span class="error"><?php echo $error_enquiry; ?></span>
    <?php } ?>
    </div>
    <div class="frmFldArea">
    <label><?php echo $entry_captcha; ?></label>
    <img src="index.php?route=information/contact/captcha" alt="" />
    <input type="text" name="captcha" value="<?php echo $captcha; ?>" class="capinput" />
    <?php if ($error_captcha) { ?>
    <span class="error"><?php echo $error_captcha; ?></span>
    <?php } ?>
    </div>
    <div class="frmFldArea">
    <label>&nbsp;</label>
    <input type="submit" value="<?php echo $button_continue; ?>" class="submit" />
    </div>
    </div>
  </form>-->
  
  <?php echo $content_bottom; ?></div>
  
  </div>
  </div>
<?php echo $footer; ?>
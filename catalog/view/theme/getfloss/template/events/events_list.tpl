<?php echo $header; ?>

<div class="infoConAra">
<div class="innerwrapper">
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>

<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/events.css" />
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php //echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1 class="prdct_name"><?php echo $heading_title; ?></h1>
  <div class="buttons">
    <div class="left"><a href="<?php echo $create; ?>" class="gray_button"><?php echo $button_create_event; ?></a></div>
	<div class="right"><a class="gray_button" id="showSearch" style="margin-right: 10px"><span><?php echo $button_show_search; ?></span></a>
	<a class="gray_button" id="hideSearch" style="margin-right: 10px"><span><?php echo $button_hide_search; ?></span></a></div>
  </div>
	<div id="search_form" style="display:none;">
    <p><label><?php echo $entry_event_title; ?></label>
      <?php if ($filter_title) { ?>
      <input type="text" name="filter_title" value="<?php echo $filter_title; ?>" />
      <?php } else { ?>
      <input type="text" name="filter_title" value="<?php echo $filter_title; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
      <?php } ?></p>
    <p><label><?php echo $entry_event_name; ?></label>
      <?php if ($filter_name) { ?>
      <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" />
      <?php } else { ?>
      <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
      <?php } ?></p>
    <p><label><?php echo $entry_event_owner; ?></label>
      <?php if ($filter_owner) { ?>
      <input type="text" name="filter_owner" value="<?php echo $filter_owner; ?>" />
      <?php } else { ?>
      <input type="text" name="filter_owner" value="<?php echo $filter_owner; ?>" onclick="this.value = '';" onkeydown="this.style.color = '000000'" style="color: #999;" />
      <?php } ?></p>
  <div class="buttons">
    <div class="right"><input type="button" value="<?php echo $button_search; ?>" id="button-search" class="yellow_btn" /></div>
  </div>
  </div>
  <h2 class="hheding"><?php echo $text_search; ?></h2>
  <?php if ($events) { ?>
  <div class="wishlist-info">
     <div class="pricelist">
    <table>
      <thead>
        <tr>
          <td class="left">
		  <?php if ($sort == 'title') { ?>
			<a href="<?php echo $sort_title; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
		  <?php } else { ?>
			<a href="<?php echo $sort_title; ?>"><?php echo $column_title; ?></a>
		  <?php } ?></td>		  
		  
          <td class="left">
		  <?php if ($sort == 'start_date') { ?>
			<a href="<?php echo $sort_start_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_start_date; ?></a>
		  <?php } else { ?>
			<a href="<?php echo $sort_start_date; ?>"><?php echo $column_start_date; ?></a>
		  <?php } ?></td>		  
		  
          <td class="left">
		  <?php if ($sort == 'end_date') { ?>
			<a href="<?php echo $sort_end_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_end_date; ?></a>
		  <?php } else { ?>
			<a href="<?php echo $sort_end_date; ?>"><?php echo $column_end_date; ?></a>
		  <?php } ?></td>		  
		  
          <td class="left"><?php echo $column_status; ?></td>		  
		  
          <td class="left">
		  <?php if ($sort == 'type') { ?>
			<a href="<?php echo $sort_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_type; ?></a>
		  <?php } else { ?>
			<a href="<?php echo $sort_type; ?>"><?php echo $column_type; ?></a>
		  <?php } ?></td>		  
		  
          <td class="action"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <?php foreach ($events as $event) { ?>
      <tbody id="wishlist-row<?php echo $event['event_id']; ?>">
        <tr>
          <td class="name"><?php echo $event['title']; ?></td>
          <td class="name"><?php echo $event['start_date']; ?></td>
          <td class="name"><?php echo $event['end_date']; ?></td>
          <td class="name"><?php echo ($event['status'])  ?></td>
          <td class="name"><?php echo ($event['type'])  ?></td>
          <td class="action"><a href="<?php echo $event['href']; ?>"><?php echo $event['action']; ?></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>  </div>
  </div>
    <div class="pagination"><?php echo $pagination; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="yellow_btn"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_no_results; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="yellow_btn"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  </div> </div> </div>
<script type="text/javascript"><!--
$('#content input[name=\'filter_name\']').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('#button-search').bind('click', function() {
	url = 'index.php?route=events/events_list';
	
	var filter_name = $('#content input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_title = $('#content input[name=\'filter_title\']').attr('value');
	
	if (filter_title) {
		url += '&filter_title=' + encodeURIComponent(filter_title);
	}
	
	var filter_owner = $('#content input[name=\'filter_owner\']').attr('value');
	
	if (filter_owner) {
		url += '&filter_owner=' + encodeURIComponent(filter_owner);
	}

	location = url;
});

$("#frm_sendWL" ).hide();
$("#hideSearch").hide();
$('#showSearch').click(function() {
  $('#showSearch').hide();
  $('#hideSearch').show();
  $('#search_form').slideToggle('slow', function() {
  });
});
$('#hideSearch').click(function() {
  $('#showSearch').show();
  $('#hideSearch').hide();

  $('#search_form').slideToggle('slow', function() {
  });
});
//--></script> <?php echo $footer; ?>
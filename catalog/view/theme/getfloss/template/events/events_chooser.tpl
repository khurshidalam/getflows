<html>
<body>
<div id="content">
  <div class="content">
 <h2><?php echo $text_choose_event; ?></h2>
    <?php if ($events) { ?>
        <div id="select_event" class="option">
          <?php foreach ($events as $event) { ?>
          <input type="radio" name="event_id" value="<?php echo $event['event_id']; ?>" id="event-<?php echo $event['event_id']; ?>" onclick="$('#button_div').show();"/>
          <label for="event-<?php echo $event['event_id']; ?>"><?php echo $event['title']; ?></label>
          <br />
          <?php } ?>
        </div>
  <div class="buttons" style="margin-top: 30px;">
	<div class="left" id="button_div" style="display: none;" ><input type="button" value="<?php echo $button_select; ?>" class="button"  onclick="addToEvent('<?php echo $product_id; ?>',$('#select_event input[type=\'radio\']:checked').val())" /></div>
    <div class="right"><input type="button" value="<?php echo $button_cancel; ?>" id="button-cancel" onclick="$.colorbox.close();" class="button" /></div>
  </div>
    <?php } else {?>
        <div class="content">
			<h2><?php echo $text_no_events; ?></h2>
				<div class="buttons">
				<div class="left"><input type="button" value="<?php echo $button_cancel; ?>" id="button-cancel" onclick="$.colorbox.close();" class="button" /></div>
			</div>
    <?php } ?>
        </div>
</div>
</div>
</body></html>
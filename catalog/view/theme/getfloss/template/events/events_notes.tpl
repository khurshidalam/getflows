<?php if ($notes) { ?>
<?php foreach ($notes as $note) { ?>
<div class="review-list">
  <div class="author"><b><?php echo $note['author']; ?></b> <?php echo $text_on; ?> <?php echo $note['date_added']; ?></div>
  <div class="text"><?php echo $note['text']; ?></div>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_notes; ?></div>
<?php } ?>

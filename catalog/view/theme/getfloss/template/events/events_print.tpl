<!DOCTYPE html>
<html>
<title><?php echo $heading_title; ?></title>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style>
.wishlist-info table {
	width: 100%;
	border-collapse: collapse;
	border-top: 1px solid #DDDDDD;
	border-left: 1px solid #DDDDDD;
	border-right: 1px solid #DDDDDD;
	margin-bottom: 20px;
}
.wishlist-info td {
	padding: 7px;
}
.wishlist-info thead td {
	color: #4D4D4D;
	font-weight: bold;
	background-color: #F7F7F7;
	border-bottom: 1px solid #DDDDDD;
}
.wishlist-info thead .image {
	text-align: center;
}
.wishlist-info thead .name, .wishlist-info thead .model, .wishlist-info thead .stock {
	text-align: left;
}
.wishlist-info thead .quantity, .wishlist-info thead .price, .wishlist-info thead .total, .wishlist-info thead .action {
	text-align: right;
}
.wishlist-info tbody td {
	vertical-align: top;
	border-bottom: 1px solid #DDDDDD;
}
.wishlist-info tbody .image img {
	border: 1px solid #DDDDDD;
}
.wishlist-info tbody .image {
	text-align: center;
}
.wishlist-info tbody .name, .wishlist-info tbody .model, .wishlist-info tbody .stock {
	text-align: left;
}
.wishlist-info tbody .quantity, .wishlist-info tbody .price, .wishlist-info tbody .total, .wishlist-info tbody .action {
	text-align: right;
}
.wishlist-info tbody .price s {
	color: #F00;
}
.wishlist-info tbody .action img {
	cursor: pointer;
}
#content .content {
	padding: 10px;
	overflow: auto;
	margin-bottom: 20px;
	border: 1px solid #EEEEEE;
	width: 60%;
}
h1, .welcome {
	color: #000;
	font: Verdana;
	margin-top: 20px;
	margin-bottom: 20px;
	font-size: 24px;
	font-weight: normal;
	text-shadow: 0 0 1px rgba(0, 0, 0, .01);
}
h2 {
	color: #000000;
	font-size: 16px;
	margin-top: 0px;
	margin-bottom: 5px;
}

html {
	overflow: -moz-scrollbars-vertical;
	margin: 0;
	padding: 0;
}
body {
	background-color: #ffffff;
	color: #000000;
	font-family: Arial, Helvetica, sans-serif;
	margin: 0px;
	padding: 20px;
	width: 600px;
	margin: 0 auto;
}
body, td, th, input, textarea, select, a {
	font-size: 12px;
}
</style>
<script language="Javascript1.2">
  <!--
  function printpage() {
  window.print();
  }
  //-->
</script>
</head>
<body onload="printpage()">
<div><img src="<?php echo $logo; ?>" /></div>
<div id="content">
  <h1><?php echo $heading_title; ?></h1>
  <div><h2><?php echo $name; ?> - <?php echo $www; ?> - <?php echo $telephone; ?><br /><br />
</h2></div>
    <div style="float: left; margin: 10px 0 15px 0;"><a href="JavaScript:window.print();"><?php echo $button_print_page; ?></a></div>
    <div style="float: right; margin: 10px 0 15px 0;"><a href="JavaScript:window.close()">Close this Window</a></div>
	<div style="clear: both;"><div>
  <?php if ($products) { ?>
  <div class="wishlist-info">
    <table>
      <thead>
        <tr>
            <td class="image"><?php echo $column_image; ?></td>
			<td class="left">
			<?php if ($sort == 'title') { ?>
				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
			<?php } ?></td>		  
			
			<td class="left">
			<?php if ($sort == 'model') { ?>
				<a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
			<?php } ?></td>		  

            <td class="image"><?php echo $column_qty_rqd; ?></td>
            <td class="image"><?php echo $column_qty_bought; ?></td>
			
			<td class="total">
			<?php if ($sort == 'price') { ?>
				<a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
			<?php } ?></td>		  
        </tr>
      </thead>
      <tbody>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="image" style="border-bottom: none;"><?php if ($product['thumb']) { ?>
              <a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?></td>
            <td class="name" style="border-bottom: none;"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <?php if (!$product['stock']) { ?>
              <span class="stock">***</span>
              <?php } ?>
              <?php foreach ($product['option'] as $option) { ?>
              <div>
                - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
              </div>
				<?php } ?>
			</td>
            <td class="model" style="border-bottom: none;"><?php echo $product['model']; ?></td>
            <td class="image" style="border-bottom: none;"><input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" disabled="disabled" /></td>
				<td class="image" style="border-bottom: none;"><input type="text" name="bought[<?php echo $product['key']; ?>]" value="<?php echo $product['bought']; ?>" size="1" disabled="disabled" /></td>
            <td class="price" style="border-bottom: none;"><?php echo $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))); ?></td>
		  </tr>
			<tr><td colspan="6">
			<?php if ($product['comment']) { ?>
			<textarea disabled name="comment-<?php echo $product['key']; ?>" "value="<?php echo $product['comment']; ?>" cols="60" rows="1"><?php echo $product['comment']; ?></textarea></td></tr>
			<?php } ?>
          <?php } ?>
      </tbody>
    </table>
  </div>
	<div style="padding-bottom: 20px;"><div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
</body>
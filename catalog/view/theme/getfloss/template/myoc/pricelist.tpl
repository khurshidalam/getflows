<?php echo $header; ?><div id='notification'>
</div>
<div class="infoConAra">
<div class="innerwrapper">
<?php if(isset($error_customer_group)) { ?>
<div class="attention"><?php echo $error_customer_group; ?></div>
<?php } ?>
<div id="content"><?php //echo $content_top; ?>
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <h1 class="prdct_name">
    <?php echo $heading_title; ?>
    <div class="print-control">
        <div class="fright">
            <a href="<?php echo $print; ?>" class="button" target="_blank"><span><?php echo $text_print; ?></span></a>
        </div>
    </div>
    </h1>
    <?php if (!isset($error_customer_group)) { ?>
    
    <div class="filter-control">
        <div class="fleft">
        
          <strong><?php echo $text_category; ?></strong> <select name="category_id" onchange="location = this.value;">
            <?php foreach($categories as $category){ ?>
            <option value="<?php echo $category['href']; ?>"<?php if($catid == $category['category_id']) { ?> selected="selected"<?php } ?>><?php echo $category['name']; ?></option>
            <?php } ?>
          </select>
        </div>
        <div class="fright">
          <strong><?php echo $text_limit; ?></strong> <select name="limit" onchange="location = this.value;">
            <?php foreach($limits as $limit_value) { ?>
            <option value="<?php echo $limit_value['href']; ?>"<?php if($limit == $limit_value['value']) { ?> selected="selected"<?php } ?>><?php echo $limit_value['value']; ?></option>
            <?php } ?>
          </select>
        </div>
    </div>
    <div class="pricelist">
      <table>
        <thead>
          <tr>
            <th class="image"><?php echo $column_image; ?></th>
            <th class="name"><?php if ($sort == 'pd.name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></th>
            <th><?php if ($sort == 'p.model') { ?>
                <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                <?php } ?></th>
            <th><?php if ($sort == 'rating') { ?>
                <a href="<?php echo $sort_rating; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_rating; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_rating; ?>"><?php echo $column_rating; ?></a>
                <?php } ?></th>
            <th><?php if ($sort == 'p.price') { ?>
                <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                <?php } ?></th>
            <th><?php if ($sort == 'p.quantity') { ?>
                <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_stock; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_quantity; ?>"><?php echo $column_stock; ?></a>
                <?php } ?></th>
            <th><?php echo $column_qty; ?></th>
            <th class="action"><?php echo $column_action; ?></th>
          </tr>
        </thead>
     <tbody>
          <?php if(!empty($products)) { ?>
          <?php foreach($products as $product_id => $product) { ?>
            <tr>
                <td class="tcenter image"><?php if($product['popup']) { ?><a href="<?php echo $product['popup']; ?>" class="fancybox colorbox" rel="fancybox<?php echo $product_id; ?>"><?php } ?><img src="<?php echo $product['image']; ?>" alt="no_image" title="<?php echo $product['name']; ?>" /><?php if($product['popup']) { ?></a><?php } ?>
                    <?php if($product['gallery']) { ?>
                    <?php foreach ($product['gallery'] as $gallery) { ?>
                    <div style="display: none;"><a class="fancybox colorbox" href="<?php echo $gallery; ?>" rel="fancybox<?php echo $product_id; ?>"><img src="<?php echo $gallery; ?>" alt="" /></a></div>
                    <?php } ?>
                    <?php } ?>
                </td>
                <td><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a><br /><?php echo $product['description']; ?></td>
                <td class="nowrap"><?php echo $product['model']; ?>
                    <?php if($product['sku']) { ?><br /><strong><?php echo $text_sku; ?></strong> <?php echo $product['sku']; } ?>
                    <?php if($product['upc']) { ?><br /><strong><?php echo $text_upc; ?></strong> <?php echo $product['upc']; } ?>
                    <?php if($product['ean']) { ?><br /><strong><?php echo $text_ean; ?></strong> <?php echo $product['ean']; } ?>
                    <?php if($product['jan']) { ?><br /><strong><?php echo $text_jan; ?></strong> <?php echo $product['jan']; } ?>
                    <?php if($product['isbn']) { ?><br /><strong><?php echo $text_isbn; ?></strong> <?php echo $product['isbn']; } ?>
                    <?php if($product['mpn']) { ?><br /><strong><?php echo $text_mpn; ?></strong> <?php echo $product['mpn']; } ?></td>
                <td><img src="catalog/view/theme/default/image/stars-<?php echo $product['rating']; ?>.png" alt="<?php echo $product['rating']; ?> *" /></td>
                <td class="tright nowrap"><?php if($product['price']) { if(!$product['special']) { ?><span class="price"><?php echo $product['price']; ?></span><?php } else { ?><span class="price-old"><?php echo $product['price']; ?></span><br /><span class="price-new"><?php echo $product['special']; ?></span><?php } ?>
                    <?php if ($product['discounts'] && !$product['special']) { ?>
                    <br />
                    <div class="discount">
                      <?php foreach ($product['discounts'] as $discount) { ?>
                      <?php echo sprintf($text_discount, $discount['quantity'], $discount['price']); ?><br /><br />
                      <?php } ?>
                    </div>
                    <?php } ?>
                    <?php } else { ?>-<?php } ?>
                </td>
                <td class="tright"><span class="<?php if(!$product['quantity']) { ?>nostock<?php } ?>"><?php echo $product['quantity']; ?></span></td>
                <td class="tright"><input type="text" id="qty-<?php echo $product_id; ?>" value="<?php echo $product['minimum']; ?>" size="2" /></td>
                <td class="tcenter"><input type="button" value="<?php echo $button_cart; ?>" id="addtocart-<?php echo $product_id; ?>" class="addtocart-button button" /></td>
            </tr>
          <?php } ?>
          <?php } else { ?>
            <tr><td colspan="8" class="tcenter"><?php echo $text_empty; ?></td></tr>
          <?php } ?>
        </tbody>
      </table>
      
    </div>
    
    
    
    
    

    
    
    <div class="pagination"><?php echo $pagination; ?></div>
    <script type="text/javascript"><!--
    $(".addtocart-button").click(function () {
        var product_id = $(this).attr("id").substring($(this).attr("id").indexOf("-")+1);
        var quantity = $("#qty-" + product_id).val();
        //alert(product_id + ', ' + quantity);
        add_to_cart(product_id, quantity);

        return false;
    });

    if(typeof $.fancybox == 'function') {
        $('.fancybox').fancybox({cyclic: true});
    }
    if(typeof $.colorbox == 'function') {
        $('.colorbox').colorbox({
            overlayClose: true,
            opacity: 0.5
        });
    }

    function add_to_cart(product_id, quantity) {
        quantity = typeof(quantity) != 'undefined' ? quantity : 1;

        var cart_path = 'index.php?route=checkout/cart/add'; //OC v1.5.2.x
        if(!check_path(cart_path)) {
            cart_path = 'index.php?route=checkout/cart/update'; //OC v1.5.1.x
        }

        $.ajax({
            url: cart_path,
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + quantity,
            dataType: 'json',
            success: function(json) {
                $('.success, .warning, .attention, .information, .error').remove();

                if (json['error']) {
                    if (json['error']['warning']) {
                        $('#notification').html('<div class="warning" style="display: none;">' + json['error']['warning'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                        
                        $('.warning').fadeIn('slow');

                        $('html, body').animate({ scrollTop: 0 }, 'slow'); 
                    } else if (json['redirect']) {
                        location = json['redirect'];
                    }    
                }
                
                if (json['success']) {
                    $('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
                    
                    $('.success').fadeIn('slow');
                    
                    $('#cart-total, #cart_total').html(json['total']);

                    $('html, body').animate({ scrollTop: 0 }, 'slow'); 
                }   
            }
        });
    }

    function check_path(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        return http.status!=404;
    }
    //--></script>
    <?php } else { ?>
        <div class="content"><?php echo $text_empty; ?></div>
        <div class="buttons">
            <div class="right"><a href="<?php echo $continue; ?>" class="button"><span><?php echo $button_continue; ?></span></a></div>
        </div>
    <?php } ?>
    <?php echo $content_bottom; ?>
</div>

</div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/default/stylesheet/events.css" />
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
  <?php if (!$private) { ?>
  <div>
	<?php echo $start_date; ?> - <?php echo $end_date; ?><br /><br />
	<?php echo $text_event_status; ?>&nbsp;&nbsp;<?php echo $event_status; ?><br/><?php echo $text_event_owner; ?>&nbsp;&nbsp;<?php echo $event_owner; ?><br /><br />
  <?php if ($web_link) { ?>
	<a href="<?php echo $web_link; ?>" target="_blank" rel="nofollow"><?php echo $text_web_link_anchor; ?></a><br /><br />
  <?php } ?>
  </div>
  <?php if ($event_message) { ?>
  <div style="text-align: center;"><h2><?php echo $text_message; ?></h2></div>
  <div style="text-align: center;"><?php echo $event_message; ?><br /><br /></div>
  <?php } ?>
  <?php if ($photo) { ?>
  <div style="margin-left:auto; margin-right:auto; max-width: 400px"><img src="<?php echo $photo; ?>" style="max-width:100%;" alt="Event Photo" title="Event Photo" /><br /><br /></div>
  <?php } ?>
  <?php } ?>
  <div class="buttons">
    <div class="left"><a href="<?php echo $create; ?>" class="button"><?php echo $button_create_event; ?></a> <a href="<?php echo $print_button; ?>"  class="button" style="margin-right: 10px" target="_blank"><span><?php echo $button_print; ?></span></a></div>
  </div>
  <?php if ($private) { ?>
  <?php echo $text_private_info; ?>
				<h2 style="margin-top: 20px; margin-bottom: 5px;"><?php echo $text_send_email; ?></h1>
			<form name="sendPrivEmail" id="sendPrivEmail"   >
			  <table border="0" cellpadding="2" cellspacing="2">
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_from_name; ?><br /><input type="text" value="" name="from_name" id="from_name"></td>
				</tr>
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_from_email; ?><br /><input type="text" value="" name="from_email" id="from_email"></td>
				</tr>
   <tr>
     <td align="center" id="send_result"></td>
   </tr>
<input type="hidden" value="<?php echo $eid; ?>" name="eid" id="eid"><input type="hidden" value="<?php echo $akey; ?>" name="akey" id="akey">
<tr>
    <td><b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=information/contact/captcha" alt="" />
	</td>
</tr>
</table></form>    <br />
	<div class="buttons"><div class="right"><a onclick="location = '<?php echo $continue; ?>';" class="button"><?php echo $button_continue; ?></a></div>
	<div class="left" id="send_mail_button" ><a class="button" onclick="send_email()"><span><?php echo $button_send_email; ?></span></a></div>
	</div>
  <?php } else { ?>
  
  <div id="tabs" class="htabs"><a href="#tab-products"><?php echo $tab_products; ?></a>
  <?php if ($note_status) { ?>
    <a href="#tab-notes"><?php echo $tab_notes; ?></a>
  <?php } ?>
  <?php if ($rsvp_status) { ?>
	<a href="#tab-rsvp"><?php echo $tab_rsvp; ?></a>
  <?php } ?>
  </div>
  <div id="tab-products" class="tab-content">
    <div class="cart-info">
      <table>
        <thead>
          <tr>
            <td class="image"><?php echo $column_image; ?></td>
			<td class="left">
			<?php if ($sort == 'name') { ?>
				<a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
			<?php } ?></td>		  
			
			<td class="left">
			<?php if ($sort == 'model') { ?>
				<a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
			<?php } ?></td>		  

            <td class="image"><?php echo $column_qty_rqd; ?></td>
            <td class="image"><?php echo $column_qty_bought; ?></td>
            <td class="image"><?php echo $column_buy_qty; ?></td>
			
			<td class="total">
			<?php if ($sort == 'price') { ?>
				<a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
			<?php } else { ?>
				<a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
			<?php } ?></td>		  
			
            <td class="total"><?php echo $column_action; ?></td>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="image" style="border-bottom: none;"><?php if ($product['thumb']) { ?>
              <a class="colorbox" href="<?php echo $product['popup']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
              <?php } ?></td>
            <td class="name" style="border-bottom: none;"><a class="quickview" href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
              <?php if (!$product['stock']) { ?>
              <span class="stock">***</span>
              <?php } ?>
              <?php foreach ($product['option'] as $option) { ?>
              <div>
                - <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
              </div>
				<?php } ?>
			</td>
            <td class="model" style="border-bottom: none;"><?php echo $product['model']; ?></td>
            <td class="image" style="border-bottom: none;"><input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" disabled="disabled" /></td>
				<td class="image" style="border-bottom: none;"><input type="text" name="bought[<?php echo $product['key']; ?>]" value="<?php echo $product['bought']; ?>" size="1" disabled="disabled" /></td>
            <?php if (($product['fred'] > 0) && ($product['finished'])) { ?>
				<td class="image" style="border-bottom: none;"><select id="buyqty<?php echo $product['key']; ?>" name="buyqty<?php echo $product['key']; ?>">
            <?php for  ($i = 1 ; $i <= $product['fred']; $i++) { ?>
				<option value="<?php echo $i; ?>"><?php echo $i; ?></option>
            <?php } ?>
			</select></td>
            <?php } else { ?>
				<td class="image" style="border-bottom: none;"><input type="text" id="buyqty<?php echo $product['key']; ?>" name="buyqty<?php echo $product['key']; ?>" value="<?php echo $product['fred']; ?>" size="1" disabled="disabled" /></td>
            <?php } ?>
            <td class="price" style="border-bottom: none;"><?php echo $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))); ?></td>
			<td class="price" style="border-bottom: none;">
            <?php if (($product['fred'] > 0) && ($product['finished'])) { ?>
			<input type="button" value="<?php echo $button_add_cart; ?>" onclick="addToCartEvent('<?php echo $product['id']; ?>','<?php echo $eid; ?>','<?php echo $product['iid']; ?>','buyqty<?php echo $product['key']; ?>');" class="button" />
            <?php } ?>
			</td>
		  </tr>
			<tr><td colspan="8"><textarea disabled name="comment-<?php echo $product['key']; ?>" "value="<?php echo $product['comment']; ?>" cols="60" rows="1"><?php echo $product['comment']; ?></textarea></td></tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    </div>
  <?php if ($note_status) { ?>
	<div id="tab-notes" class="tab-content">
    <div id="note"></div>
    <h2 id="note-title"><?php echo $text_write; ?></h2>
    <b><?php echo $entry_name; ?></b><br />
    <input type="text" name="name" value="" />
    <br />
    <br />
    <b><?php echo $entry_note; ?></b>
    <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span><br />
    <br />
    <b><?php echo $entry_captcha; ?></b><br />
    <input type="text" name="captcha" value="" />
    <br />
    <img src="index.php?route=events/events_view/captcha" alt="" id="captcha" /><br />
    <br />
    <div class="buttons">
      <div class="right"><a id="button-note" class="button"><?php echo $button_continue; ?></a></div>
    </div>
  </div>
  <?php } ?>
  <?php if ($rsvp_status) { ?>
  <div id="tab-rsvp" class="tab-content">
  <div id="rsvpform">
    <h2 id="rsvp-title"><?php echo $text_rsvp; ?></h2>
	  <form name="sendRSVP" id="sendRSVP"   >
		<table>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_name; ?></td>
			<td><input type="text" value="" name="rsvp_name" id="rsvp_name" /><br />
			  <span id="error_rsvp_name"></span>
			</td>
		  </tr>
		  <tr>
			<td><span class="required">*</span>&nbsp;<?php echo $entry_email; ?></td>
			<td><input type="text" value="" name="rsvp_email" id="rsvp_email" /><br />
			  <span id="error_rsvp_email"></span>
			</td>
		  </tr>
		  <tr>
			<td><?php echo $entry_short_note; ?></td>
			<td><textarea rows="4" cols="50" name="short_note" placeholder="<?php echo $text_rsvp_placeholder; ?>"></textarea></td>
		  </tr>
		  <tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_1; ?></td>
			<td><input type="text" value="" name="entry_guest_name_1" id="entry_guest_name_1" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_2; ?></td>
			<td><input type="text" value="" name="entry_guest_name_2" id="entry_guest_name_2" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_3; ?></td>
			<td><input type="text" value="" name="entry_guest_name_3" id="entry_guest_name_3" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_4; ?></td>
			<td><input type="text" value="" name="entry_guest_name_4" id="entry_guest_name_4" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_5; ?></td>
			<td><input type="text" value="" name="entry_guest_name_5" id="entry_guest_name_5" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_6; ?></td>
			<td><input type="text" value="" name="entry_guest_name_6" id="entry_guest_name_6" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_7; ?></td>
			<td><input type="text" value="" name="entry_guest_name_7" id="entry_guest_name_7" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_8; ?></td>
			<td><input type="text" value="" name="entry_guest_name_8" id="entry_guest_name_8" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_9; ?></td>
			<td><input type="text" value="" name="entry_guest_name_9" id="entry_guest_name_9" /><br /></td>
		  </tr>
		  <tr>
			<td><?php echo $entry_guest_name_10; ?></td>
			<td><input type="text" value="" name="entry_guest_name_10" id="entry_guest_name_10" /><br /></td>
		  </tr>
			<input type="hidden" value="<?php echo $eid; ?>" name="eid" id="eid"><input type="hidden" value="<?php echo $akey; ?>" name="akey" id="akey">
		  <tr>
			<td><b><?php echo $entry_captcha; ?></b><br />
			<input type="text" name="captcha" value="" />
			<br />
			<img src="index.php?route=information/contact/captcha" alt="" />
			  <span id="error_captcha"></span>
			</td>
		  </tr>
		     <tr>
   </tr>

		</table>
	  </form>
	  </div>
	  <div id="send_result"></div>
      <div class="buttons" id="send_rsvp">
		<div class="left"><a class="button" onclick="send_rsvp()"><span><?php echo $button_send_rsvp; ?></span></a></div>
	  </div>
	</div>
	<?php } ?>
	<div class="buttons">
	<div class="left"><a onclick="history.go(-1);return true;" class="button"><?php echo $button_back; ?></a></div>
	<div class="right"><a onclick="location = '<?php echo $continue; ?>';" class="button"><?php echo $button_continue; ?></a></div>
	</div>
<?php } ?>
	<?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function addToCartEvent(product_id,eid,iid,bqty) {
	$.ajax({
		url: 'index.php?route=events/events_view/add',
		type: 'post',
		data: 'product_id=' + product_id + '&eid=' + eid + '&iid=' + iid + '&quantity=' + document.getElementById(bqty).value,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
			if (json['success']) {
				$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
				
				$('.success').fadeIn('slow');
				
				$('#cart-total').html(json['total']);
				
				$('html, body').animate({ scrollTop: 0 }, 'slow'); 
			}	
		}
	});
}
$('#tabs a').tabs(); 

$('#note .pagination a').live('click', function() {
	$('#note').fadeOut('slow');
		
	$('#note').load(this.href);
	
	$('#note').fadeIn('slow');
	
	return false;
});			

$('#note').load('index.php?route=events/events_view/notes&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>');

$('#button-note').bind('click', function() {
	$.ajax({
		url: 'index.php?route=events/events_view/write_note&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-note').attr('disabled', true);
			$('#note-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-note').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#note-title').after('<div class="warning">' + data['error'] + '</div>');
			}
			
			if (data['success']) {
				$('#note-title').after('<div class="success">' + data['success'] + '</div>');
				
				$('#note').load('index.php?route=events/events_view/notes&event_id=<?php echo $event_id; ?>&akey=<?php echo $akey; ?>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
$(document).ready(function() {
	$('.colorbox').colorbox({
		overlayClose: true,
		opacity: 0.5,
		rel: "colorbox"
	});
});

//--></script> 
<script type="text/javascript">
function send_email(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=events/events_view/sendPrivateEmail',
			dataType: 'html',
            data:$("#sendPrivEmail").serialize(),
			success: function (html) {
				eval(html);
			}}); 
}
function send_rsvp(){
	$('#error_rsvp_email').html("");
	$('#error_rsvp_name').html("");
	$('#error_captcha').html("");

	$.ajax({
			type: 'post',
			url: 'index.php?route=events/events_view/sendRSVP',
			dataType: 'html',
            data:$("#sendRSVP").serialize(),
			success: function (html) {
				eval(html);

				}}); 
}
    $(".quickview").colorbox({
			'fixed'				: true,
        	'autoDimensions'	: false,
			'width'         	: '80%',
			'height'			: '80%',
			'transitionIn'		: 'elastic',
			'transitionOut'		: 'none'
		});	
</script>
<?php echo $footer; ?>
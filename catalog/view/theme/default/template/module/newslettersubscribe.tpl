<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
      <div id="frm_subscribe">
          <form name="subscribe" id="subscribe">
              <table border="0" cellpadding="2" cellspacing="2">
                   <tr>
                     <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_email; ?><br /><input type="text" value="" name="subscribe_email" id="subscribe_email"></td>
                   </tr>
                   <tr <?php if(!$thickbox) { ?> style="display:none" <?php } ?>>
                     <td align="left"><?php echo $entry_name; ?>&nbsp;<br /><input type="text" value="" name="subscribe_name" id="subscribe_name"> </td>
                   </tr>
                   <tr>
                     <td align="left">
                        <a class="button" onclick="email_subscribe()"><span><?php echo $entry_button; ?></span></a>
                        <?php if($option_unsubscribe) { ?>
                        <a class="button" onclick="email_unsubscribe()"><span><?php echo $entry_unbutton; ?></span></a>
                        <?php } ?>    
                     </td>
                   </tr>
                   
              </table>
          </form>
      </div><!-- /#frm_subscribe -->
	</div><!-- /.box-content -->
  <div class="bottom">&nbsp;</div>
<script language="javascript">
function email_subscribe(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=module/newslettersubscribe/subscribe',
			dataType: 'html',
            data:$("#subscribe").serialize(),
			success: function (html) {
				eval(html);
			}}); 
	
	$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow'); 
	
}
function email_unsubscribe(){
	$.ajax({
			type: 'post',
			url: 'index.php?route=module/newslettersubscribe/unsubscribe',
			dataType: 'html',
            data:$("#subscribe").serialize(),
			success: function (html) {
				eval(html);
			}}); 
	
	$('html, body').delay( 1500 ).animate({ scrollTop: 0 }, 'slow'); 
	
}
</script>
</div><!-- /.box -->

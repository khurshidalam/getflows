<?php echo $header; ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?><img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?></h1>
	<div class="buttons">
	<div class="left">
		<a href="<?php echo $button_new; ?>"  class="button" style="margin-right: 10px"><span><?php echo $button_add_new; ?></span></a>
	</div>
	</div>
  <?php if ($events) { ?>
    <?php echo $text_adding_products; ?>
  <div class="wishlist-info">
    <table>
      <thead>
        <tr>
          <td class="title">&nbsp;</td>
          <td class="title"><?php echo $column_title; ?></td>
          <td class="start_date"><?php echo $column_start_date; ?></td>
          <td class="end_date"><?php echo $column_end_date; ?></td>
          <td class="status"><?php echo $column_status; ?></td>
          <td class="type"><?php echo $column_type; ?></td>
          <td class="action"><?php echo $column_action; ?></td>
        </tr>
      </thead>
      <?php foreach ($events as $event) { ?>
      <tbody id="wishlist-row<?php echo $event['event_id']; ?>">
        <tr>
          <td class="remove"><a class="delete" href="<?php echo $event['delete']; ?>"><img alt="Delete" title="Delete" src="catalog/view/theme/default/image/remove.png" /></a></td>
          <td class="name"><?php echo $event['title']; ?></td>
          <td class="name"><?php echo $event['start_date']; ?></td>
          <td class="name"><?php echo $event['end_date']; ?></td>
          <td class="name"><?php echo ($event['status'])  ?></td>
          <td class="name"><?php echo ($event['type'])  ?></td>
          <td class="action"><a href="<?php echo $event['href']; ?>"><?php echo $event['action']; ?></a></td>
        </tr>
      </tbody>
      <?php } ?>
    </table>
  </div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } else { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript">
$("#frm_sendWL" ).hide();
$('#showForm').click(function() {
  $('#frm_sendWL').slideToggle('slow', function() {
  });
});
  $(document).ready(function() {
    $("#dialog").dialog({
      autoOpen: false,
      modal: true
    });
  });

  $(".delete").click(function(e) {
    e.preventDefault();
    var targetUrl = $(this).attr("href");

    $("#dialog").dialog({
      buttons : {
        "Confirm" : function() {
          window.location.href = targetUrl;
        },
        "Cancel" : function() {
          $(this).dialog("close");
        }
      }
    });

    $("#dialog").dialog("open");
  });
</script>
<div id="dialog" title="Confirmation Required">
  Are you sure you want to delete this event?
</div>
<?php echo $footer; ?>

<?php if ($notes) { ?>
<?php foreach ($notes as $note) { ?>
<div class="review-list">
  <div class="author"><a id="remove-note"><img title="Remove" alt="Remove" src="catalog/view/theme/default/image/remove.png"></a>&nbsp;&nbsp;<b><?php echo $note['author']; ?></b> <?php echo $text_on; ?> <?php echo $note['date_added']; ?>
</div>
  <div class="text"><?php echo $note['text']; ?></div>
<form id="delete_note">
<input type="hidden" value="<?php echo $note['event_id']; ?>" name="event_id" id="event_id">
<input type="hidden" value="<?php echo $note['akey']; ?>" name="akey" id="akey">
<input type="hidden" value="<?php echo $note['note_id']; ?>" name="note_id" id="note_id">
</form>
</div>
<?php } ?>
<div class="pagination"><?php echo $pagination; ?></div>
<?php } else { ?>
<div class="content"><?php echo $text_no_notes; ?></div>
<?php } ?>
<?php if (!empty($note)) { ?>
<script type="text/javascript"><!--
$('#remove-note').bind('click', function() {
if (confirm("Are you sure you want to delete this note?")) {
	$.ajax({
		url: 'index.php?route=account/events/remove_note&event_id=<?php echo $note['event_id']; ?>&akey=<?php echo $note['akey']; ?>',
		type: 'post',
		dataType: 'json',
		data: 'note_id=' + encodeURIComponent($('input[name=\'note_id\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#remove-note').attr('disabled', true);
		},
		complete: function() {
			$('#remove-note').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			
			if (data['success']) {
				
				$('#note').load('index.php?route=account/events/notes&event_id=<?php echo $note['event_id']; ?>&akey=<?php echo $note['akey']; ?>');
								
			}
		}
	});
	}
});
//--></script>
<?php } ?>
<?php echo $header; ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_title; ?><?php echo $title; ?></h1>
  <div id="tabs" class="htabs" style="width: 60%;"><a href="#tab-general"><?php echo $tab_general; ?></a>
  </div>
  
    <div style="float: right; text-align: left; width: 37%; margin-left: 23px;">
	<h2><?php echo $text_what_is_heading; ?><br /></h2>
	<?php echo $text_what_is_help; ?>
	</div>
  <div id="tab-general" class="tab-content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			  <table border="0" cellpadding="2" cellspacing="2">
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_title; ?><br />
			    <?php if ($error_title) { ?>
				  <span class="error"><?php echo $error_title; ?></span>
				<?php } ?>
				  <input type="text" value="<?php echo $title; ?>" name="title" id="title" /></td>
				</tr>
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_name; ?><br />
			    <?php if ($error_name) { ?>
				  <span class="error"><?php echo $error_name; ?></span>
				<?php } ?>
				  <input type="text" value="<?php echo $name; ?>" name="name" id="name" /></td>
				</tr>
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_email; ?><br />
			    <?php if ($error_email) { ?>
				  <span class="error"><?php echo $error_email; ?></span>
				<?php } ?>
				  <input type="text" value="<?php echo $email; ?>" name="email" id="email" />
				  &nbsp;(<?php echo $text_not_published; ?>)</td>
				</tr>
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_start_date; ?><br />
			    <?php if ($error_start_date) { ?>
				  <span class="error"><?php echo $error_start_date; ?></span>
				<?php } ?>
				  <input type="text" value="<?php echo $start_date; ?>" name="start_date" id="start_date" /></td>
				</tr>
				<tr>
				  <td align="left"><span class="required">*</span>&nbsp;<?php echo $entry_end_date; ?><br />
			    <?php if ($error_end_date) { ?>
				  <span class="error"><?php echo $error_end_date; ?></span>
				<?php } ?>
				  <input type="text" value="<?php echo $end_date; ?>" name="end_date" id="end_date" /></td>
				</tr>
				<tr>
				<td><?php echo $entry_type; ?><br />
				<select name="type">
                  <?php if ($type) { ?>
                  <option value="0" selected="selected"><?php echo $text_public; ?></option>
                  <option value="1"><?php echo $text_private; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_public; ?></option>
                  <option value="1" selected="selected"><?php echo $text_private; ?></option>
                  <?php } ?>
                </select></td>
				</tr>
				<tr>
				<td><?php echo $entry_status; ?><br />
				<select name="status">
                  <?php if ($status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
				</tr>
				<td><?php echo $entry_message; ?><br />
				<textarea rows="4" cols="50" name="message" placeholder="<?php echo $text_placeholder; ?>"><?php echo $message; ?></textarea></td>
				</tr>
				<tr>
				  <td align="left"><br /><h2><?php echo $text_invitees; ?></h2></td>
				</tr>
				</table>
        <table id="attendee" class="list">
          <thead>
            <tr>
              <td class="left"><?php echo $entry_att_name; ?></td>
              <td class="right"><?php echo $entry_att_email; ?></td>
              <td></td>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <td colspan="2"></td>
              <td class="left"><a onclick="addAttendee();" class="button"><?php echo $button_add; ?></a></td>
            </tr>
          </tfoot>
          <?php $attendee_row = 0; ?>
  <?php if ($attendees) { ?>
          <?php foreach ($attendees as $attendee) { ?>
          <tbody id="attendee-row<?php echo $attendee_row; ?>">
            <tr>
              <td class="left"><input type="text" name="attendee[<?php echo $attendee_row; ?>][name]" value="<?php echo $attendee['name']; ?>" size="18" /></td>
              <td class="right"><input type="text" name="attendee[<?php echo $attendee_row; ?>][email]" value="<?php echo $attendee['email']; ?>" size="18" /></td>
              <td class="left"><a onclick="$('#attendee-row<?php echo $attendee_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
		   </tr>
          </tbody>
          <?php $attendee_row++; ?>
		      <?php } ?>
		      <?php } ?>
		 </table>	
</form>
</div>	
      <div class="buttons"><div class="left"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a></div><div class="right"><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div></div>

  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
var attendee_row = <?php echo $attendee_row; ?>;
function addAttendee() {	
	html  = '<tbody id="attendee-row' + attendee_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="attendee[' + attendee_row + '][name]" value="" size="18" /></td>';
	html += '    <td class="right"><input type="text" name="attendee[' + attendee_row + '][email]" value="" size="18" /></td>';
	html += '    <td class="left"><a onclick="$(\'#attendee-row' + attendee_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#attendee tfoot').before(html);
	
	attendee_row++;
}
//--></script> 
<script type="text/javascript"><!--
$("#start_date").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true,
    minDate: new Date(),
    maxDate: '+2y',
    onSelect: function(date){

        var selectedDate = new Date(date);
        var msecsInADay = 86400000;
        var endDate = new Date(selectedDate.getTime() + msecsInADay);

        $("#end_date").datepicker( "option", "minDate", endDate );
        $("#end_date").datepicker( "option", "maxDate", '+2y' );

    }
});

$("#end_date").datepicker({ 
    dateFormat: 'yy-mm-dd',
    changeMonth: true
});
$('#tabs a').tabs(); 
//--></script>
<script type="text/javascript">
function quickview() {
    $.colorbox
    ({
        	'autoDimensions'	: false,
			'width'         	: '80%',
			'height'			: '80%',
			'transitionIn'		: 'none',
			'transitionOut'		: 'none',
			'iframe':true,
			onClosed:function() { location.reload(true); }
		});
}		
    </script><?php echo $footer; ?>
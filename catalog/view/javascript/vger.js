function checkOptions(product_id){
	quantity = $('input[name=quantity]').val();
	quantity = parseInt(quantity);
	if (!quantity) quantity = 1;
		$.ajax({
			url: 'index.php?route=account/events/checkOptions',
			type: 'post',
			data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, information, .error').remove();
				if (json['error']) {
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
						}
					}
				}

				if (json['success']) {
					$.colorbox({
						href : "index.php?route=account/events/eventChooser&product_id="+json['success'],
						width : '60%',
						height : '60%',
						opacity : '0.8',
						overlayClose : false
					});
				}
			}
		});
}


function addToEvent(product_id, event_id){
	quantity = $('input[name=quantity]').val();
	quantity = parseInt(quantity);
	if (!quantity) quantity = 1;
		$.ajax({
			url: 'index.php?route=account/events/eventChooserAddProduct&event_id='+event_id,
			type: 'post',
			data: $('.product-info input[type=\'text\'], .product-info input[type=\'hidden\'], .product-info input[type=\'radio\']:checked, .product-info input[type=\'checkbox\']:checked, .product-info select, .product-info textarea'),
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, information, .error').remove();
				if (json['error']) {
					if (json['error']['option']) {
						for (i in json['error']['option']) {
							$('#option-' + i).after('<span class="error">' + json['error']['option'][i] + '</span>');
						}
					}
				}

				if (json['success']) {
					$.colorbox.close();
					$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.success').fadeIn('slow');
					$('#cart-total').html(json['total']);
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}
		});
}

function checkOptionsCP(product_id, quantity) {
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;

	$.ajax({
		url: 'index.php?route=account/events/checkOptions',
		type: 'post',
		data: 'product_id=' + product_id + '&quantity=' + quantity,
		dataType: 'json',
		success: function(json) {
			$('.success, .warning, .attention, .information, .error').remove();
			
			if (json['redirect']) {
				location = json['redirect'];
			}
			
				if (json['success']) {
					$.colorbox({
						href : "index.php?route=account/events/eventChooserCP&product_id="+json['success'],
						width : '60%',
						height : '60%',
						opacity : '0.8',
						overlayClose : false
					});
				}
			}
		});
}

function addToEventCP(product_id, event_id){
	quantity = typeof(quantity) != 'undefined' ? quantity : 1;
		$.ajax({
			url: 'index.php?route=account/events/eventChooserAddProduct&event_id='+event_id,
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + quantity,
			dataType: 'json',
			success: function(json) {
				$('.success, .warning, .attention, .information, .error').remove();
			
				if (json['redirect']) {
					location = json['redirect'];
				}

				if (json['success']) {
					$.colorbox.close();
					$('#notification').html('<div class="success" style="display: none;">' + json['success'] + '<img src="catalog/view/theme/default/image/close.png" alt="" class="close" /></div>');
					$('.success').fadeIn('slow');
					$('#cart-total').html(json['total']);
					$('html, body').animate({ scrollTop: 0 }, 'slow');
				}
			}
		});
}
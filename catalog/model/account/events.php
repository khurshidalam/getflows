<?php
class ModelAccountEvents extends Model {
	public function addEvent($data, $accesskey) {
		
     	$this->db->query("INSERT INTO " . DB_PREFIX . "events SET customer_id = '" . (int)$this->customer->getId() . "', name = '" . $this->db->escape($data['name']) . "', email = '" . $this->db->escape($data['email']) . "', title = '" . $this->db->escape($data['title']) . "', type = '" . $this->db->escape($data['type']) . "', status = '" . $this->db->escape($data['status']) . "', start_date = '" . $this->db->escape($data['start_date']) . "', end_date = '" . $this->db->escape($data['end_date']) . "', e_comment = '" . $this->db->escape($data['message']) . "', access_key = '" . $this->db->escape($accesskey) . "'");
		$event_id = $this->db->getLastId();

		if (isset($data['attendee'])) {
		  foreach ($data['attendee'] as $a) {
			if (empty($a['email']) || empty($a['name'])) {
				continue;
			}
     		$this->db->query("INSERT IGNORE INTO " . DB_PREFIX . "events_invitees SET event_id = '" . (int)$event_id . "', name = '" . $this->db->escape($a['name']) . "', email = '" . $this->db->escape($a['email']) . "'");
		  }
		}
	}
	
	public function getEventDetails($event_id, $akey) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		return $query -> row;
	}
	
	public function getEventDetail($event_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");

		return $query -> row;
	}
	
	public function getEventInvitees($event_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_invitees WHERE event_id = '" . (int)$event_id . "'");

		return $query -> rows;
	}

	public function getAccessKey($event_id) {
		$query = $this->db->query("SELECT DISTINCT access_key FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		if ($query->num_rows) {
			foreach ($query->row as $v) {
				$return = $v;
			}
			
			return $return;
		}
	}
	
	public function getEvents($cid) {
     	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events WHERE customer_id = '" . (int)$cid . "'");
		
		return $query -> rows;
	}
	
	public function getEventsList($data) {
     	$sql = "SELECT * FROM " . DB_PREFIX . "events WHERE status = 1";
		
			$sort_data = array(
				'title',
				'start_date',
				'end_date',
				'type'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY end_date";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
		
		
		if (isset($data['start'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				
			if ($data['limit'] < 1) {
				$data['limit'] = 10;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);
		
		return $query -> rows;
	}
	
	public function getEventsEndDate($eid) {
     	$query = $this->db->query("SELECT DISTINCT end_date FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$eid . "'");
		
		return $query -> row;
	}
		
	public function deleteEvents($event_id, $akey) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_invitees WHERE event_id = '" . (int)$event_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "'");
	}
	
	public function checkEvents($event_id) {
     	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");

		return $query -> rows;
	}
	
	public function checkDelete($event_id, $akey) {
     	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		return $query -> rows;
	}
	
	public function getEventsTitle($event_id) {
     	$query = $this->db->query("SELECT DISTINCT title FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		return $query -> row;
	}
	
	public function getEventsName($event_id) {
     	$query = $this->db->query("SELECT DISTINCT name FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		return $query->row;
	}
	
	public function getNotifyMessage($event_id) {
     	$query = $this->db->query("SELECT DISTINCT notify_message FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		return $query->row;
	}

	public function getEmailAddress($event_id, $akey) {
     	$query = $this->db->query("SELECT DISTINCT email FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		return $query->row;
	}
	
	public function getPrivateEvents($event_id) {
     	$query = $this->db->query("SELECT type FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		$private = false;
		
		if ($query->num_rows) {
			if ($query->row['type']) {
				$private = true;
			}
		}
		return $private;
	}
	
	public function getEnabledEvents($event_id) {
     	$query = $this->db->query("SELECT status FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "'");
		
		$enabled = false;
		
		if ($query->num_rows) {
			if ($query->row['status']) {
				$enabled = true;
			}
		}
		return $enabled;
	}
	
	public function getEventsProducts($event_id) {
     	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "'");

		return $query->rows;
	}
	
	public function getEventComment($event_id, $key) {
     	$query = $this->db->query("SELECT DISTINCT p_comment FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "' AND product = '" . $key . "'");

		return $query->row;
	}
	
	public function getEventsViewProducts($event_id) {
     	$query = $this->db->query("SELECT product, quantity FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "'");
		if($query->num_rows) {
			foreach ($query->rows as $key => $value) {
				$results[$value['product']] = $value['quantity'];
			}
		return $results;
		}
	}
	
	public function getProductOption($event_id, $iid) {
     	$query = $this->db->query("SELECT product FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "' AND  item_id = '" . (int)$iid . "'");

		return $query->row;
	}
	
	
	public function createEventsSessions($eid) {
	   	$query = $this->db->query("SELECT product, quantity FROM " . DB_PREFIX . "events_products WHERE customer_id = '" . (int)$this->customer->getId() . "' AND event_id = '" . (int)$eid . "'");

		if ($query->num_rows > 0) {
			foreach ($query->rows as $key => $value) {
				$results[$value['product']] = $value['quantity'];
			}
   	   	  $this->session->data['event' . $eid] = $results;
		}
	}
	
	public function editEvent($data, $event_id) {
     	$this->db->query("UPDATE " . DB_PREFIX . "events SET customer_id = '" . (int)$this->customer->getId() . "', name = '" . $this->db->escape($data['name']) . "', email = '" . $this->db->escape($data['email']) . "', title = '" . $this->db->escape($data['title']) . "', type = '" . $this->db->escape($data['type']) . "', status = '" . $this->db->escape($data['status']) . "', enable_rsvp = '" . $this->db->escape($data['enable_rsvp']) . "', photo = '" . $this->db->escape($data['photo']) . "', web_link = '" . $this->db->escape($data['web_link']) . "', enable_notes = '" . $this->db->escape($data['enable_notes']) . "', start_date = '" . $this->db->escape($data['start_date']) . "', end_date = '" . $this->db->escape($data['end_date']) . "', e_comment = '" . $this->db->escape($data['message']) . "', notify_message = '" . $this->db->escape($data['notify_message']) . "' WHERE event_id = '" . $event_id . "'");
	
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_invitees WHERE event_id = '" . (int)$event_id . "'");

		if (isset($data['attendee'])) {
		  foreach ($data['attendee'] as $a) {
			if (empty($a['email']) || empty($a['name'])) {
				continue;
			}
     		$this->db->query("INSERT INTO " . DB_PREFIX . "events_invitees SET event_id = '" . (int)$event_id . "', name = '" . $this->db->escape($a['name']) . "', email = '" . $this->db->escape($a['email']) . "'");
		  }
		}
		if (isset($this->session->data['event' . $event_id])) {
			foreach ($this->session->data['event' . $event_id] as $key => $quantity) {
				$product = explode(':', $key);
				
				$product_id = $product[0];
				
				if (isset($product[1])) {
					$product_options = $product[1];
				} else {
					$product_options = '';
				}
				$comment = $this->db->escape($data['comment' . '-' . $this->db->escape($key)]);
   		  		$this->db->query("UPDATE " . DB_PREFIX . "events_products SET quantity = '" . (int)$quantity . "' WHERE event_id = '" . (int)$event_id . "' AND product = '" . $this->db->escape($key) . "'");
   		  		$this->db->query("UPDATE " . DB_PREFIX . "events_products SET p_comment = '" . $comment . "' WHERE event_id = '" . (int)$event_id . "' AND product = '" . $this->db->escape($key) . "'");
			}
		}
	}
	
	public function deleteEventProduct($event_id, $key) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "' AND product = '" . $key . "'");
		
	}
	
	public function addEventProducts($data) {
		if (isset($data['option'])) {
      		$key = (int)$data['product_id'] . ':' . base64_encode(serialize($data['option']));
			$options = base64_encode(serialize($data['option']));
    	} else {
      		$key = (int)$data['product_id'];
			$options = '';
    	}

		$update = 0;
		$records = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$data['event_id'] . "'");
		foreach ($records->rows as $record) {

			if ($record['product'] == $key) {
				$update = 1;
				$qnty = $record['quantity'];
			}
		}

		if ($update === 1) {
     		$this->db->query("UPDATE " . DB_PREFIX . "events_products SET quantity = '" . ($qnty + (int)$data['quantity']) . "' WHERE event_id = '" . (int)$data['event_id'] . "' AND product = '" . $this->db->escape($key) . "'");
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "events_products SET event_id = '" . (int)$data['event_id'] . "', customer_id = '" . (int)$this->customer->getId() . "', product = '" . $this->db->escape($key) . "', product_id = '" . (int)$data['product_id'] . "', options = '" . $this->db->escape($options) . "', quantity = '" . (int)$data['quantity'] . "'");
		}
	}

	public function addEventProductsFromChooser($data, $event_id) {
		if (isset($data['option'])) {
      		$key = (int)$data['product_id'] . ':' . base64_encode(serialize($data['option']));
			$options = base64_encode(serialize($data['option']));
    	} else {
      		$key = (int)$data['product_id'];
			$options = '';
    	}

		$update = 0;
		$records = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$event_id . "'");
		foreach ($records->rows as $record) {

			if ($record['product'] == $key) {
				$update = 1;
				$qnty = $record['quantity'];
			}
		}

		if ($update === 1) {
     		$this->db->query("UPDATE " . DB_PREFIX . "events_products SET quantity = '" . ($qnty + (int)$data['quantity']) . "' WHERE event_id = '" . (int)$event_id . "' AND product = '" . $this->db->escape($key) . "'");
		} else {
			$this->db->query("INSERT INTO " . DB_PREFIX . "events_products SET event_id = '" . (int)$event_id . "', customer_id = '" . (int)$this->customer->getId() . "', product = '" . $this->db->escape($key) . "', product_id = '" . (int)$data['product_id'] . "', options = '" . $this->db->escape($options) . "', quantity = '" . (int)$data['quantity'] . "'");
		}
	}


	public function installDB() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events` (`event_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT, `customer_id` mediumint(8) unsigned NOT NULL DEFAULT '0', `start_date` date NOT NULL DEFAULT '0000-00-00',`end_date` date NOT NULL DEFAULT '0000-00-00', `type` tinyint(1) NOT NULL, `status` tinyint(1) NOT NULL, `enable_notes` tinyint(1) NOT NULL, `enable_rsvp` tinyint(1) NOT NULL, `photo` VARCHAR( 255 ) NOT NULL, `web_link` VARCHAR(255) NOT NULL, `address_id` INT( 11 ) NOT NULL, `searchable` tinyint(1) NOT NULL, `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `e_comment` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `notify_message` TEXT CHARACTER SET utf8 COLLATE utf8_bin, `name` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `email` varchar(64) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `access_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, PRIMARY KEY (`event_id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events_invitees` (`event_id` mediumint(8) unsigned NOT NULL DEFAULT '0', `name` varchar(128) NOT NULL DEFAULT '', `email` varchar(64) NOT NULL DEFAULT '', PRIMARY KEY (`event_id`,`email`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");	
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events_products` (`item_id` int(11) NOT NULL AUTO_INCREMENT, `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0', `product` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `product_id` mediumint(8) NOT NULL, `quantity` smallint(5) unsigned NOT NULL DEFAULT '0', `p_comment` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, `ordered_amount` smallint(5) unsigned NOT NULL DEFAULT '0', `customer_id` smallint(7) NOT NULL, `options` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, PRIMARY KEY (`item_id`,`event_id`), UNIQUE KEY `item_id` (`item_id`)) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events_notes` (`note_id` int(11) NOT NULL AUTO_INCREMENT, `event_id` int(11) NOT NULL, `customer_id` int(11) NOT NULL, `author` varchar(64) NOT NULL, `text` text NOT NULL, `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00', `access_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, PRIMARY KEY (`note_id`), KEY `event_id` (`event_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events_rsvp` (`rsvp_id` int(11) NOT NULL AUTO_INCREMENT, `event_id` int(11) NOT NULL, `name` varchar(64) NOT NULL, `short_note` text NOT NULL, `email` varchar(64) NOT NULL DEFAULT '', `guests` text NOT NULL, `access_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, PRIMARY KEY (`rsvp_id`), KEY `event_id` (`event_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
		$install_test = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "order_product` WHERE Field = 'eventpid'");
		if (!$install_test->num_rows) {
			$this->db->query("ALTER TABLE `" . DB_PREFIX . "order_product` ADD `eventpid` TEXT NOT NULL;");
		}
		
 	}
	
	public function getEventsSearch($data=array()) {
     	$sql = "SELECT * FROM " . DB_PREFIX . "events WHERE status = '1'";
					
			if (!empty($data['filter_name']) && !empty($data['filter_title']) && !empty($data['filter_owner'])) {
				$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%' OR LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";	
			} elseif (!empty($data['filter_name']) && !empty($data['filter_title'])) {
				$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%'";
			} elseif (!empty($data['filter_name']) && !empty($data['filter_owner'])) {
				$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "%'";	
			} elseif (!empty($data['filter_title']) && !empty($data['filter_owner'])) {
				$sql .= " AND LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%' OR LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";	
			} elseif (!empty($data['filter_name'])) {
				$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
			} elseif (!empty($data['filter_title'])) {
				$sql .= " AND LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%'";
			} elseif (!empty($data['filter_owner'])) {
				$sql .= " AND LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";
			}
			
			$sort_data = array(
				'title',
				'start_date',
				'end_date',
				'type'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY end_date";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'ASC')) {
				$sql .= " ASC";
			} else {
				$sql .= " DESC";
			}
			
		if (isset($data['start'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				
			if ($data['limit'] < 1) {
				$data['limit'] = 10;
			}	
		
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
					
						
			$query = $this->db->query($sql);
		
		return $query -> rows;
	}

	public function getTotalEvents($data=array()) {
     	$sql = "SELECT COUNT(DISTINCT event_id) AS total FROM " . DB_PREFIX . "events WHERE status = 1";

		if (!empty($data['filter_name']) && !empty($data['filter_title']) && !empty($data['filter_owner'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%' OR LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";	
		} elseif (!empty($data['filter_name']) && !empty($data['filter_title'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%'";
		} elseif (!empty($data['filter_name']) && !empty($data['filter_owner'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%' OR LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "%'";	
		} elseif (!empty($data['filter_title']) && !empty($data['filter_owner'])) {
			$sql .= " AND LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%' OR LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";	
		} elseif (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		} elseif (!empty($data['filter_title'])) {
			$sql .= " AND LCASE(title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%'";
		} elseif (!empty($data['filter_owner'])) {
			$sql .= " AND LCASE(email) = '" . $this->db->escape(utf8_strtolower($data['filter_owner'])) . "'";
		}
	
		$query = $this->db->query($sql);
		
		if (isset($query->row['total'])) {
			return $query->row['total'];
		} else {
			return 0;	
		}
	}
	
	public function addNote($event_id, $akey, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "events_notes SET author = '" . $this->db->escape($data['name']) . "', customer_id = '" . (int)$this->customer->getId() . "', event_id = '" . (int)$event_id . "', text = '" . $this->db->escape($data['text']) . "', date_added = NOW(), access_key = '" . $this->db->escape($akey) . "'");
	}
	
	public function removeNote($event_id, $akey, $data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_notes WHERE event_id = '" . (int)$event_id . "' AND note_id = '" . $this->db->escape($data['note_id']) . "' AND access_key = '" . $this->db->escape($akey) . "'");
	}
		
	public function getNotesByEventId($event_id, $akey, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		if ($limit < 1) {
			$limit = 20;
		}		
		
		$query = $this->db->query("SELECT en.note_id, en.author, en.text, e.event_id, e.title, en.date_added FROM " . DB_PREFIX . "events_notes en LEFT JOIN " . DB_PREFIX . "events e ON (en.event_id = e.event_id) WHERE e.event_id = '" . (int)$event_id . "' AND e.status = '1' AND en.access_key = '" . $this->db->escape($akey) . "' ORDER BY en.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
			
		return $query->rows;
	}

	public function getTotalNotesByEvent_id($event_id, $akey) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "events_notes en LEFT JOIN " . DB_PREFIX . "events e ON (en.event_id = e.event_id) WHERE e.event_id = '" . (int)$event_id . "' AND e.status = '1' AND en.access_key = '" . $this->db->escape($akey) . "'");
		
		return $query->row['total'];
	}
	
	public function getTotalNotesByEvent_id_Account($event_id, $akey) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "events_notes en LEFT JOIN " . DB_PREFIX . "events e ON (en.event_id = e.event_id) WHERE e.event_id = '" . (int)$event_id . "' AND en.access_key = '" . $this->db->escape($akey) . "'");
		
		return $query->row['total'];
	}
	
	public function getNotesByEventId_Account($event_id, $akey, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		if ($limit < 1) {
			$limit = 20;
		}		
		
		$query = $this->db->query("SELECT en.note_id, en.author, en.text, e.event_id, e.title, en.date_added, en.access_key FROM " . DB_PREFIX . "events_notes en LEFT JOIN " . DB_PREFIX . "events e ON (en.event_id = e.event_id) WHERE e.event_id = '" . (int)$event_id . "'  AND en.access_key = '" . $this->db->escape($akey) . "' ORDER BY en.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);
			
		return $query->rows;
	}
	
	public function getEventRSVPs($event_id, $akey) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_rsvp WHERE event_id = '" . $event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		return $query -> rows;
	}
	
	public function getEventRSVPguests($event_id, $akey, $email) {
		$query = $this->db->query("SELECT guests FROM " . DB_PREFIX . "events_rsvp WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "' AND email = '" . $this->db->escape($email) . "'");

		$exploded = array();
		$return = array();
		
		foreach ($query -> rows as $v) {
			foreach ($v as $k) {
				$exploded = explode(',', $k);
			}
		}
		
		return $exploded;
	}
	
	public function addRSVP($data) {
		$guests = $this->db->escape($data['entry_guest_name_1']).','.$this->db->escape($data['entry_guest_name_2']).','.$this->db->escape($data['entry_guest_name_3']).','.$this->db->escape($data['entry_guest_name_4']).','.$this->db->escape($data['entry_guest_name_5']).','.$this->db->escape($data['entry_guest_name_6']).','.$this->db->escape($data['entry_guest_name_7']).','.$this->db->escape($data['entry_guest_name_8']).','.$this->db->escape($data['entry_guest_name_9']).','.$this->db->escape($data['entry_guest_name_10']);
		//		$this->db->query("INSERT INTO " . DB_PREFIX . "events_rsvp SET event_id = '" . (int)$data['eid'] . "', name = '" . $this->db->escape($data['rsvp_name']) . "', short_note = '" . $this->db->escape($data['short_note']) . "', email = '" . $this->db->escape($data['rsvp_email']) . "', guest1 = '" . $this->db->escape($data['entry_guest_name_1']) . "', guest2 = '" . $this->db->escape($data['entry_guest_name_2']) . "', guest3 = '" . $this->db->escape($data['entry_guest_name_3']) . "', guest4 = '" . $this->db->escape($data['entry_guest_name_4']) . "', guest5 = '" . $this->db->escape($data['entry_guest_name_5']) . "', guest6 = '" . $this->db->escape($data['entry_guest_name_6']) . "', guest7 = '" . $this->db->escape($data['entry_guest_name_7']) . "', guest8 = '" . $this->db->escape($data['entry_guest_name_8']) . "', guest9 = '" . $this->db->escape($data['entry_guest_name_9']) . "', guest10 = '" . $this->db->escape($data['entry_guest_name_10']) . "', access_key = '" . $this->db->escape($data['akey']) . "'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "events_rsvp SET event_id = '" . (int)$data['eid'] . "', name = '" . $this->db->escape($data['rsvp_name']) . "', short_note = '" . $this->db->escape($data['short_note']) . "', email = '" . $this->db->escape($data['rsvp_email']) . "', guests = '" . $guests . "', access_key = '" . $this->db->escape($data['akey']) . "'");
	}
	
	public function checkRSVPemail($email) {
     	$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_rsvp WHERE email = '" . $this->db->escape($email) . "'");

		if ($query->num_rows) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getRSVPstatus($event_id, $akey) {
		$query = $this->db->query("SELECT enable_rsvp FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		if ($query->num_rows && $query->row['enable_rsvp'] == 1) {
			return true;
		} else {
			return false;
		}
	}

	public function getNoteStatus($event_id, $akey) {
		$query = $this->db->query("SELECT enable_notes FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$event_id . "' AND access_key = '" . $this->db->escape($akey) . "'");

		if ($query->num_rows &&  $query->row['enable_notes'] == 1) {
			return true;
		} else {
			return false;
		}
	}
	
}
?>
<?php
// Text
$_['text_home']           = 'Home';
$_['text_wishlist']       = 'Wish List (%s)';
$_['text_shopping_cart']  = 'Shopping Cart';
$_['text_search']         = 'Search';
//$_['text_welcome']        = 'Welcome visitor you can <a href="%s">login</a> or <a href="%s">create an account</a>.';
$_['text_welcome']        = '<a href="%s">Sign In</a> or <a href="%s">create an account</a>.';
$_['text_logged']         = 'You are logged in as <br><a href="%s">%s</a> <b>(</b> <a href="%s">Logout</a> <b>)</b>';
$_['text_account']        = 'My Account';

$_['text_events']      = 'My Events';
            
$_['text_checkout']       = 'Checkout';
?>
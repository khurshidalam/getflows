<div id="footer_outer">
<div id="footer">
<div class="innerwrapper">
  <?php if ($informations) { ?>
  <div class="column">
    <h3><?php echo $text_information; ?></h3>
    <ul>
      <?php foreach ($informations as $information) { ?>
      <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
      <?php } ?>
    </ul>
  </div>
  <?php } ?>
  <div class="column">
    <h3><?php echo $text_service; ?></h3>
    <ul>
      <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
      <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $sitemap; ?>"><?php echo $text_sitemap; ?></a></li>
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_extra; ?></h3>
    <ul>
      <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
      <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
      <!--<li><a href="<?php //echo $affiliate; ?>"><?php //echo $text_affiliate; ?></a></li>-->
      <li><a href="<?php echo $special; ?>"><?php echo $text_special; ?></a></li>
<?php if($this->config->get('myocwpl_status') && $this->config->get('myocwpl_store') && in_array($this->config->get('config_store_id'), $this->config->get('myocwpl_store'))) { ?><li><a href="<?php echo $this->url->link('product/pricelist', '', 'SSL'); ?>"><?php $this->language->load('myoc/pricelist'); ?><?php echo $this->language->get('text_pricelist'); ?></a></li><?php } ?>
      <li><a href="index.php?route=product/testimonial">Testimonials</a></li>
    </ul>
  </div>
  <div class="column">
    <h3><?php echo $text_account; ?></h3>
    <ul>
      <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>

      <li><a href="<?php echo $events; ?>"><?php echo $text_events; ?></a></li>
            
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <div class="column-rgt">
       <h3 class="last">
    <span id="txtHint"> <h3 class="last">Subscribe Newsletter</h3></span></h3>
    
    <script>
function showHint() {
      var email = document.getElementById('name');
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email.value)) {
    alert('Please provide a valid email address');
    email.focus;
    return false;
 }
 
  var str=document.getElementById('name').value;
  var xmlhttp=new XMLHttpRequest();
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      //document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
      window.location.href = 'index.php?route=information/information&information_id=7';
      
    }
  }
  xmlhttp.open("GET","gethint.php?q="+str,true);
  xmlhttp.send();
}
</script>
    
    
    
    <form action="" method="post" >
    <input name="email" type="text" class="newslter_input" placeholder="Email address..." id="name" />
 <input name="submit" type="button" class="foot_signupbtn" value="SignUp" onclick="showHint()" />

    </form>
  </div>

  
  <!--bottom area open-->
  <div class="footer_btm_area">
  
  <div class="footer_link">
  <ul>
  <li><a><img src="catalog/view/theme/B3netdemostore/images/footer-link/facebook-icon.png" width="23" height="24" /></a></li>
  <li><a><img src="catalog/view/theme/B3netdemostore/images/footer-link/twitter-icon.png" width="23" height="24" /></a></li>
  <li><a><img src="catalog/view/theme/B3netdemostore/images/footer-link/youtube-icon.png" width="23" height="24" /></a></li>
  <li><a><img src="catalog/view/theme/B3netdemostore/images/footer-link/google-icon.png" width="23" height="24" /></a></li>
  </ul>
  </div>
  
  <div id="powered">
    
   © Copyright 2014 B3NET Demo Store  <?php
//if ($this->request->get['route'] == 'common/home')
if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] == 'common/home') {
echo  "| Powered by<a href='http://www.b3net.com/orange-county-ecommerce-developer.html' target='_blank'> B3NET.com</a>";

}?>
     </div>
  
  </div>
  <!--bottom area closed-->
  
</div>
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
<!--<div id="powered"><?php echo $powered; ?></div>-->
<!--
OpenCart is open source software and you are free to remove the powered by OpenCart if you want, but its generally accepted practise to make a small donation.
Please donate via PayPal to donate@opencart.com
//-->
</div>
</div>

</div>
<script>
$(window).load(function() {
    $("#flexiselDemo4").flexisel({
        clone:false
    });
	$("#flexiselDemo5").flexisel({
        clone:false,
		visibleItems: 5
    });
    
});
</script>
<script type="text/JavaScript">
// prepare the form when the DOM is ready 
$(document).ready(function() {
	$("#gallery-pic li img").hover(function(){
		$('#main-img').attr('src',$(this).attr('src').replace('thumb/', ''));
	});
	var imgSwap = [];
	 $("#gallery-pic li img").each(function(){
		imgUrl = this.src.replace('thumb/', '');
		imgSwap.push(imgUrl);
	});
	$(imgSwap).preload();
});
$.fn.preload = function() {
    this.each(function(){
        $('<img/>')[0].src = this;
    });
	
	
	if($("#gallery-scroll-area").length){
        
        // Declare variables
        var totalImages = $("#gallery-scroll-area > li").length, 
            imageWidth = $("#gallery-scroll-area > li:first").outerWidth(true),
            totalWidth = imageWidth * totalImages,
            visibleImages = Math.round($("#gallery-wrap-area-scroll").width() / imageWidth),
            visibleWidth = visibleImages * imageWidth,
            stopPosition = (visibleWidth - totalWidth);
            
        $("#gallery-scroll-area").width(totalWidth);
        
        $("#gallery-scroll-area-prev").click(function(){
            if($("#gallery-scroll-area").position().left < 0 && !$("#gallery-scroll-area").is(":animated")){
                $("#gallery-scroll-area").animate({left : "+=" + imageWidth + "px"});
            }
            return false;
        });
        
        $("#gallery-scroll-area-next").click(function(){
			
            if($("#gallery-scroll-area").position().left > stopPosition && !$("#gallery").is(":animated")){
                $("#gallery-scroll-area").animate({left : "-=" + imageWidth + "px"});
            }
            return false;
        });
    }
}
</script>
</body></html>
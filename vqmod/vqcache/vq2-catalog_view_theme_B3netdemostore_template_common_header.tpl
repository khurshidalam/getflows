<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<!--[if lt IE 9]>
<style>body {min-width: 760px;}#columns {zoom: 1;}.p7PMMv19 ul ul {border: 1px solid #444;}</style>
<![endif]-->
<!--[if lt IE 8]>
<style>.three-column-column2 {width: 30%;}.main-content {width: 59%;}</style>
<![endif]-->
<!--[if lte IE 6]>
<style>#masthead, #columns, #footer {width: 980px;}</style>
<![endif]-->
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php if ($icon) { ?>
<link href="<?php echo $icon; ?>" rel="icon" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="catalog/view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="catalog/view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="catalog/view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" />
<script type="text/javascript" src="catalog/view/javascript/common.js"></script>
<?php foreach ($scripts as $script) { ?>
<script type="text/javascript" src="<?php echo $script; ?>"></script>
<?php } ?>

<link href="catalog/view/theme/B3netdemostore/css/blue-mobile.css" rel="stylesheet" type="text/css" />
<script src="catalog/view/theme/B3netdemostore/js/blue-mobile.js"></script> 

<!--[if IE 7]> 
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/stylesheet/ie7.css" />
<![endif]-->
<!--[if lt IE 7]>
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/stylesheet/ie6.css" />
<script type="text/javascript" src="catalog/view/javascript/DD_belatedPNG_0.0.8a-min.js"></script>
<script type="text/javascript">
DD_belatedPNG.fix('#logo img');
</script>
<![endif]-->
<?php if ($stores) { ?>
<script type="text/javascript"><!--
$(document).ready(function() {
<?php foreach ($stores as $store) { ?>
$('body').prepend('<iframe src="<?php echo $store; ?>" style="display: none;"></iframe>');
<?php } ?>
});
//--></script>
<?php } ?>
<?php echo $google_analytics; ?>

<!-- custom css and jss start-->
 <link href="catalog/view/theme/B3netdemostore/css/style2.css" rel="stylesheet" type="text/css" />
 <link href="catalog/view/theme/B3netdemostore/css/mobile2.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/css/fonts.css" />
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/css/mobile.css" />
<!--<script src="catalog/view/theme/B3netdemostore/js/icheck.js"></script>-->


<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/css/easy-responsive-tabs.css" />
<script src="catalog/view/theme/B3netdemostore/js/easyResponsiveTabs.js"></script>

<style type="text/css">
        .demo {
            width: 980px;
            margin: 0px auto;
        }
        .demo h1 {
                margin:33px 0 25px;
            }
        .demo h3 {
                margin: 10px 0;
            }
        pre {
            background: #fff;
        }
        @media only screen and (max-width: 780px) {
        .demo {
                margin: 5%;
                width: 90%;
         }
        .how-use {
                float: left;
                width: 300px;
                display: none;
            }
        }
        #tabInfo {
            display: none;
        }
    </style>


<link href="catalog/view/theme/B3netdemostore/css/dropdown-list.css" rel="stylesheet">
<script src="catalog/view/theme/B3netdemostore/js/dropdown-list-jquery.js"></script>
<script src="catalog/view/theme/B3netdemostore/js/dropdown-list-jquery-2.js"></script>

<div style="display: none;">
        
<script type="text/javascript" src="catalog/view/theme/B3netdemostore/js/jquery.flexisel.js"></script>
<link href="catalog/view/theme/B3netdemostore/css/flexisel-style.css" rel="stylesheet" type="text/css" />

<?php if ($this->request->get['route'] != 'product/product'){ ?>

<script src="catalog/view/theme/B3netdemostore/js/jquery-1.9.1.min.js"></script>
<script src="catalog/view/theme/B3netdemostore/js/owl.carousel.js"></script>
<script src="catalog/view/theme/B3netdemostore/js/brand-carousel.js"></script>

<?php } ?>
<link href="catalog/view/theme/B3netdemostore/css/brand-carousel-style.css" rel="stylesheet">
<?php if ($this->request->get['route'] != 'product/product'){ ?>       
<script src="catalog/view/theme/B3netdemostore/js/prduct-thumb-carousel.js"></script>
<?php } ?>
<link href="catalog/view/theme/B3netdemostore/css/product-thumb-carousel-style.css" rel="stylesheet">
<link href="catalog/view/theme/B3netdemostore/css/owl.carousel.css" rel="stylesheet">
<link href="catalog/view/theme/B3netdemostore/css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="catalog/view/theme/B3netdemostore/css/jquery.fancybox-1.3.1.css" media="screen" />
<?php if ($this->request->get['route'] != 'product/product'){ ?>  
  <script type="text/javascript" src="catalog/view/theme/B3netdemostore/js/jquery.fancybox-1.3.1.js"></script>
    <script type="text/javascript">
    // Fancybox specific
    // To make images pretty. Not important
    $(document).ready(function(){
		var $p = jQuery.noConflict();
        $p(".gallery__link").fancybox({
            'titleShow'     : false,
            'transitionIn'  : 'elastic',
            'transitionOut' : 'elastic'
        });
    });
    </script>



<script src="catalog/view/theme/b2bstore/js/prouct-detail-thumb-js.js"></script>

<?php } ?>
</div>


</head>
<body>
 
<!----------------------google analytics code start ------------------------------>
 <!-- Piwik -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://searchomatrix.com/" : "http://searchomatrix.com/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 84);
piwikTracker.trackPageView();
piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://searchomatrix.com/piwik.php?idsite=84" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Tracking Code -->
<!----------------------google analytics code end ------------------------------>


        
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
               // var $info = $('#tabInfo');
               // var $name = $('span', $info);

                $name.text($tab.text());

                $info.show();
            }
        });


    });
</script>
<div id="container">

<!-- header area open-->
<div class="headerArea">
<div class="innerwrapper">
<div id="header">
  <!--mobile menu-->
  <?php if ($categories) { ?>
<div class="resmenuAra">
<div href="#blue-mobile-menu"></div>
<ul id="blue-mobile-menu">
<?php foreach ($categories as $category) { ?>
<li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
<?php if ($category['children']) { ?>
<?php for ($i = 0; $i < count($category['children']);) { ?>
<ul>
<?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
<li><a href="<?php echo $category['children'][$i]['href']; ?>"><?php echo $category['children'][$i]['name']; ?></a></li>
          <?php } ?>
          <?php } ?>
</ul>
<?php } ?>
<?php } ?>
</li>
<?php } ?>
</ul>
</div>
<?php } ?>
  <!--mobile menu closed-->
  <?php if ($logo) { ?>
  <div id="logo"><a href="<?php echo $home; ?>">
  <!--<img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" />-->
  <img src="catalog/view/theme/B3netdemostore/images/logo.png" alt="" title="" border="0"> </a></div>
  <?php } ?>
  <?php echo $language; ?>
  <?php echo $currency; ?>
  <div id="welcome-mobile">
  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
    <!--<a>Sign In</a>  or  <a>Create an Account</a> -->
  </div>
  </div>
  <div id="search">
    <input type="text" name="search" placeholder="<?php echo $text_search; ?>" value="<?php echo $search; ?>" />
    <div class="button-search"></div>
  </div>
  <?php echo $cart; ?>
  <div id="welcome-desktop">
  <div id="welcome">
    <?php if (!$logged) { ?>
    <?php echo $text_welcome; ?>
    <?php } else { ?>
    <?php echo $text_logged; ?>
    <?php } ?>
    <!--<a>Sign In</a>  or  <a>Create an Account</a> -->
  </div>
  </div>
  

  

           
	    <?php foreach ($modules as $module) { ?>
  <?php echo $module; ?>
  <?php } ?>
            
  <div class="links"><a href="<?php echo $home; ?>"><?php echo $text_home; ?></a><?php if($this->config->get('myocwpl_status') && $this->config->get('myocwpl_store') && in_array($this->config->get('config_store_id'), $this->config->get('myocwpl_store'))) { ?><a href="<?php echo $this->url->link('product/pricelist', '', 'SSL'); ?>"><?php $this->language->load('myoc/pricelist'); ?><?php echo $this->language->get('text_pricelist'); ?></a><?php } ?><a href="<?php echo $wishlist; ?>" id="wishlist-total"><?php echo $text_wishlist; ?></a>
<a href="<?php echo $events; ?>"><?php echo $text_events; ?></a><a href="<?php echo $account; ?>">
            <?php echo $text_account; ?></a><a href="<?php echo $shopping_cart; ?>"><?php echo $text_shopping_cart; ?></a><a href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a></div>
</div>
</div>
</div>
<!-- header area closed-->

<!--menu area open-->
<div class="menuArea">
<div class="innerwrapper">
<?php if ($categories) { ?>
<div id="menu">
  <ul>
    <?php foreach ($categories as $category) { ?>
    <li><a href="<?php echo $category['href']; ?>"  onmouseover="openSubMenu2()"><?php echo $category['name']; ?></a>
      <?php if ($category['children']) { ?>
      <div>
        <?php for ($i = 0; $i < count($category['children']);) { ?>
        <ul>
          <?php $j = $i + ceil(count($category['children']) / $category['column']); ?>
          <?php for (; $i < $j; $i++) { ?>
          <?php if (isset($category['children'][$i])) { ?>
          <li>

			<?php
			if(count($category['children'][$i]['children_level2'])>0){
			?>
				<a href="<?php echo $category['children'][$i]['href']; ?>" onmouseover='JavaScript:openSubMenu("<?php echo $category['children'][$i]['id']; ?>")'><?php echo $category['children'][$i]['name']; ?>
			<?php

				echo "</a>";
				//echo "<span style='right:5px;margin-top:-6px;position:absolute;color:white;font-weight:bold;font-size:18px;'>&#187;</span></a>";

			}
			else
			{
			?>
			<a href="<?php echo $category['children'][$i]['href']; ?>" onmouseover="openSubMenu2()"
 ><?php echo $category['children'][$i]['name']; ?></a>
			<?php
			}
			?>

			<?php if ($category['children'][$i]['children_level2']) { ?>
			      <div class="submenu" id="id_menu_<?php echo $category['children'][$i]['id']; ?>">
			       <ul>
			        <?php for ($wi = 0; $wi < count($category['children'][$i]['children_level2']); $wi++) { ?>
			          <li>
			          	<a href="<?php echo $category['children'][$i]['children_level2'][$wi]['href']; ?>"><?php echo $category['children'][$i]['children_level2'][$wi]['name']; ?>
			          	</a>
		          	  </li>
		          <?php } ?>
		        </ul>
		      </div>
		      <?php } ?>

          </li>
          <?php } ?>
          <?php } ?>
        </ul>
        <?php } ?>
      </div>
      <?php } ?>
    </li>
    <?php } ?>
  </ul>
</div>
<?php } ?>

</div>
</div>
<!-- menu area closed-->

</div>

<?php if ($error) { ?>
    
    <div class="warning"><?php echo $error ?><img src="catalog/view/theme/B3netdemostore/image/close.png" alt="" class="close" /></div>
    
<?php } ?>
<div class="innerwrapper"><?php
//if ($this->request->get['route'] != 'common/home')
if (!isset($this->request->get['route']) || isset($this->request->get['route']) && $this->request->get['route'] != 'common/home') {
echo  "<div id='notification'>
</div>";

}?>

</div>

<script>
$("#blue-mobile-menu").blueMobileMenu();
</script>

<script type="text/javascript">
function openSubMenu(id){
	
	$('.submenu').hide();
	document.getElementById("id_menu_"+id).style.display="block";
	
}
function openSubMenu2(){

$('.submenu').hide();
document.getElementById("id_menu_"+id).style.display="none";

}

//$(document).ready(function(e) {
//	$('#menu > ul > li').hover(function(){
//    $('#menu > ul > li').has('.submenu').addClass('subshow');
//		
//	});
	 //$(this).siblings('document.getElementById("id_menu_"+id)').fadeOut(2000);
//});
</script>

<!--css breaking for this reason start-->
<style>
.submenu{
    /*background: url('catalog/view/theme/default/image/menu.png') repeat scroll 0 0 transparent;*/
	background:#fff;
    margin-top:-32px;
    left:140px;
    position:absolute;
    min-width:140px;
    display:none;
	min-height:25px;
	box-shadow:#CCC 0px 0px 3px;
}
.subshow{
	display:block;
}
/*#menu > ul > li >:hover .submenu{
	display:block;
}*/
</style>
<!--[if IE 7]>
<style>
#menu > ul > li > div {
width:140px!important;
}
.submenu{
   left:145px;
}
</style>
<![endif]-->
<!--[if IE 8]>
<style>
#menu > ul > li > div {
width:140px!important;
}
.submenu{
   left:150px;
}
</style>
<!--css breaking for this reason end-->
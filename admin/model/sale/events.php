<?php
// Version 1.4
class ModelSaleEvents extends Model {
	private $data = array();
	public function deleteEvent($eid) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "events WHERE event_id = '" . (int)$eid . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_invitees WHERE event_id = '" . (int)$eid . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_products WHERE event_id = '" . (int)$eid . "'");
	}	

	public function getEvents($data) {
		$sql = "SELECT e.*, CONCAT(c.firstname, ' ', c.lastname) as customer_name FROM " . DB_PREFIX . "customer c INNER JOIN " . DB_PREFIX . "events e ON c.customer_id = e.customer_id ";
		
		if (isset($data['filter_event_id'])) {
			$sql .= " WHERE event_id = '" . (int)$data['filter_event_id'] . "'";
		} else {
			$sql .= " WHERE event_id > '0'";
		}

		if (!empty($data['filter_customer_id'])) {
			$sql .= " AND LCASE(CONCAT(c.firstname, ' ', c.lastname)) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_customer_id'])) . "%'";
		}

		if (!empty($data['filter_title'])) {
			$sql .= " AND LCASE(e.title) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_title'])) . "%'";
		}

		if (!empty($data['filter_name'])) {
			$sql .= " AND LCASE(e.name) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_name'])) . "%'";
		}

		if (!empty($data['filter_email'])) {
			$sql .= " AND LCASE(e.email) LIKE '%" . $this->db->escape(utf8_strtolower($data['filter_email'])) . "%'";
		}

		$sort_data = array(
			'event_id',
			'customer_id',
			'title',
			'name',
			'email'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY event_id";
		}

		if (isset($data['order']) && ($data['order'] == 'ASC')) {
			$sql .= " ASC";
		} else {
			$sql .= " DESC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTotalEvents() {
     	$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "events");
		return $query->row['total'];
	}
	
	public function getEventDetails($event_id) {
		$query = $this->db->query("SELECT e.*, CONCAT(c.firstname, ' ', c.lastname) AS customer FROM " . DB_PREFIX . "customer c INNER JOIN " . DB_PREFIX . "events e ON c.customer_id = e.customer_id WHERE e.event_id = '" . (int)$event_id . "'");

		return $query->row;
	}
	
	public function getCustomerName($customer_id) {
     	$query = $this->db->query("SELECT CONCAT(firstname, ' ', lastname) AS customer_name FROM " . DB_PREFIX . "customer WHERE customer_id = '" . (int)$customer_id . "'");
		return $query->row['customer_name'];
	}	

	public function editEvent($id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "events SET customer_id='" . $this->db->escape($data['customer_id']) . "', email='" . $this->db->escape($data['email']) . "' WHERE event_id = '" . (int)$id . "'");
	}
	
	public function getEventsViewProducts($event_id) {
     	$query = $this->db->query("SELECT product, quantity FROM " . DB_PREFIX . "events_products WHERE event_id = '" . $event_id . "'");
		if($query->num_rows) {
			foreach ($query->rows as $key => $value) {
				$results[$value['product']] = $value['quantity'];
			}
		return $results;
		}
	}

  	public function getProductsForEventsView($prods, $eid) {
	  if ($prods) {
		if (!$this->data) {
			foreach ($prods as $key => $quantity) {
				$product = explode(':', $key);
				$product_id = $product[0];
				$stock = true;
	
				// Options
				if (isset($product[1])) {
					$options = unserialize(base64_decode($product[1]));
				} else {
					$options = array();
				} 
				
				$product_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.date_available <= NOW() AND p.status = '1'");
				
				if ($product_query->num_rows) {
					$option_price = 0;
					$option_points = 0;
					$option_weight = 0;
	
					$option_data = array();
	
					foreach ($options as $product_option_id => $option_value) {
						$option_query = $this->db->query("SELECT po.product_option_id, po.option_id, od.name, o.type FROM " . DB_PREFIX . "product_option po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE po.product_option_id = '" . (int)$product_option_id . "' AND po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$this->config->get('config_language_id') . "'");
						
						if ($option_query->num_rows) {
							if ($option_query->row['type'] == 'select' || $option_query->row['type'] == 'radio' || $option_query->row['type'] == 'image') {
								$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$option_value . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
								
								if ($option_value_query->num_rows) {
									if ($option_value_query->row['price_prefix'] == '+') {
										$option_price += $option_value_query->row['price'];
									} elseif ($option_value_query->row['price_prefix'] == '-') {
										$option_price -= $option_value_query->row['price'];
									}
	
									if ($option_value_query->row['points_prefix'] == '+') {
										$option_points += $option_value_query->row['points'];
									} elseif ($option_value_query->row['points_prefix'] == '-') {
										$option_points -= $option_value_query->row['points'];
									}
																
									if ($option_value_query->row['weight_prefix'] == '+') {
										$option_weight += $option_value_query->row['weight'];
									} elseif ($option_value_query->row['weight_prefix'] == '-') {
										$option_weight -= $option_value_query->row['weight'];
									}
									
									if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
										$stock = false;
									}
									
									$option_data[] = array(
										'product_option_id'       => $product_option_id,
										'product_option_value_id' => $option_value,
										'option_id'               => $option_query->row['option_id'],
										'option_value_id'         => $option_value_query->row['option_value_id'],
										'name'                    => $option_query->row['name'],
										'option_value'            => $option_value_query->row['name'],
										'type'                    => $option_query->row['type'],
										'quantity'                => $option_value_query->row['quantity'],
										'subtract'                => $option_value_query->row['subtract'],
										'price'                   => $option_value_query->row['price'],
										'price_prefix'            => $option_value_query->row['price_prefix'],
										'points'                  => $option_value_query->row['points'],
										'points_prefix'           => $option_value_query->row['points_prefix'],									
										'weight'                  => $option_value_query->row['weight'],
										'weight_prefix'           => $option_value_query->row['weight_prefix']
									);								
								}
							} elseif ($option_query->row['type'] == 'checkbox' && is_array($option_value)) {
								foreach ($option_value as $product_option_value_id) {
									$option_value_query = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_option_value_id = '" . (int)$product_option_value_id . "' AND pov.product_option_id = '" . (int)$product_option_id . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
									
									if ($option_value_query->num_rows) {
										if ($option_value_query->row['price_prefix'] == '+') {
											$option_price += $option_value_query->row['price'];
										} elseif ($option_value_query->row['price_prefix'] == '-') {
											$option_price -= $option_value_query->row['price'];
										}
	
										if ($option_value_query->row['points_prefix'] == '+') {
											$option_points += $option_value_query->row['points'];
										} elseif ($option_value_query->row['points_prefix'] == '-') {
											$option_points -= $option_value_query->row['points'];
										}
																	
										if ($option_value_query->row['weight_prefix'] == '+') {
											$option_weight += $option_value_query->row['weight'];
										} elseif ($option_value_query->row['weight_prefix'] == '-') {
											$option_weight -= $option_value_query->row['weight'];
										}
										
										if ($option_value_query->row['subtract'] && (!$option_value_query->row['quantity'] || ($option_value_query->row['quantity'] < $quantity))) {
											$stock = false;
										}
										
										$option_data[] = array(
											'product_option_id'       => $product_option_id,
											'product_option_value_id' => $product_option_value_id,
											'option_id'               => $option_query->row['option_id'],
											'option_value_id'         => $option_value_query->row['option_value_id'],
											'name'                    => $option_query->row['name'],
											'option_value'            => $option_value_query->row['name'],
											'type'                    => $option_query->row['type'],
											'quantity'                => $option_value_query->row['quantity'],
											'subtract'                => $option_value_query->row['subtract'],
											'price'                   => $option_value_query->row['price'],
											'price_prefix'            => $option_value_query->row['price_prefix'],
											'points'                  => $option_value_query->row['points'],
											'points_prefix'           => $option_value_query->row['points_prefix'],
											'weight'                  => $option_value_query->row['weight'],
											'weight_prefix'           => $option_value_query->row['weight_prefix']
										);								
									}
								}						
							} elseif ($option_query->row['type'] == 'text' || $option_query->row['type'] == 'textarea' || $option_query->row['type'] == 'file' || $option_query->row['type'] == 'date' || $option_query->row['type'] == 'datetime' || $option_query->row['type'] == 'time') {
								$option_data[] = array(
									'product_option_id'       => $product_option_id,
									'product_option_value_id' => '',
									'option_id'               => $option_query->row['option_id'],
									'option_value_id'         => '',
									'name'                    => $option_query->row['name'],
									'option_value'            => $option_value,
									'type'                    => $option_query->row['type'],
									'quantity'                => '',
									'subtract'                => '',
									'price'                   => '',
									'price_prefix'            => '',
									'points'                  => '',
									'points_prefix'           => '',								
									'weight'                  => '',
									'weight_prefix'           => ''
								);						
							}
						}
					} 
				
					$price = $product_query->row['price'];
					
					// Product Discounts
					$discount_quantity = 0;
					
					foreach ($prods as $key_2 => $quantity_2) {
						$product_2 = explode(':', $key_2);
						
						if ($product_2[0] == $product_id) {
							$discount_quantity += $quantity_2;
						}
					}
					
					// Downloads		
					$download_data = array();     		
					
					$download_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download p2d LEFT JOIN " . DB_PREFIX . "download d ON (p2d.download_id = d.download_id) LEFT JOIN " . DB_PREFIX . "download_description dd ON (d.download_id = dd.download_id) WHERE p2d.product_id = '" . (int)$product_id . "' AND dd.language_id = '" . (int)$this->config->get('config_language_id') . "'");
				
					foreach ($download_query->rows as $download) {
						$download_data[] = array(
							'download_id' => $download['download_id'],
							'name'        => $download['name'],
							'filename'    => $download['filename'],
							'mask'        => $download['mask'],
							'remaining'   => $download['remaining']
						);
					}
					
					$bought_data = array();     		
					
					$bought_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "events_products  WHERE product = '" . $this->db->escape($key) . "' AND event_id = '" . (int)$eid . "'");
				
					foreach ($bought_query->rows as $bought) {
						$bought_data[] = array(
							'bought_qty' => $bought['ordered_amount']
						);
					}
					
					// Stock
					if (!$product_query->row['quantity'] || ($product_query->row['quantity'] < $quantity)) {
						$stock = false;
					}
					
					
					if ($options) {
						$iid_query = $this->db->query("SELECT item_id FROM " . DB_PREFIX . "events_products WHERE product = '" . $key . "' AND event_id = '" . $eid . "'");
					}else{
						$iid_query = $this->db->query("SELECT item_id FROM " . DB_PREFIX . "events_products WHERE product_id = '" . $product_id . "' AND event_id = '" . $eid . "'");
					}
				
					$this->data[$key] = array(
						'key'             => $key,
						'product_id'      => $product_query->row['product_id'],
						'iid'			  => $iid_query->row['item_id'],
						'name'            => $product_query->row['name'],
						'model'           => $product_query->row['model'],
						'shipping'        => $product_query->row['shipping'],
						'image'           => $product_query->row['image'],
						'option'          => $option_data,
						'bought'		  => $bought_data,
						'download'        => $download_data,
						'quantity'        => $quantity,
						'minimum'         => $product_query->row['minimum'],
						'subtract'        => $product_query->row['subtract'],
						'stock'           => $stock,
						'price'           => ($price + $option_price),
						'total'           => ($price + $option_price) * $quantity,
						'points'          => ($product_query->row['points'] ? ($product_query->row['points'] + $option_points) * $quantity : 0),
						'tax_class_id'    => $product_query->row['tax_class_id'],
						'weight'          => ($product_query->row['weight'] + $option_weight) * $quantity,
						'weight_class_id' => $product_query->row['weight_class_id'],
						'length'          => $product_query->row['length'],
						'width'           => $product_query->row['width'],
						'height'          => $product_query->row['height'],
						'length_class_id' => $product_query->row['length_class_id']					
					);
				}
			}
		}
		
		return $this->data;
	  }
  	}

	public function deleteProduct($event_id, $iid) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "events_products WHERE event_id = '" . $event_id . "' AND item_id = '" . $iid . "'");
	}
	
	public function updateQty($id, $value) {
		$this->db->query("UPDATE " . DB_PREFIX . "events_products SET ordered_amount='" . $this->db->escape($value) . "' WHERE item_id = '" . $id. "'");
	}
	
	public function getOrderId($iid) {
	
		$query = $this->db->query("SELECT eventpid, order_id FROM " . DB_PREFIX . "order_product");
		$event_data = array();
		$order_id = array();
		foreach ($query->rows as $query2) {
			$evpid = explode(':', $query2['eventpid']);
			
			if (isset($evpid[1])) {
				$event_pid = unserialize(base64_decode($evpid[1]));
			
				$event_data[] = array(
				  'epid' => (isset($event_pid['eventpid']) ? $event_pid['eventpid'] : ''),
				  'oid'  => $query2['order_id']
				);
			}
		}
		
		foreach ($event_data as $return) {
			if (isset($return['epid']) && $return['epid'] == $iid) {
				$order_id[] = array(
				  'oid'  => $return['oid']
				);
			}
		}
	return $order_id; 
	}
	
	public function doUpgrade() {
		$test1 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'e_comment'");
		if (!$test1->num_rows) {
			$upgrade1 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `e_comment` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER `title`";
			$this->db->query($upgrade1);
		}
		
		$test2 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events_products` WHERE Field = 'p_comment'");
		if (!$test2->num_rows) {
			$upgrade2 = "ALTER TABLE `" . DB_PREFIX . "events_products` ADD `p_comment` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER `quantity`";
			$this->db->query($upgrade2);
		}
		
		$test3 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'notify_message'");
		if (!$test3->num_rows) {
			$upgrade3 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `notify_message` TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL AFTER `title`";
			$this->db->query($upgrade3);
		}
		
		$test4 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'enable_notes'");
		if (!$test4->num_rows) {
			$upgrade4 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `enable_notes` tinyint(1) NOT NULL AFTER `status`";
			$this->db->query($upgrade4);
		}
		
		$test5 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'enable_rsvp'");
		if (!$test5->num_rows) {
			$upgrade5 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `enable_rsvp` tinyint(1) NOT NULL AFTER `status`";
			$this->db->query($upgrade5);
		}
		
		$test6 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'photo'");
		if (!$test6->num_rows) {
			$upgrade6 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `photo` VARCHAR( 255 ) NOT NULL AFTER `enable_rsvp`";
			$this->db->query($upgrade6);
		}
		
		$test7 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'web_link'");
		if (!$test7->num_rows) {
			$upgrade7 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `web_link` VARCHAR( 255 ) NOT NULL AFTER `photo`";
			$this->db->query($upgrade7);
		}
		
		$test8 = $this->db->query("SHOW COLUMNS FROM `" . DB_PREFIX . "events` WHERE Field = 'address_id'");
		if (!$test8->num_rows) {
			$upgrade8 = "ALTER TABLE `" . DB_PREFIX . "events` ADD `address_id` INT( 11 ) NOT NULL AFTER `web_link`";
			$this->db->query($upgrade8);
		}
		
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "events_rsvp` (`rsvp_id` int(11) NOT NULL AUTO_INCREMENT, `event_id` int(11) NOT NULL, `name` varchar(64) NOT NULL, `short_note` text NOT NULL, `email` varchar(64) NOT NULL DEFAULT '', `guests` text NOT NULL, `access_key` varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL, PRIMARY KEY (`rsvp_id`), KEY `event_id` (`event_id`)) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
	}
	 
}
?>
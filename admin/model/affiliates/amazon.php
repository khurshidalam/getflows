<?php
class ModelAffiliatesAmazon extends Model {
	
	
	/**
         * Check if the xml received from Amazon is valid
         * 
         * @param mixed $response xml response to check
         * @return bool false if the xml is invalid
         * @return mixed the xml response if it is valid
         * @return exception if we could not connect to Amazon
         */
        private function verifyXmlResponse($response)
        {
            if ($response === False)
            {
                throw new Exception("Could not connect to Amazon");
            }
            else
            {
 
				if (isset($response->Items->Item->ItemAttributes->Title))
                {
                    return ($response);
                
                }elseif (isset($response->Cart))
                {
                    return ($response);
                }
				elseif(isset($response->Items->Request->Errors->Error->Message)){
				throw new Exception("<h1>There was an error but don't panic..</h1><p>".$response->Items->Request->Errors->Error->Message[0]."</p>");
				}
                else
                {
                    throw new Exception("<h1>We did not find any matches for your request</h1>");
                }
            }
        }
	
	/**
         * Query Amazon with the issued parameters
         * 
         * @param array $parameters parameters to query around
         * @return simpleXmlObject xml query response
         */
        private function queryAmazon($parameters, $country="com")
        {
            return $this->aws_signed_request($country, $parameters);
        }
	
	private function  aws_signed_request($region,$params)
	{

		global $wpdb;

		if($region == ""){ $region ="com"; }
	
		if($region == "it")
		{
		$host = "webservices.amazon.it"; // must be in small case
		}
		elseif($region == "es")
		{
		$host = "webservices.amazon.es"; // must be in small case
		}
		elseif($region == "cn")
		{
		$host = "webservices.amazon.cn"; // must be in small case
		}
		else
		{
		$host = "ecs.amazonaws.".$region; // must be in small case
		}
	
		$method = "GET";
    
		$uri = "/onca/xml";
		
		$affiliates=$this->getamazondetails();
    
    
		$params["Service"]          = "AWSECommerceService";
		$params["AWSAccessKeyId"]   = $affiliates[1]['affiliates_value'];
		$params["Timestamp"]        = gmdate("Y-m-d\TH:i:s\Z");
		$params["Version"]          = "2011-08-01";
		$params[ 'AssociateTag' ]   = $affiliates[0]['affiliates_value'];
	
	    /* The params need to be sorted by the key, as Amazon does this at
	      their end and then generates the hash of the same. If the params
	      are not in order then the generated hash will be different thus
	      failing the authetication process.
	    */
	    ksort($params);
	    
	    $canonicalized_query = array();
	
	    foreach ($params as $param=>$value)
	    {
		$param = str_replace("%7E", "~", rawurlencode($param));
		$value = str_replace("%7E", "~", rawurlencode($value));
		$canonicalized_query[] = $param."=".$value;
	    }
	    
	    $canonicalized_query = implode("&", $canonicalized_query);
	
	    $string_to_sign = $method."\n".$host."\n".$uri."\n".$canonicalized_query;
	    
	    /* calculate the signature using HMAC with SHA256 and base64-encoding.
	       The 'hash_hmac' function is only available from PHP 5 >= 5.1.2.
	    */
	    $signature = base64_encode(hash_hmac("sha256", $string_to_sign, $affiliates[2]['affiliates_value'], true));
	    
	    /* encode the signature for the request */
	    $signature = str_replace("%7E", "~", rawurlencode($signature));
	    
	    /* create request */
	    $request = "http://".$host.$uri."?".$canonicalized_query."&Signature=".$signature;
		
		//die($request);
	    /* I prefer using CURL */
	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL,$request);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	
	    $xml_response = curl_exec($ch);
	    
	    /* If cURL doesn't work for you, then use the 'file_get_contents'
	       function as given below.
	    */
	    //$xml_response = file_get_contents($request);
	    
	    if ($xml_response === False)
	    {
		return False;
	    }
	    else
	    {
		/* parse XML */
		$parsed_xml = @simplexml_load_string($xml_response);
		return ($parsed_xml === False) ? False : $parsed_xml;
	    }
	}
	
	/**
         * Return details of products searched by various types
         * 
         * @param string $search search term
         * @param string $category search category         
         * @param string $searchType type of search
         * @return mixed simpleXML object
         */
        public function searchProducts($search, $category, $values)
        { 
		
		if(strlen($values['minprice']) > 1){
		$values['minprice'].="00";
		}
		
		if(strlen($values['maxprice']) > 1){
		$values['maxprice'].="00";
		}
		
 
		if($category =="All"){
		$parameters = array(
		"Operation"  	=> "ItemSearch",
		"Keywords" 		=> $search,
		"SearchIndex"   => $category,
		"ResponseGroup" => "ItemAttributes,Offers,Images,EditorialReview,Reviews",
		"MinimumPrice" 	=> $values['minprice'],
		"MaximumPrice" 	=> $values['maxprice'],
		"ItemPage" 		=> $values['start_page'],
		"Brand"	 		=> $values['brand'],		
		"Condition" 	=> $values['condition'], 
		//"Sort" 			=> $values['Sort'],
		//"MerchantId"	=> $values['merchantid']
		);
		}else{
		$parameters = array(
		"Operation"  	=> "ItemSearch",
		"Title" 		=> $search,
		"SearchIndex"   => $category,
		"ResponseGroup" => "ItemAttributes,Offers,Images,EditorialReview,Reviews",
		"MinimumPrice" 	=> $values['minprice'],
		"MaximumPrice" 	=> $values['maxprice'],
		"ItemPage" 		=> $values['start_page'],
		"BrowseNode"	=> $values['node'],
		"Brand"	 		=> $values['brand'],
		"Condition" 	=> $values['condition'],
		"Sort" 			=> $values['Sort'],
		//"MerchantId"	=> $values['merchantid']
		);
		}
		
		//die(print_r($values));
		
		//die(print_r($parameters));
		$xml_response = $this->queryAmazon($parameters, $values['country']);
	    
		return $this->verifyXmlResponse($xml_response);
	}
	
	protected function getMerchantId($name){
		$query = $this->db->query("SELECT merchant_id FROM " . DB_PREFIX . "merchant WHERE UPPER( firstname ) = '". $name ."'");
		return $query->row["merchant_id"];
	}
	
	protected function getManufacturerId($name){
		$sql = "SELECT manufacturer_id FROM " . DB_PREFIX . "manufacturer WHERE name = '". addslashes($name) ."'";
		$query = $this->db->query($sql);
		if( ($query->num_rows) > 0) {
		return $query->row["manufacturer_id"];
		}
		else
		return false;
	}
	
	public function checkProd($manufacturer, $asin){
		$manID=$this->getManufacturerId($manufacturer);
		if($manID)
		{
		$sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE manufacturer_id='".$manID."' AND asin='".$asin."'";
		}
		else
		$sql = "SELECT product_id FROM ".DB_PREFIX."product WHERE asin='".$asin."'";
		$query = $this->db->query($sql);
		if( ($query->num_rows) > 0) {
			return $query->row["product_id"];
		} else {
			return 0;
		}
	}
	
	protected function checkProdMerchant($merId, $prodId) {
		$query = $this->db->query("SELECT id FROM ".DB_PREFIX."product_merchant WHERE merchant_id='".$merId."' AND product_id='".$prodId."'");
		if( ($query->num_rows) > 0) {
			return $query->row["id"];
		} else {
			return 0;
		}
	}
	
	public function getamazondetails()
	{
		$query = $this->db->query("select * from ".DB_PREFIX."affiliates where affiliates_name = 'Amazon'");
		if( ($query->num_rows) > 0) {
			return $query->rows;
		}
		else
		{
			return 0;
		}
	}
	
	public function saveAmazoneDataToDB($postdata)
	{//print_r($postdata['affiliate'][1]);exit;
		for($i=1;$i<=6;$i++)
		{ 
			$query = $this->db->query("UPDATE ".DB_PREFIX."affiliates set `affiliates_value` = '".$postdata['affiliate'][$i]."' where affiliates_id = '".$i."'");
		}
	}
	
	
	
	public function saveProd($manufacturer,$asin,$country,$cats)
	{
		$result=$this->getItemByAsin($asin,$country);
		$nwrec=0;
		$exrec=0;
		//die(var_dump($result->Items->Item));
		foreach($result->Items->Item as $dataAmazon){
		
			//Basic things of products.
			if(empty($dataAmazon->ItemAttributes->Title)){ $dataAmazon->ItemAttributes->Title="NA"; }
			if(empty($dataAmazon->ItemAttributes->SKU)){ $dataAmazon->ItemAttributes->SKU="NA"; }
			if(empty($dataAmazon->ItemAttributes->EAN)){ $dataAmazon->ItemAttributes->EAN=""; }
			if(empty($dataAmazon->ItemAttributes->UPC)){ $dataAmazon->ItemAttributes->UPC=""; }
			if(empty($dataAmazon->OfferSummary->TotalNew)){ $dataAmazon->OfferSummary->TotalNew="1"; }
			if(empty($dataAmazon->ItemAttributes->Manufacturer)){ $dataAmazon->ItemAttributes->Manufacturer="NA"; }
			if(empty($dataAmazon->ItemAttributes->ListPrice->Amount)){ $dataAmazon->ItemAttributes->ListPrice->Amount="NA"; }
			if(empty($dataAmazon->ItemAttributes->Model)){ $dataAmazon->ItemAttributes->Model="NA"; }
			if(empty($dataAmazon->EditorialReviews->EditorialReview->Content)){$dataAmazon->EditorialReviews->EditorialReview->Content=""; }
			if(empty($dataAmazon->DetailPageURL)){ $dataAmazon->DetailPageURL="NA"; }
							
			
			$title        = $this->db->escape(trim($dataAmazon->ItemAttributes->Title));
			$sku          = $this->db->escape(trim($dataAmazon->ItemAttributes->SKU));
			$ean          = $this->db->escape(trim($dataAmazon->ItemAttributes->EAN));
			$upc          = $this->db->escape(trim($dataAmazon->ItemAttributes->UPC));
			//$quantity     = $this->db->escape(trim($dataAmazon[$i]['ItemAttributes']['PackageQuantity']));
			$quantity     = $this->db->escape(trim($dataAmazon->OfferSummary->TotalNew)); // for New Products
			
			$manufacturer = $this->user->checkManu( $this->db->escape(trim($dataAmazon->ItemAttributes->Manufacturer)), 'Amazon');
			//$price        = $this->db->escape(trim($dataAmazon[$i]['ItemAttributes']['ListPrice']['Amount']));
			
			$model        = $this->db->escape(trim($dataAmazon->ItemAttributes->Model));
			
			if(isset($dataAmazon->CustomerReviews->IFrameURL)){
				$reviews      = urldecode($this->db->escape(trim($dataAmazon->CustomerReviews->IFrameURL)));
			}else{
				$reviews      = '';
			}
			
			$content       = stripslashes($this->db->escape(trim($dataAmazon->EditorialReviews->EditorialReview->Content)));
			$image         =urldecode($this->db->escape(trim($dataAmazon->LargeImage->URL)));
			$detailpageUrl = urldecode($dataAmazon->DetailPageURL);
			$data['status'] = 1;
			
			$desc = $content;
			
			if(isset($dataAmazon->ItemAttributes->Feature)){
			$features=$dataAmazon->ItemAttributes->Feature;
			if($features!=''){
				$desc .= '<br><br><strong>Features</strong><br><ul>';
				
				if(is_array($features)){
				  foreach($features as $feature){
				       $desc .= '<li>'.$feature.'</li>';
				  }
				}else{
				       $desc .= '<li>'.$features.'</li>';
				}
				
			       $desc .= '</ul>';
			}
			}
			
			$desc = $this->db->escape($desc);
			
			if(empty($dataAmazon->LargeImage->URL)){
				$filename1='';
			}else{
				if (!file_exists(DIR_IMAGE.'data/amazon/')) {
				    mkdir(DIR_IMAGE.'data/amazon/', 0777, true);
				}
				$filename = DIR_IMAGE.'data/amazon/'.basename($image);
				$filename1 = 'data/amazon/'.basename($image);
				file_put_contents($filename, file_get_contents($image));
			}
			
			
			
			//if(isset($dataAmazon[$i]['OfferSummary']['LowestNewPrice']['FormattedPrice'])){
			//	$formatted_price = $this->db->escape(trim($dataAmazon[$i]['OfferSummary']['LowestNewPrice']['FormattedPrice']));
			//	$pos=strpos($formatted_price,'$');
			//	if ($pos === false) {
			//		$price=0;
			//	}else{
			//		$currency=array('$');
			//                $price=str_replace($currency,'',$formatted_price);
			//	}
			//}else{
			//	$price=0;
			//}
			//if(isset($dataAmazon->OfferSummary->LowestNewPrice->Amount)){
			//	$mainprice=$dataAmazon->OfferSummary->LowestNewPrice->Amount;
			//	$price=(float)$mainprice/100;//number_format((float)$mainprice/100, 2);
			//}else{
			//	$price=0;
			//}
			
			$old_price 	= "";
			$price		= $dataAmazon->ItemAttributes->ListPrice->Amount;	 				
			$price=(float)$price/100;
				
			if($price == ""){
			$price = $dataAmazon->OfferSummary->LowestNewPrice->Amount;
			$price=(float)$price/100;
			}
			
			if(isset($dataAmazon->Offers->Offer->OfferListing->Price->Amount) && strlen($dataAmazon->Offers->Offer->OfferListing->Price->Amount) > 1){
				$old_price	= $price;
				$price		=  $dataAmazon->Offers->Offer->OfferListing->Price->Amount;
				$price=(float)$price/100;
			}
			
			
			//echo $old_price.' :: '.$price;die;
			//echo $i.$title.$price.$ean.$manufacturer.'<br>';
			//echo 'category ID: '.$data["categoryId"];
			//Attributes of products.
			$atrbDumps = $dataAmazon->ItemAttributes;
			
			foreach($atrbDumps as $key=>$atrDmp){
				if(!is_array($atrDmp)){
					$atrbs[$key]        = $this->db->escape($atrDmp);
				}
			}
			$afId = $this->getMerchantId(strtoupper('Amazon'));
			//$prodExistenceChecker = $this->checkProd($manufacturer, $sku);
			
				//$this->user->prodInsAmazon($model, $title, $sku,$ean,$upc, $quantity, $manufacturer, $price, $filename1, $detailpageUrl, $afId);
				$this->user->prodInsAmazon($model, $title, $sku,$ean,$upc,$asin,$quantity, $manufacturer, $old_price, $price, $filename1, $detailpageUrl);
				$lastInsId = $this->db->getLastId();
				
				//additional images
				if(isset($dataAmazon->ImageSets->ImageSet)){	
				    $i=1;
				    foreach($dataAmazon->ImageSets->ImageSet as $img)
				    {
				    $images = urldecode($this->db->escape(trim($img->LargeImage->URL)));
				    $relimage=$this->db->escape(html_entity_decode(urldecode($images), ENT_QUOTES, 'UTF-8'));
				    $imgfilename = 'data/amazon/'.$lastInsId.'/'.basename($images);
				    //echo $imgfilename;
				    //exit;
				    if (!file_exists(DIR_IMAGE.'data/amazon/'.$lastInsId.'/')) {
					mkdir(DIR_IMAGE.'data/amazon/'.$lastInsId.'/', 0777, true);
				    }
				    $relfilename = DIR_IMAGE.'data/amazon/'.$lastInsId.'/'.basename($images);
				    file_put_contents($relfilename, file_get_contents($images));
							
				    $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$lastInsId . "', image = '" . $imgfilename . "', sort_order = '" . (int)$i . "'");
				    $i++;
				    }
				}
		
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_merchant SET product_id = '" . (int)$lastInsId . "', `review_url`='".$reviews."', merchant_id = '" . $afId . "', price = '". (float)$price ."', date_added = NOW(), target_url = '" . $detailpageUrl . "', status = '" . (int)$data['status'] . "'");
				$catIds    = explode(',',$cats);
				
				$storeId  = '0';
				$lanId    = '1';
				foreach( $atrbs as $key=>$atrb){
				    $atrbId = $this->user->atributeCheck($key);
				    $this->user->prodAttribute($lastInsId, $atrbId, $atrb);
				}
				
				$this->user->prodDescIns($lastInsId, $lanId, $title, $desc);
				//$invalidletters1=array(" ","/");
				//$invalidletters2=array(",","'","*",".","!","%");
				$newtitle=substr($title,0,35);
				$newtitle=getUrlFriendlyString($newtitle);
				//$newtitle=str_replace($invalidletters1,"-",$newtitle);
				//$newtitle=str_replace($invalidletters2,"",$newtitle);
				$newtitle=rtrim($newtitle,'-');
				$this->user->prodUrlSeo($lastInsId,strtolower($newtitle));
				$this->user->prodToStore($lastInsId, $storeId);
				$this->user->prodToCategory($lastInsId, $catIds);
				$this->user->prodReward($lastInsId);
				if($this->db->countAffected() > 0){
				    $msz = "saved";
				    $nwrec++;
			
				}else{
				    $msz = "error";
				}
			
			
	        
		}
		return $msz;
	}
	
	/**
         * Return details of a product searched by ASIN
         * 
         * @param int $asin_code ASIN code of the product to search
         * @return mixed simpleXML object
         */
        private function getItemByAsin($asin_code, $country)
        {
		 
            $parameters = array("Operation"     => "ItemLookup",
                                "ItemId"        => $asin_code,
                                "ResponseGroup" => "ItemAttributes,Offers,Images,EditorialReview,Reviews");
                                
            $xml_response = $this->queryAmazon($parameters, $country);
            
            return $this->verifyXmlResponse($xml_response);
        }
	
	public static function getUrlFriendlyString($str)
	{
	// convert spaces to '-', remove characters that are not alphanumeric
	// or a '-', combine multiple dashes (i.e., '---') into one dash '-'.
	$str = ereg_replace("[-]+", "-", ereg_replace("[^a-z0-9-]", "",
	    strtolower( str_replace(" ", "-", $str) ) ) );
	return $str;
	}
	
	public function printItemByAsin($asin_code, $country)
        {
		 
            $parameters = array("Operation"     => "ItemLookup",
                                "ItemId"        => $asin_code,
                                "ResponseGroup" => "ItemAttributes,Offers,Images,EditorialReview,Reviews");
                                
            $xml_response = $this->queryAmazon($parameters, $country);
            
            return $this->verifyXmlResponse($xml_response);
        }
    
	
}
?>
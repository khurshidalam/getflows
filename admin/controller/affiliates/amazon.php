<?php    
class ControllerAffiliatesAmazon extends Controller { 
	private $error = array();
  
  	public function index() {
		$this->language->load('affiliates/amazon');
		 
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('affiliates/amazon');
		
		$this->getForm();
  	}

	
	///////
	
	public function saveamazondetails()
	{
		$this->language->load('affiliates/amazon');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('affiliates/amazon');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && !$this->validateAmazon())
		{
			$temData=$this->model_affiliates_amazon->saveAmazoneDataToDB($this->request->post);
			if($temData){
				$this->session->data['amazonwarning'] = 'Error in saving connection setting.';
			}else{
				$this->session->data['amazonsuccess'] = 'Connection setting updated successfully';
			}
			
			$this->redirect($this->url->link('affiliates/amazon', 'token=' . $this->session->data['token'] , 'SSL'));
		}
			$this->getForm();
	}
	
	public function fetchdata() {
		$this->language->load('affiliates/amazon');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('affiliates/amazon');
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
		$count=0;
		$cats="";
		
		foreach($_POST['cat'] as $cat){			
		$cats .= $cat.",";
		}
		$cats = rtrim($cats,',');
		$_POST['amazon']['keywords'] = str_replace('*','"',$_POST['amazon']['keywords']);
		try
		{
		$result=$this->model_affiliates_amazon->searchProducts($_POST['amazon']['keywords'],$_POST['amazon']['keyword_cat'],$_POST['amazon']);
		}
		catch(Exception $e)
		{

			echo $e->getMessage();
			exit();		
		}
		if(!$result )
		{
			$this->session->data['warning'] = 'No result found. Please try again.';
			$this->data['warning'] = $this->session->data['warning'];
		}
		else
		{
			//$this->session->data['success'] = 'Successfull';
			$this->data['keyword'] = $_POST['amazon']['keywords'];
			$this->data['start_page'] = $_POST['amazon']['start_page'];
			$this->data['keyword_cat'] = $_POST['amazon']['keyword_cat'];
			$this->data['cate'] = $_POST['cat'];
			$this->data['postdata'] = $_POST;
			$this->data['postamazon'] = $_POST['amazon'];
			$this->data['ajaxaction'] = $this->url->link('affiliates/amazon/checkdb', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['cats'] = $cats;
			$this->data['result']=$result;
			$this->template = 'affiliates/amazon_result.tpl';
			$this->children = array(
				'common/amazonlistheader',
				'common/footer'
			);
						
			$this->response->setOutput($this->render());
			
		}
			
		}
  	}
	
	/////
     
  	public function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');				
		$this->data['entry_keywords'] = $this->language->get('entry_keywords');
		$this->data['entry_category'] = $this->language->get('entry_category');
		$this->data['entry_amazoncategory'] = $this->language->get('entry_amazoncategory');
		$this->data['error_keywords'] = $this->language->get('error_keywords');
		//$this->data['product_category'] = $this->getProductCategories('product_category');
		$this->data['affiliates']=$this->model_affiliates_amazon->getamazondetails();
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->session->data['warning'])) {
			$this->data['warning'] = $this->session->data['warning'];

			unset($this->session->data['warning']);
		} else {
			$this->data['warning'] = '';
		}
		
		if (isset($this->session->data['amazonsuccess'])) {
			$this->data['amazonsuccess'] = $this->session->data['amazonsuccess'];

			unset($this->session->data['amazonsuccess']);
		} 
		
		if (isset($this->session->data['amazonwarning'])) {
			$this->data['amazonwarning'] = $this->session->data['amazonwarning'];

			unset($this->session->data['amazonwarning']);
		} 
 
		$this->data['button_feed'] = $this->language->get('button_feed');	
 		
		// Categories
		$this->load->model('catalog/category');
		$this->data['categories']=$this->model_catalog_category->getAllCategories();
		
		if (isset($this->error['amazon'])) {
			$this->data['amazon_saving_error'] = $this->error['amazon'];
		}

 		if (isset($this->error['keywords'])) {
			$this->data['error_keywords'] = $this->error['keywords'];
		} else {
			$this->data['error_keywords'] = '';
		}
		
		if (isset($this->error['category'])) {
			$this->data['error_category'] = $this->error['category'];
		} else {
			$this->data['error_category'] = '';
		}

	  		$this->data['breadcrumbs'] = array();
	
	   		$this->data['breadcrumbs'][] = array(
	       		'text'      => $this->language->get('text_home'),
				'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
	      		'separator' => false
	   		);
	
	   		$this->data['breadcrumbs'][] = array(
	       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('affiliates/amazon', 'token=' . $this->session->data['token'], 'SSL'),
	      		'separator' => ' :: '
	   		);
			
			$this->data['action'] = $this->url->link('affiliates/amazon/fetchdata', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['amazonformaction'] = $this->url->link('affiliates/amazon/saveamazondetails', 'token=' . $this->session->data['token'], 'SSL');
			$this->data['iframeaction'] = $this->url->link('affiliates/amazon/fetchdata', 'token=' . $this->session->data['token'], 'SSL');
			
			
	    	$this->data['cancel'] = $this->url->link('affiliates/amazon', 'token=' . $this->session->data['token'], 'SSL');
	
		$this->data['token'] = $this->session->data['token'];

						
	    	if (isset($this->request->post['keywords'])) {
			$this->data['keywords'] = $this->request->post['keywords'];
		}
		else {
	      		$this->data['keywords'] = '';
	    	}
		
		if (isset($this->request->post['category'])) {
			$this->data['category'] = $this->request->post['category'];
		}
		else {
	      		$this->data['category'] = '';
	    	}
																													
	    	if (isset($this->request->post['status'])) {
	      		$this->data['status'] = $this->request->post['status'];
	    	} elseif (!empty($affiliate_info)) { 
				$this->data['status'] = $affiliate_info['status'];
			} else {
	      		$this->data['status'] = 1;
	    	}

    	
		
		if (isset($this->request->post['confirm'])) { 
	    		$this->data['confirm'] = $this->request->post['confirm'];
		} else {
				$this->data['confirm'] = '';
		}

		$this->template = 'affiliates/amazon.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
					
		$this->response->setOutput($this->render());
	}

  	protected function validateForm() {
		
		//if ((utf8_strlen($this->request->post['keywords']) < 1) || (utf8_strlen($this->request->post['keywords']) > 32)){
		//	$this->error['keywords'] = $this->language->get('error_keywords');
		//}
		
		//if ((utf8_strlen($this->request->post['category']) < 1) || (utf8_strlen($this->request->post['category']) > 32)) {
		//	$this->error['category'] = $this->language->get('error_category');
		//}
		if (trim($this->request->post['amazon']['keywords'])=='') {
			$this->error['keywords'] = $this->language->get('error_keywords');
			die('<h1>You forgot to enter a search keyword.</h1>');
		}
		
		if (trim($this->request->post['amazon']['keyword_cat'])=='') {
			$this->error['category'] = $this->language->get('error_category');
			die($this->language->get('error_category'));
		}
		if(!isset($_POST['cat']) || (!is_array($_POST['cat']) || $_POST['cat'][0] =="0"))
		{
			die("<h1> You didnt select any category to save the products</h1>");
		}
		
		if (!$this->error) {
	  		return true;
		} else {
	  		return false;
		}
	}
	
	protected function validateAmazon()
	{
		$this->error['amazon'] = '';
		if (trim($this->request->post['affiliate'][1])=='') {
			$this->error['amazon'] .= 'Please enter Amazon Affiliate ID';
		}
		if (trim($this->request->post['affiliate'][2])=='') {
			$this->error['amazon'] .= '<br>Please enter Amazon Access Key ID';
		}
		if (trim($this->request->post['affiliate'][3])=='') {
			$this->error['amazon'] .= '<br>Please enter Amazon Secret Access Key ID';
		}
		if ($this->request->post['affiliate'][6]=='') {
			$this->error['amazon'] .= '<br>Please select Amazon Updater Country';
		}
		if($this->error['amazon'] != '')
		return $this->error['amazon'];
		else
		return false;
	}

  	protected function validateDelete()
	{
	if (!$this->user->hasPermission('modify', 'sale/affiliate')) {
		$this->error['warning'] = $this->language->get('error_permission');
	}	
	
	if (!$this->error) {
		return true;
	} else {
		return false;
	}  
  	}
	
	public function checkdb()
	{
		$asin=$_POST['asin'];
		$country=$_POST['country'];
		$cats=$_POST['cats'];
		$manufacturer=$_POST['manufacturer'];
		//$asin='0621033832996';
		//$cats='105,106';
		//$country='com';
		//$manufacturer= 'niceEshop';
		$this->load->model('affiliates/amazon');
		$productexist=$this->model_affiliates_amazon->checkProd($manufacturer,$asin);
		//echo $productexist;
		if($productexist)
		{
			echo 'exist';
		}
		else
		{
			$saveproduct=$this->model_affiliates_amazon->saveProd($manufacturer,$asin,$country,$cats);
			echo $saveproduct;
		}
	}
	
	public function printItem()
	{
		$sql="Select * from oc_product where asin !='' AND product_id = 254";
		$query=$this->db->query($sql);
		echo $query->num_rows;
		$country='com';
		$this->load->model('affiliates/amazon');
		foreach($query->rows as $row)
		{
		$asin=$row['asin'];
		$lastInsId = $row['product_id'];
	
		$productexist=$this->model_affiliates_amazon->printItemByAsin($asin,$country);
		foreach($productexist->Items->Item as $dataAmazon){
			
			//function for price update
			
			//$old_price 	= 0.00;
			//$price		= $dataAmazon->ItemAttributes->ListPrice->Amount;	 				
			//$price=(float)$price/100;
			//	
			//if($price == ""){
			//$price = $dataAmazon->OfferSummary->LowestNewPrice->Amount;
			//$price=(float)$price/100;
			//}
			//
			//if(isset($dataAmazon->Offers->Offer->OfferListing->Price->Amount) && strlen($dataAmazon->Offers->Offer->OfferListing->Price->Amount) > 1){
			//	$old_price	= $price;
			//	$price		=  $dataAmazon->Offers->Offer->OfferListing->Price->Amount;
			//	$price=(float)$price/100;
			//}
			//if($price)
			//{
			//$sql1="Update oc_product set price = ".$price." , old_price = ".$old_price." where asin = '".$asin."'";
			//
			//$query1=$this->db->query($sql1);
			//echo $asin.' :: '.$old_price.' :: '.$price.'<br>';
			//}
			
			// function for image update
			//if(isset($dataAmazon->ImageSets->ImageSet)){	
			//	    $i=1;
			//	    foreach($dataAmazon->ImageSets->ImageSet as $img)
			//	    {
			//	    $images = urldecode($this->db->escape(trim($img->LargeImage->URL)));
			//	    $relimage=$this->db->escape(html_entity_decode(urldecode($images), ENT_QUOTES, 'UTF-8'));
			//	    $imgfilename = 'data/amazon/'.$lastInsId.'/'.basename($images);
			//	    //echo $imgfilename;
			//	    //exit;
			//	    if (!file_exists(DIR_IMAGE.'data/amazon/'.$lastInsId.'/')) {
			//		mkdir(DIR_IMAGE.'data/amazon/'.$lastInsId.'/', 0777, true);
			//		$relfilename = DIR_IMAGE.'data/amazon/'.$lastInsId.'/'.basename($images);
			//	    file_put_contents($relfilename, file_get_contents($images));
			//				
			//	    $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$lastInsId . "', image = '" . $imgfilename . "', sort_order = '" . (int)$i . "'");
			//	    }
			//	    
			//	    $i++;
			//	    }
			//	}
		}
		}
	}
	
}
?>
<?php
// Version 1.21
class ControllerSaleEvents extends Controller {
	private $error = array();

	public function index() {
	
		$this->load->language('sale/events');
		$this->load->model('sale/events');

		$this->getList();
	}

	public function update() {
	
		$this->load->language('sale/events');
		$this->load->model('sale/events');
	
		$this->document->setTitle($this->language->get('heading_title_edit_event'));

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validateForm())) {
			$this->model_sale_events->editEvent($this->request->get['event_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
	
		$this->load->language('sale/events');
		$this->load->model('sale/events');

		$this->document->setTitle($this->language->get('heading_title'));

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $event_id) {
				$this->model_sale_events->deleteEvent($event_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();
	}
	
	public function updateQty() {

		$this->language->load('sale/events');
		$this->load->model('sale/events');

		if (isset($this->request->post['id'])) {
			$this->model_sale_events->updateQty($this->request->post['id'], $this->request->post['value']);
		}

	}
	
	public function deleteProduct() {

		$this->language->load('sale/events');
		$this->load->model('sale/events');
		
		
		if (isset($this->request->get['event_id']) && $this->validateDelete()) {
			$this->model_sale_events->deleteProduct($this->request->get['event_id'], $this->request->get['iid']);

			$this->session->data['success'] = $this->language->get('text_remove');

			$this->redirect($this->url->link('sale/events/view', 'token=' . $this->session->data['token'] . '&event_id=' . $this->request->get['event_id'], 'SSL'));
		}

		$this->view();
	}
	
	public function view() {
	
		$this->load->language('sale/events');
		$this->load->model('sale/events');
		$this->document->setTitle($this->language->get('heading_title_view_event'));

		$this->getView();
	}

	private function getList() {
		$this->load->language('sale/events');
		$this->load->model('sale/events');

		$this->data['heading_events_list'] = $this->language->get('heading_title_events_list');
		$this->document->setTitle($this->language->get('heading_title_events_list'));
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_event_id'] = $this->language->get('column_event_id');
		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_customer_id'] = $this->language->get('column_customer_id');
		$this->data['column_title'] = $this->language->get('column_title');
		$this->data['column_email'] = $this->language->get('column_email');
		$this->data['column_type'] = $this->language->get('column_type');
		$this->data['column_start_date'] = $this->language->get('column_start_date');
		$this->data['column_end_date'] = $this->language->get('column_end_date');
		$this->data['column_status'] = $this->language->get('column_status');
		$this->data['column_action'] = $this->language->get('column_action');		

		$this->data['button_delete'] = $this->language->get('button_delete');
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->request->get['filter_event_id'])) {
			$filter_event_id = $this->request->get['filter_event_id'];
		} else {
			$filter_event_id = null;
		}

		if (isset($this->request->get['filter_customer_id'])) {
			$filter_customer_id = $this->request->get['filter_customer_id'];
		} else {
			$filter_customer_id = null;
		}

		if (isset($this->request->get['filter_title'])) {
			$filter_title = $this->request->get['filter_title'];
		} else {
			$filter_title = null;
		}
		
		if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}
		
		if (isset($this->request->get['filter_email'])) {
			$filter_email = $this->request->get['filter_email'];
		} else {
			$filter_email = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'event_id';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}
				
		$url = '';

		if (isset($this->request->get['filter_event_id'])) {
			$url .= '&filter_event_id=' . $this->request->get['filter_event_id'];
		}
		
		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}
											
		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . $this->request->get['filter_title'];
		}
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
					
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_events_list'),
			'href'      => $this->url->link('sale/events', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);
		
		$this->data['delete'] = $this->url->link('sale/events/delete', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['events'] = array();
		
		$data = array(
			'filter_event_id'        => $filter_event_id,
			'filter_customer_id'	 => $filter_customer_id,
			'filter_title' 			 => $filter_title,
			'filter_name'            => $filter_name,
			'filter_email'      	 => $filter_email,
			'sort'                   => $sort,
			'order'                  => $order,
			'start'                  => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit'                  => $this->config->get('config_admin_limit')
		);

		$events_total = $this->model_sale_events->getTotalEvents($data);

		$results = $this->model_sale_events->getEvents($data);

    	foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('sale/events/update', 'token=' . $this->session->data['token'] . '&event_id=' . $result['event_id'], 'SSL')
			);
			
			$action[] = array(
				'text' => $this->language->get('text_view_products'),
				'href' => $this->url->link('sale/events/view', 'token=' . $this->session->data['token'] . '&event_id=' . $result['event_id'], 'SSL')
			);

			$this->data['events'][] = array(
				'event_id'     => $result['event_id'],
				'customer_id'  => $this->model_sale_events->getCustomerName($result['customer_id']),
				'title'		   => $result['title'],
				'name'		   => $result['name'],
				'email'		   => $result['email'],
				'type'       => ($result['type'] ? $this->language->get('text_private') : $this->language->get('text_public')),
				'start_date'   => date($this->language->get('date_format_short'), strtotime($result['start_date'])),
				'end_date'	   => date($this->language->get('date_format_short'), strtotime($result['end_date'])),
				'status'       => ($result['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled')),
				'selected'     => isset($this->request->post['selected']) && in_array($result['event_id'], $this->request->post['selected']),
				'action'       => $action
			);
		}
		
		$url = '';

		if (isset($this->request->get['filter_event_id'])) {
			$url .= '&filter_event_id=' . $this->request->get['filter_event_id'];
		}
		
		if (isset($this->request->get['filter_customer_id'])) {
			$url .= '&filter_customer_id=' . urlencode(html_entity_decode($this->request->get['filter_customer_id'], ENT_QUOTES, 'UTF-8'));
		}
											
		if (isset($this->request->get['filter_title'])) {
			$url .= '&filter_title=' . $this->request->get['filter_title'];
		}
		
		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . $this->request->get['filter_name'];
		}
					
		if (isset($this->request->get['filter_email'])) {
			$url .= '&filter_email=' . $this->request->get['filter_email'];
		}
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
		
		if ($order == 'DESC') {
			$url .= '&order=ASC';
		} else {
			$url .= '&order=DESC';
		}
		
		$this->data['sort_event_id'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=event_id' . $url, 'SSL');
		$this->data['sort_customer_id'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=customer_id' . $url, 'SSL');
		$this->data['sort_title'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=title' . $url, 'SSL');
		$this->data['sort_name'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=name' . $url, 'SSL');
		$this->data['sort_email'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=email' . $url, 'SSL');
		$this->data['sort_type'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=type' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=status' . $url, 'SSL');
		$this->data['sort_start_date'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=start_date' . $url, 'SSL');
		$this->data['sort_end_date'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . '&sort=end_date' . $url, 'SSL');
		
		$this->data['filter_event_id'] = $filter_event_id;
		$this->data['filter_customer_id'] = $filter_customer_id;
		$this->data['filter_name'] = $filter_name;
		$this->data['filter_title'] = $filter_title;
		$this->data['filter_email'] = $filter_email;
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		
		$pagination = new Pagination();
		$pagination->total = $events_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('sale/events', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->template = 'sale/events_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	private function getForm() {
		$this->load->language('sale/events');
		$this->load->model('sale/events');

		$this->data['heading_title'] = $this->language->get('heading_title_edit_event');

    	$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');

		$this->data['entry_event_id'] = $this->language->get('entry_event_id');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_customer_id'] = $this->language->get('entry_customer_id');
		$this->data['entry_customer'] = $this->language->get('entry_customer');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_start_date'] = $this->language->get('entry_start_date');
		$this->data['entry_end_date'] = $this->language->get('entry_end_date');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_action'] = $this->language->get('entry_action');		

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');

		$this->data['token'] = $this->session->data['token'];
		
		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_events_list'),
			'href'      => $this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_edit_event'),
			'href'      => $this->url->link('sale/events/update', 'token=' . $this->session->data['token'] . '&event_id=' . $this->request->get['event_id'], 'SSL'),
			'separator' => ' :: '
		);
		
		$this->data['action'] = $this->url->link('sale/events/update', 'token=' . $this->session->data['token'] . '&event_id=' . $this->request->get['event_id'], 'SSL');

		$this->data['cancel'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL');

		if ((isset($this->request->get['event_id'])) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$event_info = $this->model_sale_events->getEventDetails($this->request->get['event_id']);
		}

		if (!empty($event_info)) {
			$this->data['event_id'] = $event_info['event_id'];
		} else {
			$this->data['event_id'] = '';
		}
		
		if (isset($this->request->post['customer_id'])) {
			$this->data['customer_id'] = $this->request->post['customer_id'];
		} elseif (!empty($event_info)) {
			$this->data['customer_id'] = $event_info['customer_id'];
		} else {
			$this->data['customer_id'] = '';
		}

		if (!empty($event_info)) {
			$this->data['customer'] = $event_info['customer'];
		} else {
			$this->data['customer'] = '';
		}

		if (!empty($event_info)) {
			$this->data['title'] = $event_info['title'];
		} else {
			$this->data['title'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['name'] = $event_info['name'];
		} else {
			$this->data['name'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['email'] = $event_info['email'];
		} else {
			$this->data['email'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['type'] = ($event_info['type'] ? $this->language->get('text_private') : $this->language->get('text_public'));
		} else {
			$this->data['type'] = '';
		}

		if (!empty($event_info)) {
			$this->data['start_date'] = date($this->language->get('date_format_short'), strtotime($event_info['start_date']));
		} else {
			$this->data['start_date'] = '';
		}

		if (!empty($event_info)) {
			$this->data['end_date'] = date($this->language->get('date_format_short'), strtotime($event_info['end_date']));
		} else {
			$this->data['end_date'] = '';
		}

		if (!empty($event_info)) {
			$this->data['status'] = ($event_info['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'));
		} else {
			$this->data['status'] = '';
		}

		$this->template = 'sale/events_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}
	
	private function getView() {
		$this->load->language('sale/events');
		$this->load->model('sale/events');

		$this->data['heading_title'] = $this->language->get('heading_title_view_event');

    	$this->data['text_enabled'] = $this->language->get('text_enabled');
    	$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['entry_event_id'] = $this->language->get('entry_event_id');
		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_customer_id'] = $this->language->get('entry_customer_id');
		$this->data['entry_customer'] = $this->language->get('entry_customer');
		$this->data['entry_title'] = $this->language->get('entry_title');
		$this->data['entry_email'] = $this->language->get('entry_email');
		$this->data['entry_type'] = $this->language->get('entry_type');
		$this->data['entry_start_date'] = $this->language->get('entry_start_date');
		$this->data['entry_end_date'] = $this->language->get('entry_end_date');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_action'] = $this->language->get('entry_action');		

		$this->data['column_action'] = $this->language->get('column_action');
		$this->data['column_image'] = $this->language->get('column_image');
		$this->data['column_buy_qty'] = $this->language->get('column_buy_qty');
		$this->data['column_qty_bought'] = $this->language->get('column_qty_bought');
		$this->data['column_qty_rqd'] = $this->language->get('column_qty_rqd');
   		$this->data['column_name'] = $this->language->get('column_name');
   		$this->data['column_model'] = $this->language->get('column_model');
   		$this->data['column_quantity'] = $this->language->get('column_quantity');
		$this->data['column_price'] = $this->language->get('column_price');
   		$this->data['column_total'] = $this->language->get('column_total');
   		$this->data['column_order'] = $this->language->get('column_order');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');

		$this->data['token'] = $this->session->data['token'];
		$this->data['delete'] = $this->language->get('delete_link');

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$this->data['store_url'] = HTTPS_SERVER;
		} else {
			$this->data['store_url'] = HTTP_SERVER;
		}

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_events_list'),
			'href'      => $this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title_view_event'),
			'href'      => $this->url->link('sale/events/view', 'token=' . $this->session->data['token'] . '&event_id=' . $this->request->get['event_id'], 'SSL'),
			'separator' => ' :: '
		);
		
		$this->data['cancel'] = $this->url->link('sale/events', 'token=' . $this->session->data['token'], 'SSL');

		if ((isset($this->request->get['event_id'])) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$event_info = $this->model_sale_events->getEventDetails($this->request->get['event_id']);
		}

		if (!empty($event_info)) {
			$this->data['event_id'] = $event_info['event_id'];
		} else {
			$this->data['event_id'] = '';
		}
		
		if (!empty($event_info)) {
			$this->data['customer_id'] = $event_info['customer_id'];
		} else {
			$this->data['customer_id'] = '';
		}

		if (!empty($event_info)) {
			$this->data['customer'] = $event_info['customer'];
		} else {
			$this->data['customer'] = '';
		}

		if (!empty($event_info)) {
			$this->data['title'] = $event_info['title'];
		} else {
			$this->data['title'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['name'] = $event_info['name'];
		} else {
			$this->data['name'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['email'] = $event_info['email'];
		} else {
			$this->data['email'] = '';
		}
			
		if (!empty($event_info)) {
			$this->data['type'] = ($event_info['type'] ? $this->language->get('text_private') : $this->language->get('text_public'));
		} else {
			$this->data['type'] = '';
		}

		if (!empty($event_info)) {
			$this->data['start_date'] = date($this->language->get('date_format_short'), strtotime($event_info['start_date']));
		} else {
			$this->data['start_date'] = '';
		}

		if (!empty($event_info)) {
			$this->data['end_date'] = date($this->language->get('date_format_short'), strtotime($event_info['end_date']));
		} else {
			$this->data['end_date'] = '';
		}

		if (!empty($event_info)) {
			$this->data['status'] = ($event_info['status'] ? $this->language->get('text_enabled') : $this->language->get('text_disabled'));
		} else {
			$this->data['status'] = '';
		}
		
		$this->load->model('tool/image');
		
		$products = $this->model_sale_events->getProductsForEventsView($this->model_sale_events->getEventsViewProducts($this->request->get['event_id']), $this->request->get['event_id']);
			
			if ($products) {
	      		foreach ($products as $product) {
				
					$product_total = 0;
						
					foreach ($products as $product_2) {
						if ($product_2['product_id'] == $product['product_id']) {
							$product_total += $product_2['quantity'];
						}
					}			
					
					if ($product['image']) {
						$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
					} else {
						$image = '';
					}
					
					$bought_data = array();				
	
					foreach ($product['bought'] as $buyed) {
						
						$bought_data[] = array(
							'bought'  => $buyed['bought_qty']
						);
	   	     		}
						
					$option_data = array();			

	        		foreach ($product['option'] as $option) {
						if ($option['type'] != 'file') {
							$value = $option['option_value'];	
						} else {
							$filename = $this->decrypt($option['option_value']);
							
							$value = utf8_substr($filename, 0, utf8_strrpos($filename, '.'));
						}
						
						$option_data[] = array(
							'name'  => $option['name'],
							'value' => (utf8_strlen($value) > 20 ? utf8_substr($value, 0, 20) . '..' : $value)
						);
   	     		}
					
				$total = $product['price'] * $product['quantity'];
					
	   	  		$this->data['products'][] = array(
					'id'	   => $product['product_id'],
					'iid'	   => $product['iid'],
					'oid' 	   => $this->model_sale_events->getOrderId($product['iid']),
					'key'      => $product['key'],
					'thumb'    => $image,
					'name'     => $product['name'],
   	    			'model'    => $product['model'],
					'bought'   => $buyed['bought_qty'],
					'fred'	   => $product['quantity'] - $buyed['bought_qty'],
					'option'   => $option_data,
					'quantity' => $product['quantity'],
					'stock'    => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'price'    => $product['price'],
					'total'    => $total,
					'href'     => $this->url->link('product/product', 'token=' . $this->session->data['token'] . '&product_id=' . $product['product_id'], 'SSL'),
					'action'   => $this->url->link('sale/events/deleteProduct', 'token=' . $this->session->data['token'] . '&product=' . $product['key'] . '&event_id=' . $this->request->get['event_id'] . '&iid=' . $product['iid'], 'SSL')
				);
			}
		}

		$this->template = 'sale/events_view.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	public function upgrade() {
	
		$this->load->model('sale/events');
		$this->load->language('sale/events');

		$this->model_sale_events->doUpgrade();
		
		$this->getList();
		
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'sale/events')) {
			$this->error['warning'] = $this->language->get('error_permission3');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function validateForm() {
		if (!$this->user->hasPermission('modify', 'sale/events')) {
			$this->error['warning'] = $this->language->get('error_permission1');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	private function validateDelete() {
		if (!$this->user->hasPermission('modify', 'sale/events')) {
			$this->error['warning'] = $this->language->get('error_permission2');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	private function decrypt($value) {

		$key = hash('sha256', $this->config->get('config_encryption'), true);
		$iv = mcrypt_create_iv(32, MCRYPT_RAND);
	
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, base64_decode(strtr($value, '-_,', '+/=')), MCRYPT_MODE_ECB, $iv));
	}
}
?>
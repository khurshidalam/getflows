<?php echo $header; ?>
<style type="text/css">
#overlay{ visibility: hidden; }
</style>
<div id="content">
  <div class="wrapper-in">  
     <div class="breadcamp-text">
      <ul>
        <?php $coun = '1'; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if(isset($breadcrumb['separator'])){ if($coun > 1){echo "<li><a>/</a></li>";}} ?>
        <li><?php if($coun == 1){ ?>
            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php }else{ echo $breadcrumb['text']; } ?>
            </li>
        <?php $coun++; } ?>
      </ul>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
   <?php if ($warning) { ?>
  <div class="warning"><?php echo $warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit(); $('#overlay').css('visibility', 'visible');" class="button"><?php echo $button_feed; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span><?php echo $entry_category; ?></td>
              <td>
	      <select name="aff_category">
		<option value="">--Select Category--</option>
		<?php
		if($getMappedCategory){
		  foreach($getMappedCategory as $getMappedCat){
		    $name='';
		    $category_id=0;
		    if($categories){
		      foreach ($categories as $category) {
			if($getMappedCat['main_catid']==$category['category_id']){
			  $name=$category['name'];
			  $category_id=$category['category_id'];
			}
		      }
		     }
		    
		    ?>
		<option value="<?php echo $getMappedCat['aff_catid'].','.$category_id; ?>" ><?php echo $name; ?></option>
		<?php } }?>
	      </select>
	      <?php //echo $becomeCategories; ?>
	      
	      <div id='overlay' style='float: right; margin-left: 5%;'>
		<img src='../image/loading.gif' /> Loading...</div>
	      </td>
            </tr>             
          </table>
	</div>
      </form>
    </div>
  </div>
  </div>
</div>
<?php echo $footer; ?>

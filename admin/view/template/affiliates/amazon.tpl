<?php echo $header; ?>
<script>
  $(function()
    {
      var affi1=$('#affiliate1').val();
      var affi2=$('#affiliate2').val();
      var affi3=$('#affiliate3').val();
      if ((affi1 == '') || (affi2 == '') || (affi3 == '')) {
	$('#error_visible').css('display','block');
	$('#startbutton').attr('onclick','javascript:void(0)');
      }
    });
</script>
<div id="content">
  <div class="wrapper-in">  
<!--  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>-->
  <div class="breadcrumb">
        <?php $coun = '1'; foreach ($breadcrumbs as $breadcrumb) { ?>
        <?php if(isset($breadcrumb['separator'])){ if($coun > 1){echo "::";}} ?>
        <?php if($coun == 1){ ?>
            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php }else{ echo $breadcrumb['text']; } ?>
            <?php $coun++; } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
   <?php if ($warning) { ?>
  <div class="warning"><?php echo $warning; ?></div>
  <?php } ?>
  <div class="box" style="width:53%; float:left; margin-right:20px">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button" id="startbutton"><?php echo $button_feed; ?></a></div>
    </div>
    <div class="content">
      <form onsubmit="$('#searchresultsform').show();window.frames['upload_target'].document.body.innerHTML='<div style=\'text-align:center;font-size:14px;font-style:italic\'>Loading results, please wait...</div>';" target="upload_target" action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        
      <input type="hidden" name="start_page" value="1" />
      <input type="hidden" name="amazon[start_page]" value="1">
	<div id="tab-general">
          <table class="form">
            <tr>
              <td><span class="required">*</span> <?php echo $entry_keywords; ?></td>
              <td>
                <input type="text" class="catagore-textfield" name="amazon[keywords]" value="<?php echo $keywords; ?>" />
                <?php if ($error_keywords) { ?>
                <span class="error"><?php echo $error_keywords; ?></span>
                <?php } ?>
              </td>
            </tr>
	    <tr>
	      <td><span class="required">*</span><?php echo $entry_amazoncategory; ?></td>
	      <td>
		<select name="amazon[keyword_cat]" class="ppt-forminput">
            <option value="All" selected>All Categories (see notes below)</option>
            <option>Apparel</option>
            <option>Automotive</option>
            <option>Baby</option>
            <option>Beauty</option>
            <option>Blended</option>
            <option>Books</option>
            <option>Classical</option>
            <option>DigitalMusic</option>
            <option>DVD</option>
            <option>Electronics</option>
            <option>ForeignBooks</option>
            <option>GourmetFood</option>
            <option>Grocery</option>
            <option>HealthPersonalCare</option>
            <option>HomeGarden</option>
            <option>HomeImprovement</option>
            <option>Industrial</option>
            <option>Jewelry</option>
            <option>KindleStore</option>
            <option>Kitchen</option>
            <option>Magazines</option>
            <option>Merchants</option>
            <option>Miscellaneous</option>
            <option>MP3Downloads</option>
            <option>Music</option>
            <option>MusicalInstruments</option>
            <option>MusicTracks</option>
            <option>OfficeProducts</option>
            <option>OutdoorLiving</option>
            <option>PCHardware</option>
            <option>PetSupplies</option>
            <option>Photo</option>
            <option>Shoes</option>
            <option>SilverMerchant</option>
            <option>Software</option>
            <option>SoftwareVideoGames</option>
            <option>SportingGoods</option>
            <option>Tools</option>
            <option>Toys</option>
            <option>VHS</option>
            <option>Video</option>
            <option>VideoGames</option>
            <option>Watches</option>
            <option>Wireless</option>
            <option>WirelessAccessories</option>
            </select>
<div class="clearfix"></div>
   <small>Note, you can not order by 'All Category Searches'. category search also have mixed sort options.  <a href="http://docs.amazonwebservices.com/AWSECommerceService/latest/DG/SortingbyPopularityPriceorCondition.html" target="_blank">See valid Sort</a></small>
	      </td>
	    </tr>
	  </table>
	  
	  <div class="box">
	    <div class="heading" style="border-radius: 0px; margin:0 -10px;">
	      <h1>Filter Options</h1>
	      <div class="buttons"><a href="javascript:void(0);" onclick="$('#contentfilter').toggle();" class="button">Show / Hide Options</a></div>
	    </div>
	    <div class="content" id="contentfilter" style="display:none; border-left:none;border-right: none;">
	      <table class="form">
		<tr>
		  <td>Order By:</td>
		  <td>
		    <select class="ppt-forminput" name="amazon[Sort]">
		      <option value=""></option>
		      <option value="price">Price (low - high)</option>
		      <option value="-price">Price (high - low)</option>
		      <option value="inverseprice">Inverse Price</option>
		      <option value="sale-flag">On Sale</option>
		      <option value="salesrank">Bestselling</option>
		      <option value="pmrank">Featured items</option>
		      <option value="relevancerank">Relevance Rank</option>
		      <option value="reviewrank">reviewrank</option>
		      <option value="titlerank">Alphabetical: A to Z</option>
		      <option value="-titlerank">Alphabetical: Z to A</option>
		      <option value="-launch-date">Newest arrivals</option>
		    </select>
		  </td>
		</tr>
		<tr>
		  <td>Price:</td>
		  <td>
		    Min <input type="text" style="width:80px;" class="ppt-forminput" name="amazon[minprice]"><br>
		    Max <input type="text" style="width:80px;" class="ppt-forminput" name="amazon[maxprice]">
		  </td>
		</tr>
		<tr>
		  <td>Condition:</td>
		  <td>
		    <select style="width: 150px;" name="amazon[condition]">
		      <option>All</option>
		      <option>New</option>
		      <option>Used</option>
		      <option>Refurbished</option>
		      <option>Collectible</option>
		    </select> 
		  </td>
		</tr>
		<tr>
		  <td>Amazon Store</td>
		  <td>
		    <select class="ppt-forminput" name="amazon[country]">
		      <option selected="" value="com">United States (.com Store)</option>
		      <option value="ca">Canada (.ca Store)</option>
		      <option value="co.uk">United Kingdom (.co.uk Store)</option>
		      <option value="fr">France (.fr Store)</option>
		      <option value="de">Germany (.de Store)</option>
		      <option value="jp">Japan (.jp Store)</option>
		      <option value="it">Italy (.it Store)</option>
		      <option value="es">Spain (.es Store)</option> 
		      <option value="cn">China (.cn Store)</option> 
		    </select>
		  </td>
		</tr>
		<tr>
		  <td>Brand</td>
		  <td><input type="text" class="ppt-forminput" name="amazon[brand]"> </td>
		</tr>
		<tr>
		  <td>Amazon Browse Node</td>
		  <td><input type="text" class="ppt-forminput" name="amazon[node]">  </td>
		</tr>
	      </table>
	    </div>
	  </div>
	  
	  <div class="box">
	    <div class="heading" style="border-radius: 0px; margin:0 -10px;">
	      <h1>Save Products</h1>
	      <div class="buttons"><a href="javascript:void(0);" onclick="$('#contentsave').toggle();" class="button">Show / Hide Options</a></div>
	    </div>
	    <div class="content" id="contentsave" style="display:none; border-left:none;border-right: none;border-bottom: none;min-height:100px;">
	      <table class="form">
		<tr>
		  <td style="border-bottom: none;">Category:</td>
		  <td style="border-bottom: none;">
		    <select style="font-size:14px; height:120px;" size="5" multiple="multiple" name="cat[]2">
		      <option value="0">-----------------------------------</option>
		      <?php foreach($categories as $categ) {?>
		      <option value="<?php echo $categ['category_id']?>"><?php echo $categ['name']?></option>
		      <?php }?>
		    </select>
		  </td>
		</tr>
	      </table>
	    </div>
	  </div>
	  
        </div>
      </form>
    </div>
  </div>
  
  <div class="box" style="width:45%; float:left;">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" />Amazon Connection Settings</h1>
      <div class="buttons"><a onclick="$('#amazonform').submit();" class="button">Update Connection</a></div>
    </div>
    <div class="content">
      <div id="error_visible" style="display:none">
	<div class="warning">
	Before you can search for Amazon products you need to fill in the details below provided to you by Amazon once you setup a FREE Amazon AWS account.
      </div>
      </div>
      <?php if (isset($amazon_saving_error)) { ?>
      <div class="warning">
	Before you can search for Amazon products you need to fill in the details below provided to you by Amazon once you setup a FREE Amazon AWS account.
      </div>
      <div class="warning"><?php echo $amazon_saving_error; ?></div>
      <?php } ?>
      <?php if(isset($amazonwarning)){?>
      <div class="warning"><?php echo $amazonwarning;?></div>
      <?php }?>
      <?php if(isset($amazonsuccess)){?>
      <div class="success"><?php echo $amazonsuccess;?></div>
      <?php }?>
      <form action="<?php echo $amazonformaction; ?>" id="amazonform" method="post" enctype="multipart/form-data">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td style="vertical-align:top;width:38%"><span class="required">*</span>Amazon Affiliate ID: </td>
              <td>
                #<input type="text" class="catagore-textfield" style="width:93%" name="affiliate[1]" id="affiliate1" value="<?php if(isset($_POST['affiliate'])){echo $_POST['affiliate'][1];} else echo $affiliates[0]['affiliates_value']; ?>" />
                <div><small>The affiliate ID is used to ensure you get comission from all sales made.<br> <a target="_blank" href="https://affiliate-program.amazon.com/gp/associates/network/main.html">Don't have an ID? Setup an account here.</a></small></div>
              </td>
            </tr>
            <tr>
              <td><span class="required">*</span>Amazon Access Key ID:</td>
              <td>
                #<input type="text" class="catagore-textfield" style="width:93%" name="affiliate[2]" id="affiliate2" value="<?php if(isset($_POST['affiliate'])){echo $_POST['affiliate'][2];} else echo $affiliates[1]['affiliates_value']; ?>" />
              </td>
            </tr>
	    <tr>
	      <td><span class="required">*</span>Amazon Secret Access Key:</td>
	      <td>
		#<input type="text" class="catagore-textfield" style="width:93%" name="affiliate[3]" id="affiliate3" value="<?php if(isset($_POST['affiliate'])){echo $_POST['affiliate'][3];} else echo $affiliates[2]['affiliates_value']; ?>" />
	      </td>
	    </tr>
            <tr>
	      <td><span class="required">*</span>Buy Now Button:</td>
	      <td>
		<select name="affiliate[4]">
		  <option value="yes" <?php if($affiliates[3]['affiliates_value'] == 'yes'){ echo 'selected';}?>>Enable</option>
		  <option value="no" <?php if($affiliates[3]['affiliates_value'] == 'no'){ echo 'selected';}?>>Disable</option>
		</select>
	      </td>
	    </tr>
	    <tr>
	      <td style="vertical-align:top; border-bottom: none;"><span class="required">*</span>Amazon Check Out:</td>
	      <td style="border-bottom: none;">
		<select name="affiliate[5]">
		  <option value="yes" <?php if($affiliates[4]['affiliates_value'] == 'yes'){ echo 'selected';}?>>Enable</option>
		  <option value="no" <?php if($affiliates[4]['affiliates_value'] == 'no'){ echo 'selected';}?>>Disable</option>
		</select>
		<select name="affiliate[6]">
		  <option <?php if($affiliates[5]['affiliates_value'] == 'com'){ echo 'selected';}?> value="com">United States (.com Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'ca'){ echo 'selected';}?> value="ca">Canada (.ca Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'co.uk'){ echo 'selected';}?> value="co.uk">United Kingdom (.co.uk Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'fr'){ echo 'selected';}?> value="fr">France (.fr Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'de'){ echo 'selected';}?> value="de">Germany (.de Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'jp'){ echo 'selected';}?> value="jp">Japan (.jp Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'it'){ echo 'selected';}?> value="it">Italy (.it Store)</option>
		  <option <?php if($affiliates[5]['affiliates_value'] == 'es'){ echo 'selected';}?> value="es">Spain (.es Store)</option> 
		</select>
		<div>
		  <small>Amazon checkout allows the user to add Amazon products to their basket and then checkout at Amazon. The store you select above will be used to determine where the user will be sent to checkout. Note, only products that are imported from that store will work. For example, you cannot import Amazon products from amazon.co.uk and checkout at amazon.com.</small>
		</div>
	      </td>
	    </tr> 
          </table>
        </div>
      </form>
    </div>
  </div>
  
  
  <div class="box" style="float:left; width:100%">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" />Search Results</h1>
    </div>
    <div class="content" id="resultfromamazon">
      <div id="searchresultsform" style="display:none">
      <iframe id="upload_target" style="width:100%;height:1065px;border:0px solid #fff; margin-left:0px;margin-top:10px; " src="<?php echo $iframeaction;?>" name="upload_target">
      </iframe>
      </div>
    </div>
  </div>
  
  </div>
</div>


<?php echo $footer; ?>
<?php
    echo $amazonlistheader;
    $TotalResults = str_replace("!ad","",$result->Items->TotalResults);
    $TotalPages	  = str_replace("!ad","",$result->Items->TotalPages);
?>
<script>
    function addAmazonProduct(cats,country,asin,manufacturer,pageaction)
    {
	$.post(pageaction,{'cats':cats,'country':country,'asin':asin,'manufacturer':manufacturer},
	function(data)
	{
	  if(data == 'exist')
	  {
	  var msg='This product is already in your database';
	  $('#Alert'+asin).html('');
	  $('#Error'+asin).html(msg);
	  }
	  else
	  { //alert(data);
	    if (data == 'saved') {
		$('#A'+asin).css('display','none');
	    }
	    else if (data == 'error') {
	    var msg='Error occurred while importing this product.';
	    $('#Alert'+asin).html('');
	    $('#Error'+asin).html(msg);
	    }
	    
	  }
	})
    }
</script>
    <div style="background:#efefef; border:1px solid #ddd; padding:9px; font-size:11px;">
    <b><?php echo $TotalResults; ?></b> results for '<?php echo $keyword;?>' in <b><?php echo $TotalPages;?></b> pages.
    </div>
<div id="resultstable">
<table class="list">
          <thead>
            <tr>
            </tr>
          </thead>
          <tbody>    
    
<?php   if($TotalResults > 0){
	foreach($result->Items->Item as $val){
	$data['images']='';
	
	
	//print_r($val);
	
	//HasReviews
	 //die(print_r($val));
	 
	//$data['totalReviews'] 	= $val->CustomerReviews->TotalReviews;
	$data['image'] 			= $val->LargeImage->URL;
	$data['thumbnail']		= $val->MediumImage->URL;
        if(isset($val->EditorialReviews->EditorialReview->Content))
	$data['desc']			= $val->EditorialReviews->EditorialReview->Content;
	else
        $data['desc']                   = '';
        $data['title'] 			= $val->ItemAttributes->Title;
	$data['asin'] 			= $val->ASIN;
	$data['url'] 			= $val->DetailPageURL;
	$data['old_price'] 		= "";
	$data['price']			= $val->ItemAttributes->ListPrice->Amount;
	$data['CurrencyCode']		= $val->ItemAttributes->ListPrice->CurrencyCode;
	$data['manufacturer'] 		= $val->ItemAttributes->Manufacturer;
	
	// IMAGE SETS	
	if(isset($val->ImageSets->ImageSet)){	
	 
	$i=1;
		foreach($val->ImageSets->ImageSet as $img){ 
			$data['images'] .= $img->LargeImage->URL.",";
		$i++;  }
	}	
	
	
	
	
	if($data['price'] == ""){
		$data['price'] = $val->OfferSummary->LowestNewPrice->Amount;
	}
	
	if(isset($val->Offers->Offer->OfferListing->Price->Amount) && strlen($val->Offers->Offer->OfferListing->Price->Amount) > 1){
	
			$data['old_price'] = $data['price'];
			$data['price'] 	=  $val->Offers->Offer->OfferListing->Price->Amount;	
	}	
	
	 
 
	// Load price options
	if($_POST['amazon']['country'] == "jp"){
	
	}else{
		$data['old_price'] = number_format(substr($data['old_price'],0, -2)).".".substr($data['old_price'],-2);
		if($data['price'] == ".99"){ $data['price'] = "0.99"; }else{
 		$data['price'] = number_format(substr($data['price'],0, -2)).".".substr($data['price'],-2);
		}
	}	
	 
 	if(strlen($data['old_price']) < 3){ $data['old_price']=""; }
			
	$AFFLINK = "http://www.amazon.".$_POST['amazon']['country']."/o/ASIN/%asin%/%amazon_id%";
	$AFFLINK = str_replace("%asin%",$data['asin'],$AFFLINK);
	$AFFLINK = str_replace("%amazon_id%",'YOURUSERID',$AFFLINK);
	
	
	if($data['price'] != "" && $data['price'] != 0){
	?>  
    <tr id="A<?php echo $data['asin']; ?>">
        <td class="center">
        <a href="<?php echo $data['image']; ?>" target="_blank"><img src="<?php echo $data['thumbnail']; ?>" style="float:left; max-width:60px; max-height:60px; padding-right:20px; padding-bottom:30px; "/></a>    
        </td>
        <td class="left" style="width:75%">
            <b><?php echo $data['title']; ?></b>
            <p style="font-size:10px;"><?php echo strip_tags(substr($data['desc'],0,200)); ?>... <a href="<?php echo $data['url']; ?>" target="_blank">View Item</a></p>
        </td>
        <td class="left">
            <?php echo $data['CurrencyCode']." ".$data['price']; ?>  / Reviews: <?php if($val->CustomerReviews->HasReviews){ echo "Yes"; }else{ echo "No"; } ?><br>
            <a href='javascript:void(0);' onclick="getElementById('Alert<?php echo $data['asin']; ?>').innerHTML='saving product, please wait...'; addAmazonProduct('<?php echo $cats; ?>','<?php echo addslashes($_POST['amazon']['country']); ?>','<?php echo $data['asin']; ?>','<?php echo addslashes($data['manufacturer']);?>','<?php echo $ajaxaction;?>');">Add To Website</a>
            <div id="Alert<?php echo $data['asin']; ?>" style="font-size:12px; font-weight:bold; background:#e6ffd7;color:#296900; margin-bottom:5px; margin-top:10px"></div>
	    <div id="Error<?php echo $data['asin']; ?>" style="font-size:12px; font-weight:bold; color:red; margin-bottom:5px; margin-top:10px"></div>
        </td>
    </tr>
    <?php
    }
    }
}
else { ?>
            <tr>
              <td class="center" colspan="9"><?php echo $warning; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
</div>

    <?php  
	$cpage = $start_page; 
	$i=$cpage;
    ?>

<?php if($TotalPages > 1):?>
<div class="admin-pagen">
 <ul>
   <?php if($start_page > 1){ ?><li><a  title="Previous Page" href="javascript:void(0);" onclick="$('#start_page').val('<?php echo $start_page-1; ?>');$('#resultstable').innerHTML='<br>Loading results, please wait...';$('#subform').submit();"><<</a></li><?php }?>
   <?php if($TotalPages > 5)
    $countpage=5;
    else
    $countpage=$TotalPages;?>
   <?php for($pc=1;$pc<=$countpage;$pc++)
    {?>
    <?php if($pc != $start_page){?>
        <li>
            <a href="javascript:void(0);" onclick="$('#start_page').val('<?php echo $pc; ?>');$('#resultstable').innerHTML='<br>Loading results, please wait...'; $('#subform').submit();"><?php echo $pc; ?></a>
            <?php }
            else {
            echo '<li class="active"><a href="javascript:void(0);">';    
            echo $pc;
            echo '</a>';
             }?>
        </li>
    <?php }?>
    <?php if(($start_page < 5) && ($TotalPages > $start_page))
    {?>
     <li><a title="Next Page" href="javascript:void(0);" onclick="$('#resultstable').innerHTML='<br>Loading results, please wait...';$('#subform').submit();">>></a></li> 
    <?php }?>
    
 </ul>   
    
</div>

    
    <form method="post" target="_self" id="subform" name="subform">			
    <input type="hidden" name="feed" value="1">
    <?php
	
	foreach($cate as $cat){			
		print '<input type="hidden" name="cat[]" value="'.$cat.'">';	
	}
	
	foreach($postdata as $key=>$val){
	if(!is_array($val)){
	print '<input type="hidden" name="'.$key.'" value="'.$val.'">';
	}	
	}
	
	foreach($postamazon as $key=>$val){
	
		if(is_array($val)){
		
			foreach($val as $key=>$val1){
				 print '<input type="hidden" name="'.$key.'" value="'.$val1.'">';			 
			}
		
		
		}else{
		
			if($key == "start_page"){	
				if($val ==""){ $val=2; }else{ $val++; }	
				print '<input type="hidden" id="start_page" name="amazon['.$key.']" value="'.$val.'" id="start_page">';	
			}else{	
				print '<input type="hidden" name="amazon['.$key.']" value="'.$val.'">';
			}
		}
	
	}
	
	?>
	</form>
    
<?php endif;?>
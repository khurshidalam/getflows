<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_events_list; ?></h1>
      <div class="buttons"><a onclick="$('#form').attr('action', '<?php echo $delete; ?>'); $('#form').attr('target', '_self'); $('#form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
      <form action="" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
			  
              <td class="right"><?php if ($sort == 'event_id') { ?>
                <a href="<?php echo $sort_event_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_event_id; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_event_id; ?>"><?php echo $column_event_id; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'customer_id') { ?>
                <a href="<?php echo $sort_customer_id; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_customer_id; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_customer_id; ?>"><?php echo $column_customer_id; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'title') { ?>
                <a href="<?php echo $sort_title; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_title; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_title; ?>"><?php echo $column_title; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'email') { ?>
                <a href="<?php echo $sort_email; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_email; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_email; ?>"><?php echo $column_email; ?></a>
                <?php } ?></td>
				
              <td class="right"><?php if ($sort == 'type') { ?>
                <a href="<?php echo $sort_type; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_type; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_type; ?>"><?php echo $column_type; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'status') { ?>
                <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'start_date') { ?>
                <a href="<?php echo $sort_start_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_start_date; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_start_date; ?>"><?php echo $column_start_date; ?></a>
                <?php } ?></td>
				
              <td class="left"><?php if ($sort == 'end_date') { ?>
                <a href="<?php echo $sort_end_date; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_end_date; ?></a>
                <?php } else { ?>
                <a href="<?php echo $sort_end_date; ?>"><?php echo $column_end_date; ?></a>
                <?php } ?></td>
				
              <td class="right"><?php echo $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <tr class="filter">
              <td></td>
              <td align="right"><input type="text" name="filter_event_id" value="<?php echo $filter_event_id; ?>" size="4" style="text-align: right;" /></td>
              <td><input type="text" name="filter_customer_id" value="<?php echo $filter_customer_id; ?>" /></td>
              <td align="right"><input type="text" name="filter_title" value="<?php echo $filter_title; ?>" size="4" style="text-align: right;" /></td>
              <td align="right"><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" size="4" style="text-align: right;" /></td>
              <td><input type="text" name="filter_email" value="<?php echo $filter_email; ?>" size="12" /></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td align="right"><a onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
            </tr>
            <?php if ($events) { ?>
            <?php foreach ($events as $event) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($event['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $event['event_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $event['event_id']; ?>" />
                <?php } ?></td>
              <td class="right"><?php echo $event['event_id']; ?></td>
              <td class="left"><?php echo $event['customer_id']; ?></td>
              <td class="left"><?php echo $event['title']; ?></td>
              <td class="left"><?php echo $event['name']; ?></td>
              <td class="left"><?php echo $event['email']; ?></td>
              <td class="right"><?php echo $event['type']; ?></td>
              <td class="right"><?php echo $event['status']; ?></td>
              <td class="left"><?php echo $event['start_date']; ?></td>
              <td class="left"><?php echo $event['end_date']; ?></td>
              <td class="right"><?php foreach ($event['action'] as $action) { ?>
                [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                <?php } ?></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="11"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=sale/events&token=<?php echo $token; ?>';
	
	var filter_event_id = $('input[name=\'filter_event_id\']').attr('value');
	
	if (filter_event_id) {
		url += '&filter_event_id=' + encodeURIComponent(filter_event_id);
	}
	
	var filter_customer_id = $('input[name=\'filter_customer_id\']').attr('value');
	
	if (filter_customer_id) {
		url += '&filter_customer_id=' + encodeURIComponent(filter_customer_id);
	}
	
	var filter_title = $('input[name=\'filter_title\']').attr('value');
	
	if (filter_title) {
		url += '&filter_title=' + encodeURIComponent(filter_title);
	}	

	var filter_name = $('input[name=\'filter_name\']').attr('value');

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}	
	
	var filter_email = $('input[name=\'filter_email\']').attr('value');
	
	if (filter_email) {
		url += '&filter_email=' + encodeURIComponent(filter_email);
	}
				
	location = url;
}
//--></script>  
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<?php echo $footer; ?>
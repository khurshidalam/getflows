<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div class="htabs">
	    <a href="#tab-general"><?php echo $tab_general; ?></a>
<!--		
	    <a href="#tab-data"><?php echo $tab_data; ?></a>
-->		
	  </div>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><?php echo $entry_event_id; ?></td>
              <td><?php echo $event_id; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_customer; ?></td>
              <td><input type="text" name="customer" value="<?php echo $customer; ?>" />
                <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>" /></td>
            </tr>
            <tr>
              <td><?php echo $entry_title; ?></td>
              <td><?php echo $title; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_name; ?></td>
              <td><?php echo $name; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_email; ?></td>
              <td><input type="text" name="email" value="<?php echo $email; ?>" />
            </tr>
            <tr>
              <td><?php echo $entry_type; ?></td>
              <td><?php echo $type; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><?php echo $status; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_start_date; ?></td>
              <td><?php echo $start_date; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_end_date; ?></td>
              <td><?php echo $end_date; ?></td>
            </tr>
          </table>
        </div>
<!--        <div id="tab-data">
          <table class="form">
            <tr>
              <td>Some stuff to go here perhaps?</td>
            </tr>
          </table>
        </div>
-->		
      </form>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

$('input[name=\'customer\']').catcomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {	
				response($.map(json, function(item) {
					return {
						category: item.customer_group,
						label: item.name,
						value: item.customer_id,
						firstname: item.firstname,
						lastname: item.lastname,
						email: item.email,
					}
				}));
			}
		});
		
	}, 
	select: function(event, ui) {
		$('input[name=\'customer\']').attr('value', ui.item.label);
		$('input[name=\'customer_id\']').attr('value', ui.item.value);
		$('input[name=\'email\']').attr('value', ui.item['email']);
		
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$('.htabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>
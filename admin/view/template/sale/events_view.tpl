<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="success" id="saved"><?php echo 'saved' ?></div>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/customer.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <div class="htabs">
	    <a href="#tab-general"><?php echo $tab_general; ?></a>
	    <a href="#tab-data"><?php echo $tab_data; ?></a>
	  </div>
        <div id="tab-general">
          <table class="form">
            <tr>
              <td><?php echo $entry_event_id; ?></td>
              <td><?php echo $event_id; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_customer; ?></td>
              <td><?php echo $customer; ?>
            </tr>
            <tr>
              <td><?php echo $entry_title; ?></td>
              <td><?php echo $title; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_name; ?></td>
              <td><?php echo $name; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_email; ?></td>
              <td><?php echo $email; ?>
            </tr>
            <tr>
              <td><?php echo $entry_type; ?></td>
              <td><?php echo $type; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_status; ?></td>
              <td><?php echo $status; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_start_date; ?></td>
              <td><?php echo $start_date; ?></td>
            </tr>
            <tr>
              <td><?php echo $entry_end_date; ?></td>
              <td><?php echo $end_date; ?></td>
            </tr>
          </table>
        </div>
        <div id="tab-data">
		<div>
			<table class="list">
				<thead>
				<tr>
					<td class="left"><?php echo $column_image; ?></td>
					<td class="left"><?php echo $column_name; ?></td>
					<td class="left"><?php echo $column_model; ?></td>
					<td class="left"><?php echo $column_qty_rqd; ?></td>
					<td class="left"><?php echo $column_qty_bought; ?></td>
					<td class="left"><?php echo $column_order; ?></td>
					<td class="total"><?php echo $column_price; ?></td>		  
					<td class="right"><?php echo $column_action; ?></td>
				</tr>
			</thead>
			<tbody>
			<?php if (isset($products)) { ?>
			<?php foreach ($products as $product) { ?>
			<tr>
				<td class="center"><?php if ($product['thumb']) { ?>
					<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" /></a>
				<?php } ?></td>
				<td class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
				<?php if (!$product['stock']) { ?>
				<span class="stock">***</span>
				<?php } ?>
				<?php foreach ($product['option'] as $option) { ?>
					<div>
					- <small><?php echo $option['name']; ?>: <?php echo $option['value']; ?></small><br />
					</div>
				<?php } ?>
				</td>
				<td class="left"><?php echo $product['model']; ?></td>
				<td class="left"><input type="text" name="quantity[<?php echo $product['key']; ?>]" value="<?php echo $product['quantity']; ?>" size="1" disabled="disabled" /></td>
				<td class="left"><input type="text" id="<?php echo $product['iid']; ?>" name="bought[<?php echo $product['key']; ?>]" value="<?php echo $product['bought']; ?>" size="1" /></td>
				<td>
				<?php foreach ($product['oid'] as $oid) { ?>
				<?php foreach ($oid as $order_id) { ?>
				<a href="<?php echo $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $order_id, 'SSL') ?>" /><?php echo $order_id; ?></a>
				<?php } ?>
				<?php } ?>
				</td>
				<td class="price"><?php echo $this->currency->format($product['price'], $this->config->get('config_currency')); ?></td>
				<td class="right"><a href="<?php echo $product['action']; ?>" onClick="return confirm('Delete can\'t be undone - Are you Sure?');"><?php echo $delete; ?></a></td>
			</tr>
			<?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
			</tbody>
		</table>
		</div>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
$(document).ready(function() {
$('#saved').css('display', 'none').css('opacity', '1');
$('input').on('change', function() {
	var itemID = $(this).attr('id');
    var itemVal = $(this).val();
	var itemdata = "id=" + itemID + "&value=" + itemVal;

	processChange(itemdata);
});
function processChange(itemdata){
	$.ajax({
		type: 'post',
		url: '<?php echo $store_url; ?>index.php?route=sale/events/updateQty&token=<?php echo $token; ?>',
		data: itemdata,
		success: function() {
			$('#saved').css('display', 'block');
			setTimeout(function() {
				$('#saved').animate({
					opacity: 0,
				}, 500, function(){
					$('#saved').css('display', 'none').css('opacity', '1');
				});
			}, 1000);
		}
	});
}
});
$('.htabs a').tabs(); 
//--></script> 
<?php echo $footer; ?>
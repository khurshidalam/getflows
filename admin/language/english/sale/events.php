<?php
// Version 1.21
// Heading
$_['heading_title']    					= 'Events';
$_['heading_title_events_list']    		= 'Events List';
$_['heading_title_edit_event']    		= 'Editing Event';
$_['heading_title_view_event']    		= 'Viewing Event';

// Column
$_['column_event_id']    = 'Event ID';
$_['column_customer_id'] = 'Customer Name';
$_['column_name']        = 'Name';
$_['column_email']       = 'Email';
$_['column_type']        = 'Type';
$_['column_action']      = 'Action';
$_['column_title']       = 'Title';
$_['column_start_date']	 = 'Start Date';
$_['column_end_date']  	 = 'End Date';
$_['column_status']      = 'Status';
$_['column_action']      = 'Action';
$_['column_qty_rqd'] 	= 'Qty Required';
$_['column_qty_bought'] = 'Qty Bought';
$_['column_image'] 		= 'Image';
$_['column_model'] 		= 'Model';
$_['column_price'] 		= 'Price';
$_['column_order'] 		= 'Order ID';

// Text
$_['text_view_products'] = 'View';
$_['text_public'] 		 = 'Public';
$_['text_private'] 		 = 'Private';
$_['text_success'] 		 = 'Success, You have modified Events.';
$_['text_no_results'] 	 = 'This Event Has No Products';

// Entry
$_['entry_event_id']    = 'Event ID:';
$_['entry_customer_id'] = 'Customer ID:';
$_['entry_customer'] 	= 'Customer:';
$_['entry_name']        = 'Name:';
$_['entry_email']       = 'Email:';
$_['entry_type']        = 'Type:';
$_['entry_action']      = 'Action:';
$_['entry_title']       = 'Title:';
$_['entry_start_date'] 	= 'Start Date:';
$_['entry_end_date']  	= 'End Date:';
$_['entry_status']      = 'Status:';
$_['entry_action']      = 'Action:';

//Buttons
$_['button_save']  	 = 'Save';
$_['delete_link']	 = 'Delete';

// Error
$_['error_title']      = 'Title required';
?>
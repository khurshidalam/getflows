<?php
// Heading
$_['heading_title']		= 'Wholesale Price List<span style="float:right;">[ <a href="http://opencart.my/" target="_blank">Opencart.my</a> ]</span>';
$_['common_title']		= 'Wholesale Price List';

// Text
$_['text_module']		= 'Modules';
$_['text_success']      = 'Success: You have modified Wholesale Price List module!';

//Entry
$_['entry_login']       = 'Customer Login Required:<br /><span class="help">Only enable price list when customer is logged in.</span>';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify Wholesale Price List module!';

$_['myoc_copyright']	= '<p>Copyright &copy; 2012-2013 Opencart.my. All rights reserved.<br />Version 1.3.1</p>';
?>
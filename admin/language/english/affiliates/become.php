<?php
// Heading
$_['heading_title']             = 'Feed Become data by category';

// Text
$_['text_success']              = 'Success: You have successfully modified affiliates!';
$_['text_approved']             = 'You have approved %s accounts!';
$_['text_wait']                 = 'Please Wait!';
$_['text_balance']              = 'Balance:';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';

// Column
$_['column_name']               = 'Affiliate Name';
$_['column_email']              = 'E-Mail';
$_['column_code']               = 'Tracking Code';
$_['column_balance']            = 'Balance';
$_['column_status']             = 'Status';
$_['column_approved']           = 'Approved';
$_['column_date_added']         = 'Date Added';
$_['column_description']        = 'Description';
$_['column_amount']             = 'Amount';
$_['column_action']             = 'Action';

// Entry
$_['entry_keywords']           = 'Enter Keywords:';
$_['entry_category']           = 'Enter Category:';
$_['button_feed']              = 'Feed Data';


// Error
$_['error_keywords']            = 'Invalid Keywords!';
$_['error_category']            = 'Invalid Category!';

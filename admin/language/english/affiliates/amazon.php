<?php
// Heading
$_['heading_title']             = 'Feed Amazon data';

// Text
//$_['text_success']              = 'Success: You have modified affiliates!';
$_['text_success']              = 'Success: %d new and %d exsisting record(s) modified!';
$_['text_approved']             = 'You have approved %s accounts!';
$_['text_wait']                 = 'Please Wait!';
$_['text_balance']              = 'Balance:';
$_['text_cheque']               = 'Cheque';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Bank Transfer';

// Column
$_['column_name']               = 'Affiliate Name';
$_['column_email']              = 'E-Mail';
$_['column_code']               = 'Tracking Code';
$_['column_balance']            = 'Balance';
$_['column_status']             = 'Status';
$_['column_approved']           = 'Approved';
$_['column_date_added']         = 'Date Added';
$_['column_description']        = 'Description';
$_['column_amount']             = 'Amount';
$_['column_action']             = 'Action';

// Entry
$_['entry_keywords']           = 'Search Keyword:';
$_['entry_category']           = 'Category:';
$_['entry_amazoncategory']      = 'Search Category:';
$_['button_feed']              = 'Start Search';


// Error
$_['error_keywords']            = 'Invalid Keywords!';
$_['error_category']            = 'Invalid Category!';

<?php
class User {
	private $user_id;
	private $username;
	private $permission = array();

	public function __construct($registry) {
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

				$permissions = unserialize($user_group_query->row['permission']);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($username, $password) {
		$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape($password) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");

		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];

			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];			

			$user_group_query = $this->db->query("SELECT permission FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			$permissions = unserialize($user_group_query->row['permission']);

			if (is_array($permissions)) {
				foreach ($permissions as $key => $value) {
					$this->permission[$key] = $value;
				}
			}

			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		unset($this->session->data['user_id']);

		$this->user_id = '';
		$this->username = '';

		session_destroy();
	}

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getUserName() {
		return $this->username;
	}
	
	
	
	
	
	/**
	  *Custom Functions for the amazon api starts here.
	  **/
	
	
	public function prodInsAmazon($model, $title, $sku,$ean,$upc,$asin,$quantity, $manufacturer, $old_price, $price, $image, $detailpageUrl){
	
		$manufacturerTable = DB_PREFIX."manufacturer";
		$productTable      = DB_PREFIX."product";
		$todaysdate        = date("Y-m-d");
		$dateAdded         = date("Y-m-d H:i:s");
		$sql = "INSERT INTO `".$productTable."` SET
			`model`    = '".$model."',
			`sku`      = '".$sku."',
			`ean`      = '".$ean."',
			`upc`      = '".$upc."',
			`asin`      = '".$asin."',
			`quantity` = '".$quantity."',
			`stock_status_id` = '7',
			`manufacturer_id` = '".$manufacturer."',
			`shipping`        = '0',
			`old_price`           = '".$old_price."',
			`price`           = '".$price."',
			`points`          = '0',
			`tax_class_id`    = '9',
			`date_available`  = '".$todaysdate."',
			`weight`          = '5.00000000',
			`weight_class_id` = '2',
			`length`          = '',
			`width`           = '',
			`height`          = '',
			`length_class_id` = '1',
			`subtract`        = '1',
			`minimum`         = '1',
			`sort_order`      = '0',
			`status`          = '1',
			`date_added`      = '".$dateAdded."',
			`viewed`          = '0',
			`image`           = '".$image."',
			`target_url`      = '".$detailpageUrl."'
			";
			
		$this->db->query($sql);
		//echo $this->db->getLastId();
		//return $this->db->getLastId();
	}
	
	public function prodUrlSeo($prodId,$seoUrl){
		$seo_table = DB_PREFIX."url_alias";
		$product_id = "product_id=".$prodId;
		$sql = "INSERT INTO `".$seo_table."` SET `query`='".$product_id."', `keyword`='".$seoUrl."'";
		$this->db->query($sql);
	}
	
	public function prodAttribute($prodId, $atrbId, $txt ){
		$productAttributeTable = DB_PREFIX."product_attribute";
		$sql = "INSERT INTO `".$productAttributeTable."` SET
			`product_id`   = '".$prodId."',
			`attribute_id` = '".$atrbId."',
			`language_id`  = '1',
			`text`         = '".$txt."'
			";
		$this->db->query($sql);
	}
	
	public function prodDescIns($id, $lanId, $name, $desc){
		$productDescTable = DB_PREFIX."product_description";
		$sql = "INSERT INTO `".$productDescTable."` SET
				   `product_id`  = '".$id."',
				   `language_id` = '".$lanId."',
				   `name`        = '".$name."',
				   `description` = '".$desc."'
				   ";
		$this->db->query($sql);
	}
	
	public function prodToStore($id, $storeId){
	    $productToStoreTable = DB_PREFIX."product_to_store";
	    $sql = "INSERT INTO `".$productToStoreTable."` SET
	           `product_id` = '".$id."',
	           `store_id`   = '".$storeId."'
	           ";
	    $this->db->query($sql);
	}
	
	public function prodToCategory($id, $catIds){
	    $productToCategory = DB_PREFIX."product_to_category";
	    for($k=0;$k<count($catIds);$k++)
	    {
	    $sql = "INSERT INTO `".$productToCategory."` SET
	            `product_id`  = '".$id."',
	            `category_id` = '".$catIds[$k]."'
	            ";
	    $this->db->query($sql);
	    }
	}
	
	public function prodReward($id){
	    $productReward = DB_PREFIX."product_reward";
	    $sql = "INSERT INTO `".$productReward."` SET
	           `product_id`        = '".$id."',
	           `customer_group_id` = '1',
	           `points`            = '0'
	           ";
	    $this->db->query($sql);
	}
	
	
	public function atributeCheck($atrb){
		$attributeTable     = DB_PREFIX."attribute";
		$attributeDescTable = DB_PREFIX."attribute_description";
		$sql = $this->db->query("SELECT `name`,`attribute_id` FROM `".$attributeDescTable."` WHERE `name` = '".$atrb."'");
		if($sql->row){
		    //$ret = mysql_fetch_assoc($sql);
		    $ret = $sql->row;
		    return $ret['attribute_id'];
		}
		else{
		    $sql2 = "INSERT INTO `".$attributeTable."` SET
			    `attribute_group_id` = '7',
			    `sort_order`         = '1'
			    ";
		    $this->db->query($sql2);
		    $atrbId = $this->db->getLastId();
		    $sql = $this->db->query("INSERT INTO `".$attributeDescTable."` SET
				       `attribute_id` = '".$atrbId."',
				       `name`         = '".$atrb."',
				       `language_id`  = '1'
				       ");
		    $this->db->query($sql2);
		    return $atrbId;
		}
	}
	
	public function checkManu($name, $creator){
		$manufacturerTable = DB_PREFIX."manufacturer";
		$name2=strtolower($name);
		$que="SELECT `manufacturer_id`,`name` FROM `".$manufacturerTable."` WHERE LOWER(`name`) = '".$name2."'";
		$sql = $this->db->query($que);
		if($sql->row){
		    $ret = $sql->row;
		    return $ret['manufacturer_id'];
		}
		else{
		    $sql = $this->db->query("INSERT INTO `".$manufacturerTable."` SET
				       `name`         = '".$name."',
				       `manu_creator` = '".$creator."',
				       `sort_order`   = '0'
				       ");
		    $manuId = $this->db->getLastId();
		    return $manuId;
		}
	}
	
	/* Custom functions for the amazon api ends here. */
}
?>